from tensorflow.keras.metrics import Metric
import numpy as np
from tensorflow.python.ops import math_ops
from tensorflow.python.framework.ops import Tensor


class My_IOU(Metric):

    def __init__(self, num_classes=2, name=None, dtype=None):
        super(My_IOU, self).__init__(name=name, dtype=dtype)
        self.num_classes = num_classes

        self._mem_true = None
        self._mem_pred = None

        self._current = 0.0
        self._n = 0.0

    def result(self):
        if self._mem_true is None:
            return self._current

        temp_n = self._mem_true.shape[0]

        a_pred = (np.abs(self._mem_pred[:,0,0]-self._mem_pred[:,1,0])) * (np.abs(self._mem_pred[:,0,1]-self._mem_pred[:,1,1]))
        a_true = (np.abs(self._mem_true[:,0,0]-self._mem_true[:,1,0])) * (np.abs(self._mem_true[:,0,1]-self._mem_true[:,1,1]))

        x_min = np.maximum(self._mem_pred[:, 0, 0], self._mem_true[:, 0, 0])
        x_max = np.minimum(self._mem_pred[:, 1, 0], self._mem_true[:, 1, 0])

        y_min = np.maximum(self._mem_pred[:, 0, 1], self._mem_true[:, 0, 1])
        y_max = np.minimum(self._mem_pred[:, 1, 1], self._mem_true[:, 1, 1])

        i = np.abs((x_max - x_min) * (y_max - y_min))

        i[x_min > x_max] = 0.0
        i[y_min > y_max] = 0.0

        u = a_true + a_pred - i
        temp_iou = np.mean(i / u)

        global_n = self._n + temp_n
        res = (self._current * (self._n / global_n)) + (temp_iou * (temp_n / global_n))
        self._current = res
        self._n += temp_n

        self._reset_mem()

        return self._current

    def get_config(self):
        config = {'num_classes': self.num_classes}
        base_config = super(My_IOU, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def _reset_mem(self):
        self._mem_true = None
        self._mem_pred = None

    def reset_states(self):
        self._mem_true = None
        self._mem_pred = None

        self._current = 0.0
        self._n = 0.0

    def _update(self, y_true, y_pred):
        y_true = y_true.reshape((2, 2))
        y_pred = y_pred.reshape((2, 2))

        if y_true.shape[0] != 1:
            y_true = np.expand_dims(y_true, axis=0)
        if y_pred.shape[0] != 1:
            y_pred = np.expand_dims(y_pred, axis=0)

        if self._mem_true is None:
            self._mem_true = np.array(y_true)
        else:
            self._mem_true = np.append(self._mem_true, y_true, axis=0)

        if self._mem_pred is None:
            self._mem_pred = np.array(y_pred)
        else:
            self._mem_pred = np.append(self._mem_pred, y_pred, axis=0)

    def update_state(self, y_true, y_pred, sample_weight=None):

        if isinstance(y_true, Tensor):
            y_true = math_ops.cast(y_true, self._dtype)
            y_true = y_true.numpy()
        if isinstance(y_pred, Tensor):
            y_pred = math_ops.cast(y_pred, self._dtype)
            y_pred = y_pred.numpy()

        for i in range(y_true.shape[0]):
            self._update(y_true[i], y_pred[i])

def blub_0():
    some_test = My_IOU()

    y_true = ((0.5, 0.5), (0.6, 0.6))
    y_pred = ((0.5, 0.5), (0.7, 0.6))

    a = some_test.update_state(y_true, y_pred)

    y_true = ((0.1, 0.1), (0.2, 0.2))
    y_pred = ((0.4, 0.3), (0.7, 0.6))

    a = some_test.update_state(y_true, y_pred)

    b = some_test.result()

    print()

def blub_1():
    a = np.array([1, 2, 3, 4])
    b = a.reshape((2, 2))

    print(a)
    print(b)

if __name__ == '__main__':
    blub_1()





