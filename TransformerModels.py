import os
os.environ["TF_MIN_GPU_MULTIPROCESSOR_COUNT"]="6"

from MyTransformer_v2 import TransformerPackage, Transformer, TransformerWithPreprocess, TransformerHeadless, \
    TransformerHeadlessNoPos, TransformerHeadlessNoPosVisionTrain, TransformerHeadlessPosVisionTrain, TransformerHeadlessPos
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import Conv3D, Conv2D, MaxPooling2D, GlobalMaxPool2D, BatchNormalization, Input, SeparableConv2D, Reshape
from residual_conv import MyResidualIdentityConv2D, MyResidualConv2D

from tensorflow.keras.models import Model
import numpy as np
import tensorflow as tf

from vision_nets import get_vision_net
from my_metrics import next_char
from lip_data_reader import LipDataReader
from MyTransformer_v4 import TransformerHeadlessEmbPosCrossent, TransformerHeadlessEmbPosMean, \
    TransformerHeadlessEmbSinPosCrossent, TransformerHeadlessEmbSinPosMean, \
    TransformerHeadlessEmbSinPosCrossentMaskLess, TransformerHeadlessEmbSinPosMeanMaskLess, \
    TransformerHeadlessEmbPosMeanMaskLess, TransformerHeadlessEmbPosCrossentMaskLess, \
    MyBaseTransformer
from MyTransformers_v3 import MySwitchTransformer, MySwitchTransformerTail
from MyTransformers_V5 import MyExplicitSwitchTransformerWorkaround, MyExplicitSwitchTransformerWorkaroundTail


def get_transformer_model(version, section_len, feature_size, vocab_size, build_bs=None, lr=None, pre_train=False, bs=None):
    # strategy = tf.distribute.MirroredStrategy()
    # print('Number of devices: {}'.format(strategy.num_replicas_in_sync))
    # with strategy.scope():
    model = None
    if version == 0:
        model = get_transformer_model_v0(section_len, feature_size, vocab_size, bs=bs)
    elif version == 1:
        model = get_transformer_model_v1(section_len, feature_size, vocab_size, bs=bs)
    elif version == 2:
        model = get_transformer_model_v2(section_len, feature_size, vocab_size, bs=bs)
    elif version == 3:
        model = get_transformer_model_v3(section_len, feature_size, vocab_size, bs=bs)
    elif version == 4:
        model = get_transformer_model_v4(section_len, feature_size, vocab_size, bs=bs)
    elif version == 5:
        model = get_transformer_model_v5(section_len, feature_size, vocab_size, bs=bs)
    elif version == 6:
        model = get_transformer_model_v6(section_len, feature_size, vocab_size, bs=bs)
    elif version == 7:
        model = get_transformer_model_v7(section_len, feature_size, vocab_size, bs=bs)
    elif version == 8:
        model = get_transformer_model_v8(section_len, feature_size, vocab_size, bs=bs)
    elif version == 9:
        model = get_transformer_model_v9(section_len, feature_size, vocab_size, bs=bs)
    elif version == 10:
        model = get_transformer_model_v10(section_len, feature_size, vocab_size, bs=bs)
    elif version == 11:
        model = get_transformer_model_v11(section_len, feature_size, vocab_size, bs=bs)
    elif version == 12:
        model = get_transformer_model_v12(section_len, feature_size, vocab_size, bs=bs)
    elif version == 13:
        model = get_transformer_model_v13(section_len, feature_size, vocab_size, bs=bs)
    elif version == 14:
        model = get_transformer_model_v14(section_len, feature_size, vocab_size, bs=bs)
    elif version == 15:
        model = get_transformer_model_v15(section_len, feature_size, vocab_size, bs=bs)
    elif version == 16:
        model = get_transformer_model_v16(section_len, feature_size, vocab_size, bs=bs)
    elif version == 17:
        model = get_transformer_model_v17(section_len, feature_size, vocab_size, bs=bs)
    elif version == 18:
        model = get_transformer_model_v18(section_len, feature_size, vocab_size, bs=bs)
    elif version == 19:
        model = get_transformer_model_v19(section_len, feature_size, vocab_size, bs=bs)
    elif version == 20:
        model = get_transformer_model_v20(section_len, feature_size, vocab_size, bs=bs)
    elif version == 21:
        model = get_transformer_model_v21(section_len, feature_size, vocab_size, bs=bs)
    elif version == 22:
        model = get_transformer_model_v22(section_len, feature_size, vocab_size, bs=bs)
    elif version == 23:
        model = get_transformer_model_v23(section_len, feature_size, vocab_size, bs=bs)
    elif version == 24:
        model = get_transformer_model_v24(section_len, feature_size, vocab_size, bs=bs)
    elif version == 100:
        model = get_transformer_model_v100(section_len, feature_size, vocab_size, bs=bs)
    elif version == 101:
        model = get_transformer_model_v101(section_len, feature_size, vocab_size, bs=bs)
    elif version == 102:
        model = get_transformer_model_v102(section_len, feature_size, vocab_size, bs=bs)
    elif version == 200:
        model = get_transformer_model_v200(section_len, feature_size, vocab_size, bs=bs)
    elif version == 201:
        model = get_transformer_model_v201(section_len, feature_size, vocab_size, bs=bs)
    elif version == 202:
        model = get_transformer_model_v202(section_len, feature_size, vocab_size, bs=bs)
    elif version == 203:
        model = get_transformer_model_v203(section_len, feature_size, vocab_size, bs=bs)
    elif version == 204:
        model = get_transformer_model_v204(section_len, feature_size, vocab_size, bs=bs)
    elif version == 205:
        model = get_transformer_model_v205(section_len, feature_size, vocab_size, bs=bs)

    elif version == 1000:
        model = get_transformer_model_v1000(section_len, feature_size, vocab_size, bs=bs)

    elif version == 1001:
        model = get_transformer_model_v1001(section_len, feature_size, vocab_size, bs=bs)

    elif version == 1002:
        model = get_transformer_model_v1002(section_len, feature_size, vocab_size, bs=bs)

    elif version == 1003:
        model = get_transformer_model_v1003(section_len, feature_size, vocab_size, bs=bs)

    elif version == 1004:
        model = get_transformer_model_v1004(section_len, feature_size, vocab_size, bs=bs)

    elif version == 1005:
        model = get_transformer_model_v1005(section_len, feature_size, vocab_size, bs=bs)

    elif version == 1006:
        model = get_transformer_model_v1006(section_len, feature_size, vocab_size, bs=bs)

    elif version == 1007:
        model = get_transformer_model_v1007(section_len, feature_size, vocab_size, bs=bs)

    elif version == 1008:
        model = get_transformer_model_v1008(section_len, feature_size, vocab_size, bs=bs)

    elif version == 1009:
        model = get_transformer_model_v1009(section_len, feature_size, vocab_size, bs=bs)

    elif version == 1010:
        model = get_transformer_model_v1010(section_len, feature_size, vocab_size, bs=bs)

    elif version == 1011:
        model = get_transformer_model_v1011(section_len, feature_size, vocab_size, bs=bs)

    elif version == 1012:
        model = get_transformer_model_v1012(section_len, feature_size, vocab_size, bs=bs)

    elif version == 1013:
        model = get_transformer_model_v1013(section_len, feature_size, vocab_size, bs=bs)

    elif version == 1014:
        model = get_transformer_model_v1014(section_len, feature_size, vocab_size, bs=bs)

    elif version == 1015:
        model = get_transformer_model_v1015(section_len, feature_size, vocab_size, bs=bs)

    elif version == 1016:
        model = get_transformer_model_v1016(section_len, feature_size, vocab_size, bs=bs)

    elif version == 1017:
        model = get_transformer_model_v1017(section_len, feature_size, vocab_size, bs=bs)

    elif version == 1018:
        model = get_transformer_model_v1018(section_len, feature_size, vocab_size, bs=bs)

    elif version == 1019:
        model = get_transformer_model_v1019(section_len, feature_size, vocab_size, bs=bs)

    elif version == 1020:
        model = get_transformer_model_v1020(section_len, feature_size, vocab_size, bs=bs)

    elif version == 1021:
        model = get_transformer_model_v1021(section_len, feature_size, vocab_size, bs=bs)

    elif version == 2000:
        model = get_transformer_model_v2000(section_len, feature_size, vocab_size, bs=bs)

    elif version == 2001:
        model = get_transformer_model_v2001(section_len, feature_size, vocab_size, bs=bs)

    elif version == 2002:
        model = get_transformer_model_v2002(section_len, feature_size, vocab_size, bs=bs)

    elif version == 2003:
        model = get_transformer_model_v2003(section_len, feature_size, vocab_size, bs=bs)

    elif version == 2004:
        model = get_transformer_model_v2004(section_len, feature_size, vocab_size, bs=bs)

    elif version == 2005:
        model = get_transformer_model_v2005(section_len, feature_size, vocab_size, bs=bs)

    elif version == 2006:
        model = get_transformer_model_v2006(section_len, feature_size, vocab_size, bs=bs)

    elif version == 2007:
        model = get_transformer_model_v2007(section_len, feature_size, vocab_size, bs=bs)

    elif version == 2008:
        model = get_transformer_model_v2008(section_len, feature_size, vocab_size, bs=bs)

    elif version == 2009:
        model = get_transformer_model_v2009(section_len, feature_size, vocab_size, bs=bs)

    elif version == 2010:
        model = get_transformer_model_v2010(section_len, feature_size, vocab_size, bs=bs)

    elif version == 2011:
        model = get_transformer_model_v2011(section_len, feature_size, vocab_size, bs=bs)

    elif version == 2012:
        model = get_transformer_model_v2012(section_len, feature_size, vocab_size, bs=bs)

    elif version == 2013:
        model = get_transformer_model_v2013(section_len, feature_size, vocab_size, bs=bs)

    elif version == 2014:
        model = get_transformer_model_v2014(section_len, feature_size, vocab_size, bs=bs)

    elif version == 2015:
        model = get_transformer_model_v2015(section_len, feature_size, vocab_size, bs=bs)

    elif version == 3000:
        model = get_transformer_model_v3000(section_len, feature_size, vocab_size, bs=bs)

    elif version == 3001:
        model = get_transformer_model_v3001(section_len, feature_size, vocab_size, bs=bs)

    elif version == 3002:
        model = get_transformer_model_v3002(section_len, feature_size, vocab_size, bs=bs)

    elif version == 3003:
        model = get_transformer_model_v3003(section_len, feature_size, vocab_size, bs=bs)

    elif version == 4000:
        model = get_transformer_model_v4000(section_len, feature_size, vocab_size, bs=bs)

    elif version == 4001:
        model = get_transformer_model_v4001(section_len, feature_size, vocab_size, bs=bs)

    elif version == 4002:
        model = get_transformer_model_v4002(section_len, feature_size, vocab_size, bs=bs)

    elif version == 4003:
        model = get_transformer_model_v4003(section_len, feature_size, vocab_size, bs=bs)

    elif version == 5000:
        model = get_transformer_model_v5000(section_len, feature_size, vocab_size, bs=bs)

    elif version == 5001:
        model = get_transformer_model_v5001(section_len, feature_size, vocab_size, bs=bs)

    elif version == 7001:
        model = get_transformer_model_v7001(section_len, feature_size, vocab_size, bs=bs)

    elif version == 7003:
        model = get_transformer_model_v7003(section_len, feature_size, vocab_size, bs=bs)


    if model is None:
        return None

    print("model: " + str(version))
    if lr is None:
        lr = 0.000001

    compile_model(model, lr)

    input_a = [section_len]

    temp = feature_size if isinstance(feature_size, list) or isinstance(feature_size, tuple) else [feature_size]

    if pre_train:
        input_a = tuple()
    else:
        for t in temp:
            input_a.append(t)
        input_a = tuple(input_a)

    build_bs = build_bs if build_bs is not None else 2

    model = build_workaround(model, [input_a, (section_len, vocab_size)], bs=build_bs)

    return model


def compile_model(model, lr):
    # model.compile(optimizer=Adam(learning_rate=lr), loss="categorical_crossentropy", metrics=[next_char])
    model.compile(optimizer=Adam(learning_rate=lr), loss="categorical_crossentropy")


def get_combined_model(version, vocab_size, section_len, image_shape=(160, 160, 1)):

    model = None
    if version == 0:
        model = get_combined_model_v0(vocab_size, section_len, image_shape)

    if model is None:
        return None

    model = build_workaround(model, [image_shape, (section_len, vocab_size)])

    return model


def get_combined_model_v0(vocab_size, section_len, image_shape):

    t_feature_size = 256
    video_shape = (section_len, ) + image_shape

    t_model = get_transformer_model_v0(section_len, t_feature_size, vocab_size)
    t_model = build_workaround(t_model, [(125, 256, 1), (125, 34)])
    t_model.summary()

    input = Input(video_shape)
    x = BatchNormalization()(input)
    x = Conv3D(64, (3, 3, 3), padding="same")(x)

    n = 1
    x = MyResidualConv2D([16 * n, 16 * n, 64 * n], 7, 3)(x)
    x = MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3)(x)

    n = 2
    x = MyResidualConv2D([16 * n, 16 * n, 64 * n], 5, 3)(x)
    x = MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3)(x)
    #
    # n = 2
    # x = MyResidualConv2D([16 * n, 16 * n, 64 * n], 4, 3)(x)
    # x = MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3)(x)

    n = 4
    x = MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2)(x)
    x = MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3)(x)
    #
    # n = 4
    # x = MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2)(x)
    # x = MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2)(x)

    x = Reshape((section_len, t_feature_size))(x)

    x = t_model(x)

    # model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))


    model = Model(inputs=[input], outputs=[x])
    # model.compile()
    model.summary()

    return model


def get_transformer_model_v0(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 64
    dense_dim = 128
    num_heads = 10

    num_enc = 4
    num_dec = 4

    transformer = TransformerPackage(embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v1(section_len, feature_size, vocab_size, bs=None):
    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 64
    dense_dim = 128
    num_heads = 10

    num_enc = 2
    num_dec = 2

    transformer = Transformer(embed_dim,
                              num_heads,
                              dense_dim,
                              section_len,
                              num_enc,
                              num_dec,
                              vocab_size,
                              feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v2(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 128
    dense_dim = 128
    num_heads = 2

    num_enc = 2
    num_dec = 2

    transformer = TransformerWithPreprocess(embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v3(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 256
    dense_dim = 256
    num_heads = 8

    num_enc = 2
    num_dec = 2

    transformer = TransformerWithPreprocess(embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer




def get_transformer_model_v4(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 512
    dense_dim = 512
    num_heads = 8

    num_enc = 6
    num_dec = 6

    transformer = TransformerWithPreprocess(embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v5(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 512
    dense_dim = 256
    num_heads = 6

    num_enc = 1
    num_dec = 1

    vision_net = get_vision_net(0, section_len)

    transformer = TransformerHeadless(vision_net,
                                      embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer



def get_transformer_model_v6(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 512
    dense_dim = 256
    num_heads = 6

    num_enc = 1
    num_dec = 1

    vision_net = get_vision_net(1, section_len)

    transformer = TransformerHeadless(vision_net,
                                      embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v7(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 512
    dense_dim = 256
    num_heads = 6

    num_enc = 1
    num_dec = 1

    vision_net = get_vision_net(3, section_len)

    transformer = TransformerHeadless(vision_net,
                                      embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer

def get_transformer_model_v9(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 256
    dense_dim = 256
    num_heads = 6

    num_enc = 2
    num_dec = 2

    vision_net = get_vision_net(4, section_len)

    transformer = TransformerHeadless(vision_net,
                                      embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer




def get_transformer_model_v12(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 256
    dense_dim = 256
    num_heads = 6

    num_enc = 3
    num_dec = 3

    vision_net = get_vision_net(5, section_len)

    transformer = TransformerHeadless(vision_net,
                                      embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer




def get_transformer_model_v16(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 256
    dense_dim = 256
    num_heads = 6

    num_enc = 2
    num_dec = 2

    vision_net = get_vision_net(8, section_len)

    transformer = TransformerHeadlessNoPos(vision_net,
                                      embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v18(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 256 * 2
    dense_dim = 256 * 2
    num_heads = 8

    num_enc = 4
    num_dec = 4

    vision_net = None

    transformer = TransformerHeadlessNoPos(vision_net,
                                      embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v24(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 256 * 2
    dense_dim = 256 * 2
    num_heads = 8

    num_enc = 4
    num_dec = 4

    vision_net = None

    transformer = TransformerHeadless(vision_net,
                                      embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v20(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 512
    dense_dim = 512
    num_heads = 8

    num_enc = 4
    num_dec = 4

    vision_net = None

    transformer = TransformerHeadlessNoPos(vision_net,
                                      embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer

def get_transformer_model_v21(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 512
    dense_dim = 512
    num_heads = 8

    num_enc = 4
    num_dec = 5

    vision_net = None

    transformer = TransformerHeadlessNoPos(vision_net,
                                      embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer

def get_transformer_model_v22(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 512
    dense_dim = 512
    num_heads = 8

    num_enc = 5
    num_dec = 4

    vision_net = None

    transformer = TransformerHeadlessNoPos(vision_net,
                                      embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v23(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 512
    dense_dim = 512
    num_heads = 8

    num_enc = 5
    num_dec = 5

    vision_net = None

    transformer = TransformerHeadlessNoPos(vision_net,
                                      embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v24(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 512
    dense_dim = 512
    num_heads = 8

    num_enc = 5
    num_dec = 6

    vision_net = None

    transformer = TransformerHeadlessNoPos(vision_net,
                                      embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v100(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 4

    num_enc = 4
    num_dec = 4

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v102(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 8

    num_enc = 4
    num_dec = 4

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer

def get_transformer_model_v2000(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 512
    dense_dim = 512
    num_heads = 8

    num_enc = 2
    num_dec = 2

    vision_net = 100

    transformer = TransformerHeadlessEmbPosMean(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v2001(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 4

    num_enc = 4
    num_dec = 4

    vision_net = 100

    transformer = TransformerHeadlessEmbSinPosMean(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v2002(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 4

    num_enc = 4
    num_dec = 4

    vision_net = 100

    transformer = TransformerHeadlessEmbPosCrossent(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v2003(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 4

    num_enc = 4
    num_dec = 4

    vision_net = 100

    transformer = TransformerHeadlessEmbSinPosCrossent(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v2004(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 4

    num_enc = 4
    num_dec = 4

    vision_net = 100

    transformer = TransformerHeadlessEmbPosMeanMaskLess(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v2005(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 4

    num_enc = 4
    num_dec = 4

    vision_net = 100

    transformer = TransformerHeadlessEmbSinPosMeanMaskLess(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v2006(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 4

    num_enc = 4
    num_dec = 4

    vision_net = 100

    transformer = TransformerHeadlessEmbPosCrossentMaskLess(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v2007(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 4

    num_enc = 4
    num_dec = 4

    vision_net = 100

    transformer = TransformerHeadlessEmbSinPosCrossentMaskLess(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer



def get_transformer_model_v2008(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512 * 2
    num_heads = 8

    num_enc = 6
    num_dec = 6

    vision_net = 100

    transformer = TransformerHeadlessEmbSinPosMean(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v2009(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512 * 2
    num_heads = 4

    num_enc = 6
    num_dec = 6

    vision_net = 100

    transformer = TransformerHeadlessEmbSinPosMean(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v2010(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512 * 2
    num_heads = 8

    num_enc = 4
    num_dec = 4

    vision_net = 100

    transformer = TransformerHeadlessEmbSinPosMean(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v2011(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 8

    num_enc = 6
    num_dec = 6

    vision_net = 100

    transformer = TransformerHeadlessEmbSinPosMean(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer



def get_transformer_model_v2012(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512 * 2
    num_heads = 8

    num_enc = 6
    num_dec = 6

    vision_net = 200

    transformer = TransformerHeadlessEmbSinPosMean(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer





def get_transformer_model_v2013(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512 * 2
    num_heads = 8

    num_enc = 6
    num_dec = 6

    vision_net = 201

    transformer = TransformerHeadlessEmbSinPosMean(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v2014(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512 * 2
    num_heads = 8

    num_enc = 6
    num_dec = 6

    vision_net = 202

    transformer = TransformerHeadlessEmbSinPosMean(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v2015(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 4

    num_enc = 4
    num_dec = 4

    vision_net = 203

    transformer = TransformerHeadlessEmbSinPosMean(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v5000(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 6

    num_enc = 6
    num_dec = 6

    transformer = MyBaseTransformer(model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer

def get_transformer_model_v5001(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 6

    num_enc = 4
    num_dec = 4

    transformer = MyBaseTransformer(model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer

def get_transformer_model_v3000(section_len, feature_size, vocab_size, bs):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 6

    num_enc = 6
    num_dec = 6

    vision_net = 203
    num_tokens_per_batch = bs * section_len

    transformer = MySwitchTransformer(vision_net_id=None,
                                      num_tokens_per_batch=num_tokens_per_batch,
                                      model_dim=model_dim,
                                      key_dim=key_dim,
                                      num_heads=num_heads,
                                      feed_forward_dim=dense_dim,
                                      seq_len=section_len,
                                      num_layers_enc=num_enc,
                                      num_layers_dec=num_dec,
                                      num_classes=vocab_size
                                      )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v3001(section_len, feature_size, vocab_size, bs):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 6

    num_enc = 6
    num_dec = 6

    num_tokens_per_batch = bs * section_len

    transformer = MySwitchTransformer(vision_net_id=None,
                                      num_tokens_per_batch=num_tokens_per_batch,
                                      model_dim=model_dim,
                                      key_dim=key_dim,
                                      num_heads=num_heads,
                                      feed_forward_dim=dense_dim,
                                      seq_len=section_len,
                                      num_layers_enc=num_enc,
                                      num_layers_dec=num_dec,
                                      num_classes=vocab_size
                                      )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v7001(section_len, feature_size, vocab_size, bs):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 6

    num_enc = 6
    num_dec = 6

    num_tokens_per_batch = bs * section_len

    transformer = MySwitchTransformerTail(vision_net_id=None,
                                      num_tokens_per_batch=num_tokens_per_batch,
                                      model_dim=model_dim,
                                      key_dim=key_dim,
                                      num_heads=num_heads,
                                      feed_forward_dim=dense_dim,
                                      seq_len=section_len,
                                      num_layers_enc=num_enc,
                                      num_layers_dec=num_dec,
                                      num_classes=vocab_size
                                      )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v3002(section_len, feature_size, vocab_size, bs):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 6

    num_enc = 6
    num_dec = 6

    vision_net = 203
    num_tokens_per_batch = bs * section_len

    transformer = MySwitchTransformer(vision_net_id=None,
                                      num_tokens_per_batch=num_tokens_per_batch,
                                      model_dim=model_dim,
                                      key_dim=key_dim,
                                      num_heads=num_heads,
                                      feed_forward_dim=dense_dim,
                                      seq_len=section_len,
                                      num_layers_enc=num_enc,
                                      num_layers_dec=num_dec,
                                      num_classes=vocab_size
                                      )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v3003(section_len, feature_size, vocab_size, bs):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 6

    num_enc = 4
    num_dec = 4

    num_tokens_per_batch = bs * section_len

    transformer = MySwitchTransformer(vision_net_id=None,
                                      num_tokens_per_batch=num_tokens_per_batch,
                                      model_dim=model_dim,
                                      key_dim=key_dim,
                                      num_heads=num_heads,
                                      feed_forward_dim=dense_dim,
                                      seq_len=section_len,
                                      num_layers_enc=num_enc,
                                      num_layers_dec=num_dec,
                                      num_classes=vocab_size
                                      )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v7003(section_len, feature_size, vocab_size, bs):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 6

    num_enc = 4
    num_dec = 4

    num_tokens_per_batch = bs * section_len

    transformer = MySwitchTransformerTail(vision_net_id=None,
                                      num_tokens_per_batch=num_tokens_per_batch,
                                      model_dim=model_dim,
                                      key_dim=key_dim,
                                      num_heads=num_heads,
                                      feed_forward_dim=dense_dim,
                                      seq_len=section_len,
                                      num_layers_enc=num_enc,
                                      num_layers_dec=num_dec,
                                      num_classes=vocab_size
                                      )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v4000(section_len, feature_size, vocab_size, bs):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 6

    num_enc = 6
    num_dec = 6

    vision_net = 203

    transformer = MyExplicitSwitchTransformerWorkaround(vision_net_id=None,
                                                        model_dim=model_dim,
                                      key_dim=key_dim,
                                      num_heads=num_heads,
                                      feed_forward_dim=dense_dim,
                                      seq_len=section_len,
                                      num_layers_enc=num_enc,
                                      num_layers_dec=num_dec,
                                      num_classes=vocab_size
                                      )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v4001(section_len, feature_size, vocab_size, bs):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 6

    num_enc = 6
    num_dec = 6

    vision_net = 203

    transformer = MyExplicitSwitchTransformerWorkaroundTail(vision_net_id=None,
                                                            model_dim=model_dim,
                                      key_dim=key_dim,
                                      num_heads=num_heads,
                                      feed_forward_dim=dense_dim,
                                      seq_len=section_len,
                                      num_layers_enc=num_enc,
                                      num_layers_dec=num_dec,
                                      num_classes=vocab_size
                                      )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer

def get_transformer_model_v4002(section_len, feature_size, vocab_size, bs):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 6

    num_enc = 4
    num_dec = 4

    vision_net = 203

    transformer = MyExplicitSwitchTransformerWorkaround(vision_net_id=None,
                                                        model_dim=model_dim,
                                      key_dim=key_dim,
                                      num_heads=num_heads,
                                      feed_forward_dim=dense_dim,
                                      seq_len=section_len,
                                      num_layers_enc=num_enc,
                                      num_layers_dec=num_dec,
                                      num_classes=vocab_size
                                      )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v4003(section_len, feature_size, vocab_size, bs):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 6

    num_enc = 4
    num_dec = 4

    vision_net = 203

    transformer = MyExplicitSwitchTransformerWorkaroundTail(vision_net_id=None,
                                                            model_dim=model_dim,
                                      key_dim=key_dim,
                                      num_heads=num_heads,
                                      feed_forward_dim=dense_dim,
                                      seq_len=section_len,
                                      num_layers_enc=num_enc,
                                      num_layers_dec=num_dec,
                                      num_classes=vocab_size
                                      )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer



def get_transformer_model_v1000(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512 * 2
    num_heads = 4

    num_enc = 6
    num_dec = 6

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer




def get_transformer_model_v1001(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512 * 2
    num_heads = 6

    num_enc = 6
    num_dec = 6

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer



def get_transformer_model_v1002(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512 * 2
    num_heads = 8

    num_enc = 6
    num_dec = 6

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer



def get_transformer_model_v1003(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 32
    dense_dim = 512 * 2
    num_heads = 4

    num_enc = 6
    num_dec = 6

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v1004(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 32
    dense_dim = 512 * 2
    num_heads = 8

    num_enc = 6
    num_dec = 6

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v1005(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 128
    dense_dim = 512 * 2
    num_heads = 4

    num_enc = 6
    num_dec = 6

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v1006(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 128
    dense_dim = 512 * 2
    num_heads = 8

    num_enc = 6
    num_dec = 6

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v1007(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512 * 2
    num_heads = 4

    num_enc = 4
    num_dec = 4

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer



def get_transformer_model_v1008(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512 * 2
    num_heads = 2

    num_enc = 4
    num_dec = 4

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v1009(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 128
    dense_dim = 512 * 2
    num_heads = 2

    num_enc = 4
    num_dec = 4

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer




def get_transformer_model_v1010(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 512
    dense_dim = 512 * 2
    num_heads = 2

    num_enc = 4
    num_dec = 4

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer

def get_transformer_model_v1011(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 4

    num_enc = 6
    num_dec = 6

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer




def get_transformer_model_v1012(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 6

    num_enc = 6
    num_dec = 6

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer



def get_transformer_model_v1013(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 8

    num_enc = 6
    num_dec = 6

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer



def get_transformer_model_v1014(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 32
    dense_dim = 512
    num_heads = 4

    num_enc = 6
    num_dec = 6

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v1015(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 32
    dense_dim = 512
    num_heads = 8

    num_enc = 6
    num_dec = 6

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v1016(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 128
    dense_dim = 512
    num_heads = 4

    num_enc = 6
    num_dec = 6

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v1017(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 128
    dense_dim = 512
    num_heads = 8

    num_enc = 6
    num_dec = 6

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v1018(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 4

    num_enc = 4
    num_dec = 4

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer



def get_transformer_model_v1019(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 2

    num_enc = 4
    num_dec = 4

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v1020(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 128
    dense_dim = 512
    num_heads = 2

    num_enc = 4
    num_dec = 4

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer




def get_transformer_model_v1021(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 512
    dense_dim = 512
    num_heads = 2

    num_enc = 4
    num_dec = 4

    vision_net = 100

    transformer = TransformerHeadlessPosVisionTrain(vision_net_id=vision_net,
                                                    model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v200(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512 * 4
    num_heads = 8

    num_enc = 6
    num_dec = 6


    transformer = TransformerHeadlessPos(model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size,
                                                    feature_size=feature_size,
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v201(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512 * 4
    num_heads = 8

    num_enc = 7
    num_dec = 5


    transformer = TransformerHeadlessPos(model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size,
                                                    feature_size=feature_size,
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer



def get_transformer_model_v202(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512 * 4
    num_heads = 8

    num_enc = 5
    num_dec = 7


    transformer = TransformerHeadlessPos(model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size,
                                                    feature_size=feature_size,
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer



def get_transformer_model_v203(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512 * 4
    num_heads = 8

    num_enc = 7
    num_dec = 6


    transformer = TransformerHeadlessPos(model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size,
                                                    feature_size=feature_size,
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v204(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512 * 4
    num_heads = 8

    num_enc = 6
    num_dec = 7


    transformer = TransformerHeadlessPos(model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size,
                                                    feature_size=feature_size,
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v205(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    model_dim = 512
    key_dim = 64
    dense_dim = 512 * 4
    num_heads = 8

    num_enc = 5
    num_dec = 5


    transformer = TransformerHeadlessPos(model_dim=model_dim,
                                                    key_dim=key_dim,
                                                    num_heads=num_heads,
                                                    feed_forward_dim=dense_dim,
                                                    seq_len=section_len,
                                                    num_layers_enc=num_enc,
                                                    num_layers_dec=num_dec,
                                                    num_classes=vocab_size,
                                                    feature_size=feature_size,
                                                    )

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer



def get_transformer_model_v101(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 512
    dense_dim = 512
    num_heads = 4

    num_enc = 2
    num_dec = 2

    vision_net = 9

    transformer = TransformerHeadlessPosVisionTrain(vision_net,
                                      embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v26(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 512
    dense_dim = 512
    num_heads = 8

    num_enc = 6
    num_dec = 6

    vision_net = None

    transformer = TransformerHeadlessNoPos(vision_net,
                                      embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v27(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 256 * 2
    dense_dim = 256 * 2
    num_heads = 4

    num_enc = 2
    num_dec = 2

    transformer = TransformerHeadlessNoPosVisionTrain(9,
                                      embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v17(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 256 * 2
    dense_dim = 256 * 2
    num_heads = 4

    num_enc = 1
    num_dec = 1

    vision_net = get_vision_net(9, section_len)

    transformer = TransformerHeadlessNoPos(vision_net,
                                      embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer

def get_transformer_model_v15(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 256
    dense_dim = 256
    num_heads = 6

    num_enc = 2
    num_dec = 2

    vision_net = get_vision_net(8, section_len)

    transformer = TransformerHeadless(vision_net,
                                      embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer

def get_transformer_model_v14(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 256
    dense_dim = 256
    num_heads = 6

    num_enc = 3
    num_dec = 3

    vision_net = get_vision_net(7, section_len)

    transformer = TransformerHeadless(vision_net,
                                      embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v13(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 256
    dense_dim = 256
    num_heads = 6

    num_enc = 3
    num_dec = 3

    vision_net = get_vision_net(6, section_len)

    transformer = TransformerHeadless(vision_net,
                                      embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def get_transformer_model_v11(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 256
    dense_dim = 256
    num_heads = 6

    num_enc = 3
    num_dec = 3

    vision_net = get_vision_net(4, section_len)

    transformer = TransformerHeadless(vision_net,
                                      embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer

def get_transformer_model_v10(section_len, feature_size, vocab_size, bs=None):

    # seq_len = 125
    # feature_size = 256
    # vocab_size = 34

    embed_dim = 256
    dense_dim = 256
    num_heads = 6

    num_enc = 2
    num_dec = 2

    vision_net = get_vision_net(4, section_len)

    transformer = TransformerHeadless(vision_net,
                                      embed_dim,
                                     num_heads,
                                     dense_dim,
                                     section_len,
                                     num_enc,
                                     num_dec,
                                     vocab_size,
                                     feature_size)

    # in_enc = Input((seq_len, feature_size, 1))
    # in_dec = Input(seq_len, vocab_size)

    # transformer.compile(optimizer=Adam(learning_rate=0.0000001), loss="categorical_crossentropy")
    # transformer.build([(None125, 256, 1), (125, 34)])
    return transformer


def build_workaround(model, input_shape, bs):
    print("build with bs: ", str(bs))

    x = get_random_data(input_shape[0], bs=bs, ones=True)
    y = get_random_target_data(input_shape[1], bs=bs, ones=True)

    print(x.shape)
    print(y.shape)

    p = model.predict({"source": x, "target": y}, batch_size=bs)
    # model.summary()

    return model

def read_data(bs, seq_len):
    data_path = {"german": """/media/ubuntu/data/combined/german""",
                 "english": """/media/ubuntu/data/combined/english_small"""}
    # data_path = """/media/ubuntu/data/combined/pre_processed"""
    ldr = LipDataReader(data_path, batch_size=bs, pre_processed=False, max_text_len=seq_len, max_input_len=seq_len)

    return ldr.create_train_dataset()


def val_test(model, input_shape, bs):
    print("build with bs: ", str(bs))

    x = get_random_data(input_shape[0], bs=bs, ones=True)
    y = get_random_target_data(input_shape[1], bs=bs, ones=True)

    x, y = read_data(bs, input_shape[0][0])

    print(x.shape)
    print(y.shape)

    p = model.predict({"source": x, "target": y})
    gen = model.calc_cer(x, y, 1)
    print("prediction", p)
    model.summary()


def get_random_data(shape, bs=2, ones=False):
    target_shape = [bs]
    for s in shape:
        target_shape.append(s)
    target_shape = tuple(target_shape)

    if ones:
        return np.ones(target_shape)
    else:
        return np.random.randint(255, size=target_shape) / 255

def get_random_target_data(shape, bs=2, ones=False):
    seq_size, v_size = shape
    target_shape = (bs, seq_size)

    if ones:
        values = np.ones(target_shape)
    else:
        values = np.random.randint(0, v_size, seq_size*bs, dtype=np.int32)
        values = np.reshape(values, newshape=target_shape)
    return values


def blub():
    bs = 2
    feature_size = (100, 100, 1)
    seq_len = 25*3
    var_size = 32

    in_shape_a = [seq_len]
    for t in feature_size:
        in_shape_a.append(t)
    in_shape_a = tuple(in_shape_a)

    in_shape_b = (seq_len, var_size)
    in_shape = (in_shape_a, in_shape_b)


    m = get_transformer_model(3000, seq_len, feature_size, var_size, build_bs=bs, bs=bs)
    # val_test(m, bs=bs, input_shape=in_shape)

if __name__ == '__main__':
    blub()
