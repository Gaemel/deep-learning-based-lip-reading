from tensorflow.keras.layers import Input, Embedding, MultiHeadAttention, Layer, LayerNormalization, Dense, Conv1D, Reshape, MaxPooling3D, Flatten, BatchNormalization, Conv3D, Conv2D, MaxPooling2D, GlobalMaxPool2D
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.metrics import Mean
import tensorflow as tf
from residual_conv import MyResidualIdentityConv2D, MyResidualConv2D
from tensorflow.keras.callbacks import Callback
import numpy as np
from vision_nets import get_vision_net
from MyTransformer import TransformerSelfAttention, PositionalEmbedding
from MyLayers import BorderDrop3D

class ResidualFeedForward(Layer):

    def __init__(self, dense_dim=4, output_dim=256, **kwargs):
        super().__init__(**kwargs)

        self.ff = Sequential()
        if not isinstance(dense_dim, list):
            dense_dim = [dense_dim]
        for dd in dense_dim:
            self.ff.add(Dense(dd, activation=tf.nn.gelu))

        self.ff.add(Dense(output_dim, activation="linear"))

        self.norm = LayerNormalization()

    def call(self, inputs):
        a = self.ff(inputs)
        a = self.norm(inputs + a)
        return a


def some_test_0():
    model_dim = 512
    num_heads = 8
    dropout_rate = 0.1
    dense_dim = 512
    embed_dim = int(dense_dim / num_heads)
    seq_len = 40

    n_of_emb = 6

    in_a = Input(shape=[seq_len, dense_dim])
    # enc_pos_emb = PositionalEmbedding(seq_len, embed_dim, name="PositionalEmbeddingEncoder")(in_a)

    temp = in_a
    for n in range(n_of_emb):
        temp = MultiHeadAttention(num_heads=num_heads, key_dim=int(model_dim/num_heads), dropout=dropout_rate)(temp, temp)
        # temp = LayerNormalization()(temp)
        # temp = ResidualFeedForward(dense_dim, embed_dim)(temp)

    m = Model(inputs=[in_a], outputs=[temp])

    m.summary()


def some_test_2():
    model = Sequential()
    model.add(Input(shape=[40, 512]))
    model.add(Encoder_test(num_heads=8, key_dim=64))

    model.summary()


def some_test_3():
    embed_dim = 512
    seq_len = 40
    n_of_heads = 8

    layer = Encoder_test(num_heads=n_of_heads, key_dim=int(embed_dim / n_of_heads))
    target = tf.keras.Input(shape=[seq_len, embed_dim])
    # output_tensor, weights = layer(target, target, return_attention_scores=True)

    a = layer(target)
    print(target)
    print(a)


class Encoder_test(Layer):

    def __init__(self, key_dim=64, num_heads=2, dropout_rate=0.1, use_mask=False, **kwargs):
        super().__init__(**kwargs)

        self.key_dim = key_dim
        self.num_heads = num_heads

        self.attention = MultiHeadAttention(num_heads=num_heads, key_dim=key_dim, dropout=dropout_rate)
        self.norm = LayerNormalization()
        self.use_mask = use_mask

    def call(self, inputs):
        # print("inputs", inputs.shape)
        mask = None
        if self.use_mask:
            batch_size = inputs.shape[0]
            #
            # if batch_size is None:
            #     batch_size = 10

            seq_len = inputs.shape[1]
            mask = self.get_maks(batch_size, seq_len, seq_len)

        # print("in_shape", inputs.shape)
        att_output = self.attention(inputs, inputs, attention_mask=mask)
        normed_1 = self.norm(inputs + att_output)
        # print("att_out", att_output.shape)
        # print("normed_1", normed_1.shape)

        return normed_1

    def get_maks(self, batch_size, n_dest, n_src, dtype=tf.bool):
        """Masks the upper half of the dot product matrix in self attention.

        This prevents flow of information from future tokens to current token.
        1's in the lower triangle, counting from the lower right corner.
        """

        """

        something like this:

        1 0 0 0
        1 1 0 0
        1 1 1 0
        1 1 1 1

        """

        i = tf.range(n_dest)[:, None]
        j = tf.range(n_src)
        m = i >= j - n_src + n_dest
        mask = tf.cast(m, dtype)
        # print("mask", mask)
        return mask

        # mask = tf.reshape(mask, [1, n_dest, n_src])
        # print("mask 1", mask)
        # mult = tf.concat(
        #     [tf.expand_dims(batch_size, -1), tf.constant([1, 1], dtype=tf.int32)], 0
        # )
        # print("mult 2", mult)
        # temp = tf.tile(mask, mult)
        # print("temp", temp)
        # return temp


def next_test():
    layer = MultiHeadAttention(num_heads=8, key_dim=64)
    layer_1 = MultiHeadAttention(num_heads=8, key_dim=64)
    layer_2 = MultiHeadAttention(num_heads=8, key_dim=64)
    target = tf.keras.Input(shape=[40, 512])
    output_tensor, weights = layer(target, target, return_attention_scores=True)

    a = layer(target, target)
    print(a)
    b = layer_1(a, a)
    print(b)
    c = layer_2(b, b)
    print(c)
    print(output_tensor.shape)
    print(weights.shape)
    print(target)



if __name__ == '__main__':
    some_test_2()