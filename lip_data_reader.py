import numpy as np
import os
import yaml
import tensorflow as tf
import cv2
# import tensorflow_io as tfio

class VectorizeChar:

    START_TOKEN = 1
    END_TOKEN = 2

    def __init__(self, max_len=50):
        self.vocab = (
            [" "]
            + ["<",  ">"]
            + [chr(i + 96) for i in range(1, 27)]
        )

        for x in range(10):
            self.vocab.append(str(x))

        self.max_len = max_len
        self.char_to_idx = {}
        for i, ch in enumerate(self.vocab):
            self.char_to_idx[ch] = i

        self._replace = (("ä", "ae"), ("ö", "oe"), ("ü", "ue"), ("ß", "ss"), ("€", "euro"), ("é", "e"))

    def replace_unknown(self, some_str, replacement=" "):
        new_str = ""

        for c in some_str:
            if c in self.vocab:
                new_str += c
            else:
                new_str += replacement

        return new_str

    def get_start_token(self):
        return self.START_TOKEN

    def get_end_token(self):
        return self.END_TOKEN

    def __call__(self, text:str):
        text = text.lower()

        for r_from, r_to in self._replace:
            text = text.replace(r_from, r_to)

        text = self.replace_unknown(text, " ")

        text = text[: self.max_len - 2]
        text = "<" + text + ">"
        pad_len = self.max_len - len(text)
        return [self.char_to_idx.get(ch, 1) for ch in text] + [0] * pad_len

    @staticmethod
    def get_vocabulary():
        return VectorizeChar().vocab

    def translate_vector(self, vector):
        some_str = ""

        for v in vector:
            some_str += self.vocab[v]
            if some_str[-1] == ">":
                break

        return some_str

    def list_token(self):
        return self.vocab


def load_scene(target_file):
    cap = cv2.VideoCapture(target_file)
    scene = list()

    while True:

        s, f = cap.read()
        if not s:
            break
        scene.append(f)

    cap.release()
    return np.array(scene)


class LipDataReader():

    def __init__(self, data_path, batch_size=None, max_text_len=25*5, max_input_len=25*5, test_data_size=200, pre_processed=False):
        if isinstance(data_path, list) or isinstance(data_path, tuple):
            temp = data_path
            data_path = dict()
            for i, dp in enumerate(temp):
                data_path[str(i)] = dp
        elif not isinstance(data_path, dict):
            data_path = {"0": data_path}

        self._data_path = data_path
        self._vectorizer = VectorizeChar(max_text_len)
        self._test_data_size = test_data_size
        self._test_data_paths = None
        self._train_data_paths = None
        self._batch_index = None
        self._batch_size = batch_size
        self._max_input_len = max_input_len
        self._data_pre_processed = pre_processed

        self._section_len = None
        self._feature_shape = None
        self._vocab_size = None

    def __force_shape_load(self):
        temp_batch_size = self._batch_size
        self._batch_size = 1
        self.create_train_dataset()
        for key in self._batch_index.keys():
            self._batch_index[key] -= 1
        self._batch_size = temp_batch_size

    def get_vocab_size(self):
        return len(self.get_vocab())

    def get_section_len(self):
        if self._section_len is None:
            self.__force_shape_load()

        return self._section_len

    def get_feature_size(self):
        if self._feature_shape is None:
            self.__force_shape_load()

        return self._feature_shape

    def get_vectorizer(self):
        return self._vectorizer

    def get_vocab(self):
        return self._vectorizer.get_vocabulary()

    # def save_batches(self, target_dir, start_batch):
    #     last_batch_index = None
    #     i = start_batch
    #     while True:
    #
    #         batch_x, batch_y = self.create_train_dataset()
    #
    #         if last_batch_index is not None and last_batch_index > self._batch_index:
    #             break
    #
    #         last_batch_index = self._batch_index
    #
    #         batch_path_x = os.path.join(target_dir, "batch_" + str(i) + "_x" + ".npy")
    #         batch_path_y = os.path.join(target_dir, "batch_" + str(i) + "_y" + ".npy")
    #
    #         with open(batch_path_x, 'wb') as f:
    #             np.save(f, batch_x)
    #
    #         with open(batch_path_y, 'wb') as f:
    #             np.save(f, batch_y)
    #         del batch_x
    #         del batch_y
    #
    #         print("saved batch: " + str(i))
    #         i += 1
    #
    #     batch_x, batch_y = self.create_test_dataset()
    #
    #     batch_path_x = os.path.join(target_dir, "test_x" + ".npy")
    #     batch_path_y = os.path.join(target_dir, "test_y" + ".npy")
    #
    #     with open(batch_path_x, 'wb') as f:
    #         np.save(f, batch_x)
    #
    #     with open(batch_path_y, 'wb') as f:
    #         np.save(f, batch_y)
    #
    #     print("saved test_data")

    def _create_data_split(self):
        paths = self._prepare_data()

        self._test_data_paths = dict()
        self._train_data_paths = dict()

        self._batch_index = dict()
        for key in paths:
            p = paths[key]
            np.random.seed(1)
            np.random.shuffle(p)
            split = self._test_data_size

            self._test_data_paths[key] = p[:split]
            self._train_data_paths[key] = p[split:]
            self._batch_index[key] = 0

    def _prepare_data(self, parent_dir=None, update=False):
        if update:

            if isinstance(parent_dir, list) or isinstance(parent_dir, tuple):
                temp = parent_dir
                parent_dir = dict()
                for i, pd in enumerate(temp):
                    parent_dir[str(i)] = pd

            elif not isinstance(parent_dir, dict):
                parent_dir = {"0": parent_dir}

            self._data_path = parent_dir

        if parent_dir is None:
            parent_dir = self._data_path

        id_dict = dict()
        for key in parent_dir:
            print(key)
            id_list = list()
            for sub in os.listdir(parent_dir[key]):
                temp_path = os.path.join(parent_dir[key], sub)

                if os.path.isdir(temp_path):
                    id_list += self._prepare_data(temp_path, update=False)["0"]
                elif os.path.splitext(temp_path)[-1] == ".txt":
                    # id_list += self._prepare_indivitual_data(temp_path)
                    temp = self._prepare_indivitual_data(temp_path)
                    if temp is not None:
                        id_list += temp
                # # remove
                # if len(id_list) > 512 * 4:
                #     break

            id_dict[key] = id_list

            # # remove
            # if len(id_dict[key]) > 512 * 4:
            #     break

        return id_dict

    def _prepare_indivitual_data(self, target, inclued_confidence=True):
        if isinstance(target, list):
            res = list()
            for ele in target:
                temp = self._prepare_indivitual_data(ele)
                if temp is not None:
                    res += temp
            return res
        else:
            if str(target).endswith("done.txt"):
                return None
            text_path = target
            if self._data_pre_processed:
                video_path = os.path.splitext(target)[0] + "_pp.npy"
            else:
                video_path = os.path.splitext(target)[0] + ".mp4"

            if not os.path.exists(video_path):
                return None
            if not os.path.exists(text_path):
                return None

            try:
                with open(text_path) as f:
                    text_dict = yaml.load(f, Loader=yaml.FullLoader)
            except Exception as e:
                text_dict = None

            if text_dict is None:
                return None

            text = str(text_dict["Text"])

            if inclued_confidence:
                conf = text_dict["Conf"]

                return [(text, video_path, conf)]
            else:
                return [(text, video_path)]

    def _add_padding(self, video, padding_len):
        if isinstance(video, tuple) or isinstance(video, list):
            for n in range(len(video)):
                video[n] = self._add_padding(video[n], padding_len)
            return video

        padding = np.zeros((padding_len, video.shape[1], video.shape[2]))

        video = np.concatenate((video, padding))
        video = video[:padding_len]
        return video

    def create_text_ds(self, texts):
        text_ds = [self._vectorizer(t) for t in texts]
        # text_ds = tf.data.Dataset.from_tensor_slices(text_ds)
        text_ds = np.array(text_ds)
        return text_ds

    def __path_to_video(self, path):
        # video = tf.io.read_file(path)
        # video = tfio.experimental.ffmpeg.decode_video(
        #     video, index=0, name=None
        # )
        video = load_scene(path)
        print(video.shape)
        # video = tf.squeeze(video, axis=-1)
        pad_len = 25 * 10
        padding = tf.constant([[0, pad_len], [0, 0]])
        x = tf.pad(video, padding, "CONSTANT")[:pad_len, :]
        return x

    def create_video_ds(self, video_path):

        if isinstance(video_path, list) or isinstance(video_path, tuple):
            res = list()
            for vp in video_path:
                res += self.create_video_ds(vp)
            return res

        if self._data_pre_processed:
            return self.load_pre_processed_data(video_path)
        else:
            return self.load_video(video_path)

    def load_pre_processed_data(self, target_path):
        if isinstance(target_path, list) or isinstance(target_path, tuple):
            res = list()
            for t in target_path:
                res += self.load_pre_processed_data(t)
            return res

        return [np.load(target_path)]

    @staticmethod
    def load_video(video_path):
        if isinstance(video_path, list) or isinstance(video_path, tuple):
            res = list()
            for vp in video_path:
                res += LipDataReader.load_video(vp)
            return res

        cap = cv2.VideoCapture(video_path)
        scene = list()

        while True:
            s, f = cap.read()
            if not s:
                break
            scene.append(cv2.cvtColor(f, cv2.COLOR_BGR2GRAY) / 255)
        cap.release()

        return [np.array(scene)]

    def _get_test_data(self, key=None, as_dict=False):
        if self._test_data_paths is None:
            self._create_data_split()

        if as_dict:
            data = dict()
            for key in self._test_data_paths:
                data[key] = self._get_test_data(key, as_dict=False)

            return data

        if key is None:
            data = list()
            for key in self._test_data_paths:
                data += self._test_data_paths[key]
            return data
        else:
            return self._test_data_paths[key]

    def _get_next_batch(self, keys=None):
        if self._train_data_paths is None:
            self._create_data_split()

        if keys is None:
            keys = list(self._train_data_paths.keys())
        elif not isinstance(keys, list):
            keys = [keys]

        if self._batch_size is None:
            batch = list()

            for k in keys:
                batch += self._train_data_paths[k]

            batch = self.weave_lists([batch])
            return batch

        else:

            batch = list()
            temp_bs = int(self._batch_size / len(self._batch_index.keys()))

            for k in keys:

                if self._batch_index[k] + temp_bs < len(self._train_data_paths[k]):
                    temp_batch = self._train_data_paths[k][self._batch_index[k]: self._batch_index[k]+temp_bs]
                    self._batch_index[k] += temp_bs
                    batch.append(temp_batch)

                else:
                    temp_batch = self._train_data_paths[k][self._batch_index[k]:]
                    self._batch_index[k] = temp_bs - len(temp_batch)
                    temp_batch += self._train_data_paths[k][:self._batch_index[k]]
                    batch.append(temp_batch)

                print("new index for " + str(k) + ": " + str(self._batch_index[k]))

            batch = self.weave_lists(batch)

            return batch

    def weave_lists(self, some_lists):
        res = list()
        for i in range(len(some_lists[0])):
            for list_index, l in enumerate(some_lists):
                # temp = tuple([list_index] + list(l[i]))
                temp = l[i]
                res.append(temp)

        return res

    def _expand_dim(self, ds):
        return np.expand_dims(ds, axis=-1)

    def __pack_ds(self, data_package):
        if isinstance(data_package, dict):

            data = dict()
            for key in data_package.keys():
                data[key] = self.__pack_ds(data_package[key])

            return data

        else:
            temp = tuple(zip(*data_package))
            text, video_path, conf = temp
            text_ds = self.create_text_ds(text)

            video_ds = self.create_video_ds(video_path)

            if not self._data_pre_processed:
                video_ds = self._add_padding(video_ds, self._max_input_len)
                video_ds = np.array(video_ds)
                video_ds = self._expand_dim(video_ds)
            else:
                video_ds = np.array(video_ds)
                video_ds = np.squeeze(video_ds)

            # ds = tf.data.Dataset.zip((video_ds, text_ds))
            # ds = ds.map(lambda x, y: {"source": x, "target": y})
            # ds = ds.batch(ds)

            return video_ds, text_ds

    def __load_shapes(self, data):
        if isinstance(data, dict):
            self.__load_shapes(data[list(data.keys())[0]])
        else:
            if self._feature_shape is None:
                self._feature_shape = data[0].shape[2:]

            if self._section_len is None:
                self._section_len = data[0].shape[1]

    def create_train_dataset(self):
        batch_data = self._get_next_batch()
        batch_data = self.__pack_ds(batch_data)
        self.__load_shapes(batch_data)
        return batch_data

    def create_test_dataset(self, as_dict=False):
        data = self._get_test_data(as_dict=as_dict)
        data = self.__pack_ds(data)
        self.__load_shapes(data)
        return data

    def set_batch(self, batch, key):
        self._batch_index[key] = batch * self._batch_size

    def translate_vector(self, vector):
        return self._vectorizer.translate_vector(vector)
#
# class DisplayOutputs(keras.callbacks.Callback):
#     def __init__(
#         self, batch, idx_to_token, target_start_token_idx=27, target_end_token_idx=28
#     ):
#         """Displays a batch of outputs after every epoch
#
#         Args:
#             batch: A test batch containing the keys "source" and "target"
#             idx_to_token: A List containing the vocabulary tokens corresponding to their indices
#             target_start_token_idx: A start token index in the target vocabulary
#             target_end_token_idx: An end token index in the target vocabulary
#         """
#         self.batch = batch
#         self.target_start_token_idx = target_start_token_idx
#         self.target_end_token_idx = target_end_token_idx
#         self.idx_to_char = idx_to_token
#
#     def on_epoch_end(self, epoch, logs=None):
#         if epoch % 5 != 0:
#             return
#         source = self.batch["source"]
#         target = self.batch["target"].numpy()
#         bs = tf.shape(source)[0]
#         preds = self.model.generate(source, self.target_start_token_idx)
#         preds = preds.numpy()
#         for i in range(bs):
#             target_text = "".join([self.idx_to_char[_] for _ in target[i, :]])
#             prediction = ""
#             for idx in preds[i, :]:
#                 prediction += self.idx_to_char[idx]
#                 if idx == self.target_end_token_idx:
#                     break
#             print(f"target:     {target_text.replace('-','')}")
#             print(f"prediction: {prediction}\n")




def save_data_to_file(dir_in, dir_out, start_batch=0):
    ldr = LipDataReader(dir_in, batch_size=400)
    ldr.set_batch(start_batch)
    ldr.save_batches(dir_out, start_batch)
    print("done")


def testing():
    data_path = {"german": """/media/ubuntu/data/combined/german_cropped_pre""", "english": """/media/ubuntu/data/combined/english_cropped_pre"""}
    # data_path = """/media/ubuntu/data/combined/pre_processed"""
    ldr = LipDataReader(data_path, batch_size=10, pre_processed=True, max_text_len=75, max_input_len=75)
    train_x, train_y = ldr.create_train_dataset()
    test_x, test_y = ldr.create_test_dataset()
    print(len(train_x), len(train_y))
    for y in train_y:
        print(ldr.translate_vector(y))


    print()


def list_tokens():
    print(VectorizeChar().list_token())

if __name__ == '__main__':
    list_tokens()
