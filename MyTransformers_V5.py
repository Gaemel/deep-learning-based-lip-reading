from MyTransformers_v3 import Router, create_feedforward_network
import numpy as np
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.metrics import Mean, CategoricalCrossentropy
from MyTransformer_v2 import TransformerSelfAttention, TransformerOutputEncoding, DecoderEncoderAttention, TransformerEncoder
from MyTransformer_v4 import TransformerBaseMean
from my_trig_pos_embd import TrigPosEmbedding

from vision_nets import get_vision_net


def some_test_0():
    bs = 100
    seq_len = 3
    num_tokens_per_batch = bs * seq_len
    num_experts = 2
    embed_dim = 10
    expert_capacity = num_tokens_per_batch // num_experts

    # inputs = np.random.uniform(0, 1, (bs, seq_len, embed_dim))
    inputs = np.ones((bs, seq_len, embed_dim))
    index_list = np.random.random_integers(0, 1, bs)

    ff_list = list()

    # blub = tf.unstack(inputs, axis=0)

    out_list = list()
    for x in range(bs):
        index = index_list[x]
        some_input = inputs[x]
        out_list.append(ff_list[index](some_input))

    print()




class MyExplicitSwitch(layers.Layer):

    def __init__(self, num_experts, embed_dim, dense_dim, num_dense=1):
        self.num_experts = num_experts
        self.embed_dim = embed_dim
        self.experts = list()
        for _ in range(num_experts):
            self.experts.append(create_feedforward_network(dense_dim, embed_dim, num_dense))
        super(MyExplicitSwitch, self).__init__()

    def call(self, inputs, indexes):
        expert_input_list = tf.unstack(inputs, axis=0)

        out_list = list()
        for x, expert_input in enumerate(expert_input_list):
            index = indexes[x]
            # can't get the value
            temp = self.experts[index](expert_input)
            out_list.append(temp)

        return tf.stack(out_list, axis=1)


class MyExplicitSwitchWorkaround(layers.Layer):

    MODE_ALTERNATING = None

    def __init__(self, num_experts, embed_dim, dense_dim, num_dense=1):
        self.num_experts = num_experts
        self.embed_dim = embed_dim
        self.experts = list()
        for _ in range(num_experts):
            self.experts.append(create_feedforward_network(dense_dim, embed_dim, num_dense))

        self.mode = self.MODE_ALTERNATING
        super(MyExplicitSwitchWorkaround, self).__init__()

    def set_mode(self, new_mode):
        # print("new mode: ", new_mode)
        self.mode = new_mode

    def call(self, inputs):
        expert_input_list = tf.unstack(inputs, axis=0)

        out_list = list()
        for x, expert_input in enumerate(expert_input_list):

            if self.mode is None:
                index = x % len(self.experts)
            else:
                index = self.mode
            # print(index)
            temp = self.experts[index](expert_input)
            out_list.append(temp)

        temp = tf.stack(out_list, axis=0)
        return temp


class ExplicitSwitchTransformerEncoder(layers.Layer):

    def __init__(self, key_dim=64, model_dim=512, dense_dim=512, num_heads=2, num_experts=2, dropout_rate=0.1, **kwargs):
        super().__init__(**kwargs)
        self.self_att = TransformerSelfAttention(key_dim, num_heads, dropout_rate, use_mask=False)
        self.ff = MyExplicitSwitch(num_experts=num_experts, embed_dim=dense_dim, dense_dim=model_dim, num_dense=1)

    def call(self, inputs, indexes):
        a = self.self_att(inputs)
        a = self.ff(a, indexes)
        return a


class ExplicitSwitchTransformerEncoderWorkaround(layers.Layer):

    def __init__(self, key_dim=64, model_dim=512, dense_dim=512, num_heads=2, num_experts=2, dropout_rate=0.1, **kwargs):
        super().__init__(**kwargs)
        self.self_att = TransformerSelfAttention(key_dim, num_heads, dropout_rate, use_mask=False)
        self.ff = MyExplicitSwitchWorkaround(num_experts=num_experts, embed_dim=dense_dim, dense_dim=model_dim, num_dense=1)

    def set_mode(self, new_mode):
        self.ff.set_mode(new_mode)

    def call(self, inputs):
        a = self.self_att(inputs)
        a = self.ff(a)
        return a


class ExplicitSwitchTransformerDecoderWorkaround(layers.Layer):

    def __init__(self, key_dim=64, model_dim=512, dense_dim=512, num_heads=2, num_experts=2, dropout_rate=0.1, **kwargs):
        super().__init__(**kwargs)

        self.self_att = TransformerSelfAttention(key_dim, num_heads, dropout_rate, use_mask=True)
        self.enc_att = DecoderEncoderAttention(key_dim, num_heads, dropout_rate)
        self.ff = MyExplicitSwitchWorkaround(num_experts=num_experts, embed_dim=dense_dim, dense_dim=model_dim, num_dense=1)

    def set_mode(self, new_mode):
        self.ff.set_mode(new_mode)

    def call(self, enc_out, target):
        a = self.self_att(target)
        a = self.enc_att(enc_out, a)
        return self.ff(a)


class ExplicitSwitchTransformerDecoder(layers.Layer):

    def __init__(self, key_dim=64, model_dim=512, dense_dim=512, num_heads=2, num_experts=2, dropout_rate=0.1, **kwargs):
        super().__init__(**kwargs)

        self.self_att = TransformerSelfAttention(key_dim, num_heads, dropout_rate, use_mask=True)
        self.enc_att = DecoderEncoderAttention(key_dim, num_heads, dropout_rate)
        self.ff = MyExplicitSwitch(num_experts=num_experts, embed_dim=dense_dim, dense_dim=model_dim, num_dense=1)

    def call(self, enc_out, target, indexes):
        a = self.self_att(target)
        a = self.enc_att(enc_out, a)
        return self.ff(a, indexes)


class MyExplicitSwitchTransformerEncoders(layers.Layer):

    def __init__(self, key_dim, model_dim, feed_forward_dim, num_heads, num_layers_enc, **kwargs):
        self.enc_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingEncoder")

        self.num_layers_enc = num_layers_enc
        self.encoders = list()
        for i in range(self.num_layers_enc):
            self.encoders.append(ExplicitSwitchTransformerEncoder(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim,
                                                 num_heads=num_heads, name="Encoder_" + str(i)))

        super().__init__(**kwargs)

    def call(self, source, indexes):
        x = self.vision_net(source)
        x = self.enc_pos_emb(x)

        for i, encoder in enumerate(self.encoders):
            x = encoder(x, indexes)

        return x


class MyExplicitSwitchTransformerWorkaround(TransformerBaseMean):

    def __init__(self, vision_net_id=None,
                 model_dim=512,
                 key_dim=64,
                 num_heads=1,
                 feed_forward_dim=64,
                 seq_len=125,
                 num_layers_enc=4,
                 num_layers_dec=1,
                 num_classes=10,
                 **kwargs):

        super().__init__(**kwargs)

        # with tf.device("/gpu:1"):
        self.loss_metric = Mean(name="loss")
        self.num_layers_enc = num_layers_enc
        self.num_layers_dec = num_layers_dec
        self.seq_len = seq_len
        self.num_classes = num_classes

        self.enc_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingEncoder")
        # self.enc_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingEncoder")
        # encoder

        self.encoder = Sequential(name="Encoders")
        self.encoder.add(self.enc_pos_emb)

        self.encoder_list = list()
        for i in range(self.num_layers_enc):
            self.encoder.add(ExplicitSwitchTransformerEncoderWorkaround(key_dim=key_dim,
                                                      model_dim=model_dim, dense_dim=feed_forward_dim,
                                                      num_heads=num_heads, name="Encoder_" + str(i)))

        # with tf.device("/gpu:0"):
        self.dec_enc = TransformerOutputEncoding(seq_len=seq_len, model_dim=model_dim, name="InputEmbeddingDecoder")
        self.dec_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingDecoder")
        # self.dec_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingDecoder")

        for i in range(num_layers_dec):
            setattr(self, f"dec_layer_{i}",
                    ExplicitSwitchTransformerDecoderWorkaround(key_dim=key_dim,
                                             model_dim=model_dim, dense_dim=feed_forward_dim,
                                             num_heads=num_heads, name="Decoder_" + str(i)))

        self.classifier = layers.Dense(num_classes, activation="softmax")


    def set_mode(self, new_mode):
        # print(new_mode)
        for e in self.encoder.layers:
            # print(e)
            if isinstance(e, ExplicitSwitchTransformerEncoderWorkaround):
                e.set_mode(new_mode)

        for i in range(self.num_layers_dec):
            d = getattr(self, f"dec_layer_{i}")
            # print(d)
            if isinstance(d, ExplicitSwitchTransformerDecoderWorkaround):
                d.set_mode(new_mode)

class MyExplicitSwitchTransformer(TransformerBaseMean):

    def __init__(self, vision_net_id,
                 model_dim=512,
                 key_dim=64,
                 num_heads=1,
                 feed_forward_dim=64,
                 seq_len=125,
                 num_layers_enc=4,
                 num_layers_dec=1,
                 num_classes=10,
                 **kwargs):

        super().__init__(**kwargs)

        # with tf.device("/gpu:1"):

        self.loss_metric = Mean(name="loss")
        self.num_layers_enc = num_layers_enc
        self.num_layers_dec = num_layers_dec
        self.seq_len = seq_len
        self.num_classes = num_classes

        self.enc_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingEncoder")
        # self.enc_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingEncoder")
        # encoder

        self.encoder = MyExplicitSwitchTransformerEncoders(key_dim=key_dim, model_dim=model_dim, feed_forward_dim=feed_forward_dim, num_heads=num_heads, num_layers_enc=num_layers_enc)

        # with tf.device("/gpu:0"):
        self.dec_enc = TransformerOutputEncoding(seq_len=seq_len, model_dim=model_dim, name="InputEmbeddingDecoder")
        self.dec_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingDecoder")

        for i in range(num_layers_dec):
            setattr(self, f"dec_layer_{i}",
                    ExplicitSwitchTransformerDecoder(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim,
                                       num_heads=num_heads, name="Decoder_" + str(i)))

        self.classifier = layers.Dense(num_classes, activation="softmax")

    def call(self, inputs):
        # print(inputs)
        source = inputs["source"]
        target = inputs["target"]
        indexes = inputs["indexes"]
        # print("source", source)
        # print("target", target)

        # a = self.enc_emb(source)
        # print("a_0", a)
        # a = self.enc_pos_emb(a)
        # print("a_1", a)
        a = self.encoder(source, indexes)
        # print("a", a)
        a = self.decode(a, target, indexes)

        return self.classifier(a)

    def train_step(self, batch):
        """Processes one batch inside model.fit()."""
        batch = batch[0]
        source = batch["source"]
        target = batch["target"]
        indexes = batch["indexes"]

        dec_input = target[:, :-1]
        dec_target = target[:, 1:]

        with tf.GradientTape() as tape:
            preds = self({"source": source, "target": dec_input, "indexes": indexes})
            # print("dec_target", dec_target)
            one_hot = tf.one_hot(dec_target, depth=self.num_classes)
            mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
            loss = self.compiled_loss(one_hot, preds, sample_weight=mask)

        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))
        self.loss_metric.update_state(loss)

        return {"loss": self.loss_metric.result()}

    def test_step(self, batch):
        batch = batch[0]
        # print(batch)
        source = batch["source"]
        target = batch["target"]
        indexes = batch["indexes"]

        dec_input = target[:, :-1]
        dec_target = target[:, 1:]
        # print("dec_input", dec_input)
        # print("dec_target", dec_target)
        # print("source", source)
        preds = self({"source": source, "target": dec_input, "indexes": indexes})
        one_hot = tf.one_hot(dec_target, depth=self.num_classes)
        mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
        loss = self.compiled_loss(one_hot, preds, sample_weight=mask)
        self.loss_metric.update_state(loss)
        return {"loss": self.loss_metric.result()}

    def calc_cer(self, source, target, indexes, target_start_token_idx):
        bs = tf.shape(source)[0].numpy()
        # print("bs", bs)
        # print("source", source)
        enc = self.encoder(source, indexes)
        # print("enc_shape", enc.shape)
        dec_input = tf.ones((bs, 1), dtype=tf.int32) * target_start_token_idx

        # dim: bs * 1  --> ["<", "<", ... "<"]
        hit_sum = 0
        dec_logits = []
        for i in range(self.seq_len - 1):
            current_target = target[:, i + 1]

            dec_out = self.decode(enc, dec_input, indexes)
            logits = self.classifier(dec_out)
            logits = tf.argmax(logits, axis=-1, output_type=tf.int32)
            last_logit = tf.expand_dims(logits[:, -1], axis=-1)
            hits = np.count_nonzero(current_target == last_logit.numpy()[:, 0])

            hit_sum += (hits / self.seq_len)

            dec_logits.append(current_target)
            dec_input = tf.concat([dec_input, np.expand_dims(current_target, axis=1)], axis=-1)

        return hit_sum / bs

    def generate(self, source, target_start_token_idx, language_index):
        """Performs inference over one batch of inputs using greedy decoding."""
        bs = tf.shape(source)[0]
        # print("bs", bs)
        # print("source", source)
        enc = self.encoder(source, language_index)
        # print("enc_shape", enc.shape)
        dec_input = tf.ones((bs, 1), dtype=tf.int32) * target_start_token_idx

        # dim: bs * 1  --> ["<", "<", ... "<"]

        dec_logits = []
        for i in range(self.seq_len - 1):
            a = dec_input.numpy()
            dec_out = self.decode(enc, dec_input, language_index)
            logits = self.classifier(dec_out)
            logits = tf.argmax(logits, axis=-1, output_type=tf.int32)
            last_logit = tf.expand_dims(logits[:, -1], axis=-1)
            dec_logits.append(last_logit)
            dec_input = tf.concat([dec_input, last_logit], axis=-1)

        return dec_input


class MyExplicitSwitchTransformerWorkaroundTail(MyExplicitSwitchTransformerWorkaround):

    def __init__(self, vision_net_id=None,
                 model_dim=512,
                 key_dim=64,
                 num_heads=1,
                 feed_forward_dim=64,
                 seq_len=125,
                 num_layers_enc=4,
                 num_layers_dec=1,
                 num_classes=10,
                 **kwargs):

        super().__init__(**kwargs)

        # with tf.device("/gpu:1"):

        self.loss_metric = Mean(name="loss")
        self.num_layers_enc = num_layers_enc
        self.num_layers_dec = num_layers_dec
        self.seq_len = seq_len
        self.num_classes = num_classes

        self.enc_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingEncoder")
        # self.enc_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingEncoder")
        # encoder

        self.encoder = Sequential(name="Encoders")
        self.encoder.add(self.enc_pos_emb)

        for i in range(self.num_layers_enc):
            self.encoder.add(TransformerEncoder(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim,
                                                num_heads=num_heads, name="Encoder_" + str(i)))

        # with tf.device("/gpu:0"):
        self.dec_enc = TransformerOutputEncoding(seq_len=seq_len, model_dim=model_dim, name="InputEmbeddingDecoder")
        self.dec_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingDecoder")

        for i in range(num_layers_dec):
            setattr(self, f"dec_layer_{i}",
                    ExplicitSwitchTransformerDecoderWorkaround(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim,
                                       num_heads=num_heads, name="Decoder_" + str(i)))

        self.classifier = layers.Dense(num_classes, activation="softmax")


def some_test_1():


    bs = 10
    seq_len = 2
    dense_dim = 10
    num_experts = 2
    embed_dim = 10
    inputs = np.ones((bs, seq_len, embed_dim))
    index_list = np.random.random_integers(0, 1, bs)

    my_cool_switch = MyExplicitSwitch(num_experts, embed_dim, dense_dim)

    res = my_cool_switch(inputs, index_list)
    print(res)
    print(index_list)


def some_test_2():
    bs = 10
    section_len = 40
    model_dim = 512
    key_dim = 512
    dense_dim = 512
    num_heads = 8
    vocab_size = 32
    num_enc = 2
    num_dec = 2

    vision_net = 100

    transformer = MyExplicitSwitchTransformerWorkaround(vision_net_id=vision_net,
                                        model_dim=model_dim,
                                        key_dim=key_dim,
                                        num_heads=num_heads,
                                        feed_forward_dim=dense_dim,
                                        seq_len=section_len,
                                        num_layers_enc=num_enc,
                                        num_layers_dec=num_dec,
                                        num_classes=vocab_size
                                        )
    transformer.compile(loss="mse")

    source = np.random.uniform(0, 1, (bs, section_len, 100, 100, 1))
    target = np.zeros((bs, section_len), dtype=int)
    indexes = np.random.random_integers(0, 1, bs)

    data = {"source": source, "target": target}
    res = transformer.predict(data, batch_size=bs)
    print("done")
    print(res)


if __name__ == '__main__':
    some_test_2()


