from tensorflow.keras import Sequential, Model
from tensorflow.keras.utils import plot_model
from tensorflow.keras import applications
from tensorflow.keras import layers
from tensorflow.keras import optimizers
from tensorflow.keras import losses, metrics
from tensorflow.keras.models import model_from_json
from tensorflow.keras.callbacks import TensorBoard
from my_iou import My_IOU

import gc

import cv2
import numpy as np
import os
from copy import copy

from Data_Loader import Data_Loadder, load_classification_data
from class_data_loader import class_data_loader

import tensorflow as tf

from models import load_model, load_classification_model

print("Num GPUs Available: ", len(tf.config.experimental.list_physical_devices('GPU')))
sess = tf.compat.v1.Session(config=tf.compat.v1.ConfigProto(log_device_placement=True))
tf.compat.v1.enable_eager_execution()

def init_tensorboard(model:Model, log_dir="logs"):
    os.makedirs(log_dir, exist_ok=True)
    tensorboard = TensorBoard(
        log_dir=os.path.join(log_dir, str(model.name)),
        histogram_freq=1,
        write_images=True
    )

    # tensorboard --logdir="logs/"

    return tensorboard

def predict_to_points(target_size, *values):
    # print(values)
    return (int(values[0]*target_size[0]), int(values[1]*target_size[0])), (int(values[2]*target_size[1]), int(values[3]*target_size[1]))


def bb_intersection_over_union(boxA, boxB):
    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])

    # compute the area of intersection rectangle
    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)

    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
    boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = interArea / float(boxAArea + boxBArea - interArea)

    # return the intersection over union value
    return iou


def save_model(model:Model, model_path, weights_path):
    if not os.path.exists(model_path):
        with open(model_path, "w") as json_file:
            json_file.write(model.to_json())
        print("saved model at: " + str(model_path))
    model.save_weights(weights_path)
    print("saved weights at: " + str(weights_path))


def init_data_loader(annotations_path, size=None, gray=True):
    loader = Data_Loadder(size, gray=gray)
    loader.load_data(annotations_path)
    return loader


def prepare_data(loader):
    data, labels = loader.get_next_batch()
    data = np.array(data)
    if loader.is_gray():
        data = np.expand_dims(data, axis=3)
    labels = np.array(labels)

    return data, labels


def get_examples(annotations_path, example_count, gray=True, target_size=160):

    bb_0 = list()
    bb_1 = list()
    img = list()
    data_list = list()

    loader_train = Data_Loadder(example_count, gray=gray)
    loader_train.load_data(annotations_path)
    data, labels = loader_train.get_next_batch()

    for x in range(len(data)):
        bb_0.append((int(labels[x][0] * target_size), int(labels[x][1] * target_size)))
        bb_1.append((int(labels[x][2] * target_size), int(labels[x][3] * target_size)))

        img.append(copy(data[x]))

        temp = img[-1]
        if gray:
            temp = np.expand_dims(temp, axis=2)

        temp = np.expand_dims(temp, axis=0)

        data_list.append(temp)

    return bb_0, bb_1, img, data_list


def show_examples(model, target_size, img, img_data, bb_0, bb_1, title):
    for i in range(len(img)):
        res = model.predict(img_data[i])[0]

        p_0, p_1 = predict_to_points(target_size, *res)

        img_bb = copy(img[i])

        img_bb = cv2.rectangle(img_bb, p_0, p_1, (0, 0, 0), 2)
        img_bb = cv2.rectangle(img_bb, bb_0[i], bb_1[i], (255, 255, 255), 1)

        cv2.imshow(title + "_" + str(i), img_bb)

def detection_training():

    train_data_path = """/media/ubuntu/data/lip_reading_data/class_train"""

    gray = True

    main_epochs = 200
    sub_epochs = 10

    target_size = (160, 160)

    model_number = 0

    model_path = "./detection/class_model_" + str(model_number) + ".json"
    weights_path = "./detection/class_weights_" + str(model_number) + ".h5"

    train_data, train_labels = load_classification_data(train_data_path, [0, 1], gray)
    # print(train_data.shape)
    # print(train_data[0])
    train_data = np.expand_dims(train_data, axis=3)
    model = load_classification_model(model_number, model_path, weights_path, target_size)

    tensorboard_callback = init_tensorboard(model, "class_logs")

    for ep in range(main_epochs):
        model.fit(train_data, train_labels, verbose=1, callbacks=[tensorboard_callback], epochs=sub_epochs)
        save_model(model, model_path, weights_path)
        gc.collect()

    print("done")

def main():
    classification_training()

def localisation_training():

    train_data_path = """/media/ubuntu/data/lip_reading_data/train_data/annotations.yaml"""
    test_data_path = """/media/ubuntu/data/lip_reading_data/test_data/annotations.yaml"""

    gray = True

    main_epochs = 4
    sub_epochs = 50

    batch_size = None
    target_size = (160, 160)

    model_number = None

    data_loader_train = init_data_loader(train_data_path, batch_size, gray=gray)
    data_loader_test = init_data_loader(test_data_path, None, gray=gray)

    # train_bb_0, train_bb_1, train_img, train_gray = get_examples(train_data_path, 5, gray=gray)
    # test_bb_0, test_bb_1, test_img, test_gray = get_examples(test_data_path, 5, gray=gray)

    train_data, train_labels = prepare_data(data_loader_train)
    test_data, test_labels = prepare_data(data_loader_test)

    # for i in range(model_number):
    done = False

    target_models = [21]
    for i in target_models:

        print("model: " + str(i))
        model_path = "./model_" + str(i) + ".json"
        weights_path = "./weights_" + str(i) + ".h5"

        model = load_model(i, model_path, weights_path, target_size)

        if model is None:
            done = True
            break

        tensorboard_callback = init_tensorboard(model)

        for x in range(main_epochs):

            print("epoch: " + str(x))
            print("train data size: " + str(len(train_data)))

            model.fit(train_data, train_labels, verbose=1, callbacks=[tensorboard_callback], validation_data=(test_data, test_labels), validation_freq=10, epochs=sub_epochs)

            print("save")
            save_model(model, model_path, weights_path)

            # show_examples(model, target_size, train_img, train_gray, train_bb_0, train_bb_1, "train")
            # show_examples(model, target_size, test_img, test_gray, test_bb_0, test_bb_1, "test")

            gc.collect()

            # cv2.waitKey(250)

        i += 1

    print("done")
    # cv2.waitKey()


def classification_training():
    data_path = """/media/ubuntu/data/lip_reading_data/detection_data"""
    test_data_part = 0.04

    main_epochs = 1
    sub_epochs = 200

    batch_size = None
    target_size = (160, 160)

    model_number = None

    train_data, train_labels, test_data, test_labels = class_data_loader(data_path, test_data_part=test_data_part)

    target_models = reversed(range(47, 54 + 1))

    for x in target_models:
        print("model: " + str(x))

        model_path = "./classification/model_" + str(x) + ".json"
        weights_path = "./classification/weights_" + str(x) + ".h5"

        model = load_model(x, model_path, weights_path, target_size, output_size=2, last_activation="softmax", metric="accuracy")

        if model is None:
            done = True
            break

        tensorboard_callback = init_tensorboard(model)

        for x in range(main_epochs):
            print("epoch: " + str(x))
            print("train data size: " + str(len(train_data)))

            model.fit(train_data, train_labels, verbose=1, callbacks=[tensorboard_callback],
                      validation_data=(test_data, test_labels), validation_freq=10, epochs=sub_epochs)

            print("save")
            save_model(model, model_path, weights_path)

            # show_examples(model, target_size, train_img, train_gray, train_bb_0, train_bb_1, "train")
            # show_examples(model, target_size, test_img, test_gray, test_bb_0, test_bb_1, "test")

            gc.collect()

            # cv2.waitKey(250)


if __name__ == '__main__':
    main()
