from ModelSaver import Saver
from TransformerModels import get_transformer_model
from lip_data_reader import VectorizeChar
from vision_nets import get_vision_net, copy_kernel
from tensorflow.keras.models import Model
import numpy as np
import os
import cv2
import shutil

def extract_trained_vision_net(model_number, target_weights, save_location, in_shape, prev_srq_len=40):
    seq_len = 75
    batch_size = 64
    lr = 0.0001
    metrics = ["loss", "test_loss", "nce", "test_nce", "german_test_loss", "german_test_nce", "english_test_loss", "english_test_nce"]

    model = get_transformer_model(model_number, prev_srq_len, in_shape, len(VectorizeChar.get_vocabulary()),
                                  build_bs=batch_size, lr=lr)
    if target_weights in [Saver.RECENT_WIGHTS, Saver.BEST_WIGHTS]:
        saver = Saver.from_data(save_location, metrics=metrics, main_metric="test_loss",
                                model_title="model_" + str(model_number))
        saver.set_smooth_plot("loss")

        model = saver.load_weights(model, target_weights)
    else:
        model.load_weights(target_weights)

    vis_net = model.get_layer("VisionLayers")

    return vis_net


def load_video(video_path, padding_len=75):
    cap = cv2.VideoCapture(video_path)
    video = list()

    while True:
        s, f = cap.read()
        if not s:
            break
        video.append(cv2.cvtColor(f, cv2.COLOR_BGR2GRAY) / 255)

    cap.release()
    video = np.array(video)
    padding = np.zeros((padding_len, video.shape[1], video.shape[2]))

    video = np.concatenate((video, padding))
    video = video[:padding_len]

    video = np.expand_dims(video, axis=-1)
    video = np.expand_dims(video, axis=0)

    return video

def extend_srq_len(old_net, net_id, seq_len):
    new_net = get_vision_net(seq_len=seq_len, version_number=net_id)
    return copy_kernel(old_net, new_net)

def preprocess_video(model_save_location, target_weights, model_number, vision_net_id, video_source, output_location, seq_len=75, in_shape=(100, 100, 1)):
    model = extract_trained_vision_net(model_number, target_weights, model_save_location, in_shape)
    model = extend_srq_len(model, vision_net_id, seq_len)
    all = len(os.listdir(video_source))
    current = 0
    for x, file in enumerate(os.listdir(video_source)):
        temp = int((100 / all) * x)
        if temp > current:
            print(temp)
            current = temp

        if not os.path.splitext(file)[-1] == ".mp4":
            continue

        text_file = os.path.join(video_source, os.path.splitext(file)[0] + ".txt")
        if not os.path.exists(text_file):
            continue

        data = load_video(os.path.join(video_source, file), seq_len)
        new_data = model.predict(data)

        new_video_file = os.path.join(output_location, os.path.split(os.path.splitext(file)[0])[-1] + "_pp.npy")
        new_text_file = os.path.join(output_location, os.path.split(os.path.splitext(file)[0])[-1] + ".txt")

        shutil.copy(text_file, new_text_file)
        with open(new_video_file, 'wb') as f:
            np.save(f, new_data)

        print("saved: ", new_video_file)
    print("done")


if __name__ == '__main__':
    preprocess_video("""/media/ubuntu/data/main_saves/test_2015_203""", Saver.RECENT_WIGHTS, 2015, 203, """/media/ubuntu/data/combined/100/german""", """/media/ubuntu/data/combined/100/german_pre""", 75, (100, 100, 1))
    preprocess_video("""/media/ubuntu/data/main_saves/test_2015_203""", Saver.RECENT_WIGHTS, 2015, 203, """/media/ubuntu/data/combined/100/english""", """/media/ubuntu/data/combined/100/english_pre""", 75, (100, 100, 1))

