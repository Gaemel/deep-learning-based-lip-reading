import cv2
import yaml
from random import shuffle
import numpy as np
import os

class Data_Loadder():

    P_0 = "pt_0"
    P_1 = "pt_1"

    def __init__(self, batch_size=None, gray=True, target_size=160):
        self._annotation = None
        self._data_order = None
        self._batch_size = batch_size
        self._batch_index = 0
        self._target_path = None
        self._gray = gray
        self.target_size = target_size

    def load_data(self, annotations_path):
        self._target_path = os.path.split(annotations_path)[0]
        print(self._target_path)
        with open(annotations_path) as f:
            self._annotation = yaml.load(f, Loader=yaml.FullLoader)

        self._data_order = list(self._annotation.keys())
        shuffle(self._data_order)
        self._batch_index = 0

    def load_img(self, target_path):
        if self._gray:
            return cv2.cvtColor(cv2.imread(target_path), cv2.COLOR_BGR2GRAY) / 255
        else:
            return cv2.imread(target_path) / 255

    def load_point(self, key, as_np_array=True):
        if as_np_array:
            return np.array((self._annotation[key][self.P_0]["x"] / self.target_size, self._annotation[key][self.P_0]["y"] / self.target_size,
                             self._annotation[key][self.P_1]["x"] / self.target_size, self._annotation[key][self.P_1]["y"] / self.target_size))
        else:
            return (self._annotation[key][self.P_0]["x"] / self.target_size, self._annotation[key][self.P_0]["y"] / self.target_size), \
                   (self._annotation[key][self.P_1]["x"] / self.target_size, self._annotation[key][self.P_1]["y"] / self.target_size)

    def is_gray(self):
        return self._gray

    def _load_data(self, keys):
        data = list()
        labels = list()

        for key in keys:
            data.append(self.load_img(os.path.join(self._target_path, key)))
            labels.append(self.load_point(key, True))

        return data, labels

    def _get_next_batch_keys(self):
        batch_size = len(self._data_order) if self._batch_size is None else self._batch_size

        batch_size = min(batch_size, len(self._data_order))

        if batch_size * (self._batch_index + 1) >= len(self._data_order):
            self._batch_index = 0

        keys = self._data_order[self._batch_index * batch_size: (self._batch_index + 1) * batch_size]

        self._batch_index += 1

        return keys

    def get_next_batch(self):
        return self._load_data(self._get_next_batch_keys())



def one_hot(index, max):
    temp = np.zeros(max)
    temp[index] = 1
    return temp

def load_classification_data(target_dir, classes, gray=True):
    data = list()
    labels = list()

    for c in classes:
        for f in os.listdir(os.path.join(target_dir, str(c))):
            img = cv2.imread(os.path.join(target_dir, str(c), f))
            if gray:
                img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            img = np.array(img) / 255
            # print(img.shape)
            data.append(img)
            labels.append(one_hot(c, len(classes)))

    return np.array(data), np.array(labels)


def main():
    loader = Data_Loadder(batch_size=100)
    loader.load_data("""/media/ubuntu/data/lip_reading_data/images (copy)/annotations.yaml""")
    data, labels = loader.get_next_batch()
    print(len(data))
    print(labels)


if __name__ == '__main__':
    main()
