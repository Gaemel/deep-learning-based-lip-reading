from tensorflow.keras.layers import Input, Embedding, MultiHeadAttention, Layer, LayerNormalization, Dense, Conv1D, Reshape, MaxPooling3D, Flatten, BatchNormalization, Conv3D, Conv2D, MaxPooling2D, GlobalMaxPool2D
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.metrics import Mean
import tensorflow as tf
from residual_conv import MyResidualIdentityConv2D, MyResidualConv2D
from tensorflow.keras.callbacks import Callback
import numpy as np
from vision_nets import get_vision_net


from MyLayers import BorderDrop3D

class InputEmbedding(Layer):

    def __init__(self, seq_len, feature_size, output_size, **kwargs):
        super().__init__(**kwargs)

        self._con_0 = Conv1D(output_size, kernel_size=feature_size, input_shape=(seq_len, feature_size))
        self._reshape = Reshape((seq_len, output_size, 1))

    def call(self, inputs):
        a = self._con_0(inputs)
        a = self._reshape(a)
        a = tf.squeeze(a, axis=-1)
        return a


class InputEmbedding_V1(Layer):

    def __init__(self, seq_len, feature_size, output_size, **kwargs):
        super().__init__(**kwargs)

        self._reshape = Reshape((seq_len, feature_size))
        self._ff = Dense(output_size, activation="relu")

    def call(self, inputs):
        # print("in inputs", inputs)
        a = self._reshape(inputs)
        # print("in a", a)
        a = tf.squeeze(a, axis=-1)
        # print("in a_1", a)
        a = self._ff(a)
        # print("in a_2", a)
        return a


class ResidualFeedForward(Layer):

    def __init__(self, dense_dim=4, output_dim=256, **kwargs):
        super().__init__(**kwargs)

        self.ff = Sequential()
        if not isinstance(dense_dim, list):
            dense_dim = [dense_dim]
        for dd in dense_dim:
            self.ff.add(Dense(dd, activation=tf.nn.gelu))

        self.ff.add(Dense(output_dim, activation="linear"))

        self.norm = LayerNormalization()

    def call(self, inputs):
        a = self.ff(inputs)
        a = self.norm(inputs + a)
        return a


class TransformerOutputEncoding(Layer):

    def __init__(self, seq_len, embed_dim, **kwargs):
        super().__init__(**kwargs)

        self.emb = Embedding(seq_len, embed_dim)
        # self.pos_emb = Embedding()

    def call(self, inputs):
        # print("inputs", inputs)
        return self.emb(inputs)


class PositionalEmbedding(Layer):
    def __init__(self, seq_len, output_dim, **kwargs):
        super().__init__(**kwargs)
        # self.embedding = Embedding(feature_len, output_dim)
        self.position_embeddings = Embedding(seq_len, output_dim)

        self.seq_len = seq_len
        self.output_dim = output_dim

    def call(self, inputs):
        # The inputs are of shape: `(batch_size, frames, num_features)`
        length = tf.shape(inputs)[1]
        positions = tf.range(start=0, limit=length, delta=1)
        embedded_positions = self.position_embeddings(positions)
        # embedded = self.embedding(inputs)
        # print("embedded", embedded.shape)
        return embedded_positions + inputs


class TransformerSelfAttention(Layer):

    def __init__(self, embed_dim=100, num_heads=2, dropout_rate=0.1, use_mask=False, **kwargs):
        super().__init__(**kwargs)

        self.embed_dim = embed_dim
        self.num_heads = num_heads

        self.attention = MultiHeadAttention(num_heads=num_heads, key_dim=embed_dim, dropout=dropout_rate)
        self.norm = LayerNormalization()
        self.use_mask = use_mask

    def call(self, inputs):
        # print("inputs", inputs.shape)
        mask = None
        if self.use_mask:
            batch_size = inputs.shape[0]
            #
            # if batch_size is None:
            #     batch_size = 10

            seq_len = inputs.shape[1]
            mask = self.get_maks(batch_size, seq_len, seq_len)

        # print("in_shape", inputs.shape)
        att_output = self.attention(inputs, inputs, attention_mask=mask)
        normed_1 = self.norm(inputs + att_output)
        # print("att_out", att_output.shape)
        # print("normed_1", normed_1.shape)

        return normed_1

    def get_maks(self, batch_size, n_dest, n_src, dtype=tf.bool):
        """Masks the upper half of the dot product matrix in self attention.

        This prevents flow of information from future tokens to current token.
        1's in the lower triangle, counting from the lower right corner.
        """

        """

        something like this:

        1 0 0 0
        1 1 0 0
        1 1 1 0
        1 1 1 1

        """

        i = tf.range(n_dest)[:, None]
        j = tf.range(n_src)
        m = i >= j - n_src + n_dest
        mask = tf.cast(m, dtype)
        # print("mask", mask)
        return mask

        # mask = tf.reshape(mask, [1, n_dest, n_src])
        # print("mask 1", mask)
        # mult = tf.concat(
        #     [tf.expand_dims(batch_size, -1), tf.constant([1, 1], dtype=tf.int32)], 0
        # )
        # print("mult 2", mult)
        # temp = tf.tile(mask, mult)
        # print("temp", temp)
        # return temp

class DecoderEncoderAttention(Layer):

    def __init__(self, embed_dim=100, num_heads=2, dropout_rate=0.1, **kwargs):
        super().__init__(**kwargs)

        self.enc_att = MultiHeadAttention(num_heads=num_heads, key_dim=embed_dim, dropout=dropout_rate)
        self.norm = LayerNormalization()

    def call(self, enc_out, target):
        a = self.enc_att(target, enc_out)
        return self.norm(a + target)


class TransformerDecoder(Layer):

    def __init__(self, embed_dim=100, dense_dim=4, num_heads=2, dropout_rate=0.1, **kwargs):
        super().__init__(**kwargs)

        self.self_att = TransformerSelfAttention(embed_dim, num_heads, dropout_rate, use_mask=True)
        self.enc_att = DecoderEncoderAttention(embed_dim, num_heads, dropout_rate)
        self.ff = ResidualFeedForward(dense_dim, embed_dim)

    def call(self, enc_out, target):
        a = self.self_att(target)
        a = self.enc_att(enc_out, a)
        return self.ff(a)


class TransformerEncoder(Layer):

    def __init__(self, embed_dim=100, dense_dim=4, num_heads=2, dropout_rate=0.1, **kwargs):
        super().__init__(**kwargs)
        self.self_att = TransformerSelfAttention(embed_dim, num_heads, dropout_rate, use_mask=False)
        self.ff = ResidualFeedForward(dense_dim, embed_dim)

    def call(self, inputs):
        a = self.self_att(inputs)
        a = self.ff(a)
        return a


class TransformerHeadless(Model):

    def __init__(self, vision_net,
               embed_dim=100,
               num_heads=1,
               feed_forward_dim=64,
               seq_len=125,
               num_layers_enc=4,
               num_layers_dec=1,
               num_classes=10,
               feature_size=(160, 160, 1),
               **kwargs):

        super().__init__(**kwargs)

        self.vision_net = vision_net
        self.loss_metric = Mean(name="loss")
        self.num_layers_enc = num_layers_enc
        self.num_layers_dec = num_layers_dec
        self.seq_len = seq_len
        self.num_classes = num_classes

        temp = np.product(self.vision_net.layers[-1].output_shape[2:])

        self.enc_pos_emb = PositionalEmbedding(seq_len, embed_dim, name="PositionalEmbeddingEncoder")

        # encoder
        self.encoder = Sequential(name="Encoders")
        self.encoder.add(self.vision_net)
        # self.encoder.add(self.enc_emb)
        self.encoder.add(self.enc_pos_emb)

        for i in range(self.num_layers_enc):
            self.encoder.add(TransformerEncoder(embed_dim, feed_forward_dim, num_heads, name="Encoder_" + str(i)))

        # decoder
        self.dec_enc = TransformerOutputEncoding(seq_len, embed_dim, name="InputEmbeddingDecoder")
        self.dec_pos_emb = PositionalEmbedding(seq_len, embed_dim, name="PositionalEmbeddingDecoder")

        for i in range(num_layers_dec):
            setattr(self, f"dec_layer_{i}",
                    TransformerDecoder(embed_dim, feed_forward_dim, num_heads, name="Decoder_" + str(i)))

        self.classifier = Dense(num_classes, activation="softmax")

    def decode(self, enc_out, target):
        a = self.dec_enc(target)
        a = self.dec_pos_emb(a)
        for i in range(self.num_layers_dec):
            a = getattr(self, f"dec_layer_{i}")(enc_out, a)
        return a

    def call(self, inputs):
        # print(inputs)
        source = inputs["source"]
        target = inputs["target"]
        # print("source", source)
        # print("target", target)

        # a = self.enc_emb(source)
        # print("a_0", a)
        # a = self.enc_pos_emb(a)
        # print("a_1", a)
        a = self.encoder(source)
        # print("a", a)
        a = self.decode(a, target)

        return self.classifier(a)

    def train_step(self, batch):
        """Processes one batch inside model.fit()."""
        batch = batch[0]
        source = batch["source"]
        target = batch["target"]
        dec_input = target[:, :-1]
        dec_target = target[:, 1:]

        with tf.GradientTape() as tape:

            preds = self({"source": source, "target": dec_input})
            one_hot = tf.one_hot(dec_target, depth=self.num_classes)
            mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
            loss = self.compiled_loss(one_hot, preds, sample_weight=mask)

        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))
        self.loss_metric.update_state(loss)

        return {"loss": self.loss_metric.result()}

    def test_step(self, batch):
        batch = batch[0]
        # print(batch)
        source = batch["source"]
        target = batch["target"]
        dec_input = target[:, :-1]
        dec_target = target[:, 1:]
        # print("dec_input", dec_input)
        # print("dec_target", dec_target)
        # print("source", source)
        preds = self({"source": source, "target": dec_input})
        one_hot = tf.one_hot(dec_target, depth=self.num_classes)
        mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
        loss = self.compiled_loss(one_hot, preds, sample_weight=mask)
        self.loss_metric.update_state(loss)
        return {"loss": self.loss_metric.result()}

    def calc_cer(self, source, target, target_start_token_idx):
        bs = tf.shape(source)[0].numpy()
        # print("bs", bs)
        # print("source", source)
        enc = self.encoder(source)
        # print("enc_shape", enc.shape)
        dec_input = tf.ones((bs, 1), dtype=tf.int32) * target_start_token_idx

        # dim: bs * 1  --> ["<", "<", ... "<"]
        hit_sum = 0
        dec_logits = []
        for i in range(self.seq_len - 1):
            current_target = target[:, i+1]

            dec_out = self.decode(enc, dec_input)
            logits = self.classifier(dec_out)
            logits = tf.argmax(logits, axis=-1, output_type=tf.int32)
            last_logit = tf.expand_dims(logits[:, -1], axis=-1)
            hits = np.count_nonzero(current_target == last_logit.numpy()[:,0])

            hit_sum += (hits / self.seq_len)

            dec_logits.append(current_target)
            dec_input = tf.concat([dec_input, np.expand_dims(current_target, axis=1)], axis=-1)

        return hit_sum / bs

    def generate(self, source, target_start_token_idx):
        """Performs inference over one batch of inputs using greedy decoding."""
        bs = tf.shape(source)[0]
        # print("bs", bs)
        # print("source", source)
        enc = self.encoder(source)
        # print("enc_shape", enc.shape)
        dec_input = tf.ones((bs, 1), dtype=tf.int32) * target_start_token_idx

        # dim: bs * 1  --> ["<", "<", ... "<"]

        dec_logits = []
        for i in range(self.seq_len - 1):
            dec_out = self.decode(enc, dec_input)
            logits = self.classifier(dec_out)
            logits = tf.argmax(logits, axis=-1, output_type=tf.int32)
            last_logit = tf.expand_dims(logits[:, -1], axis=-1)
            dec_logits.append(last_logit)
            dec_input = tf.concat([dec_input, last_logit], axis=-1)
        return dec_input

    @property
    def metrics(self):
        return [self.loss_metric]


class TransformerHeadlessNoPosVisionTrain(Model):

    def __init__(self, vision_net_id,
                 embed_dim=100,
                 num_heads=1,
                 feed_forward_dim=64,
                 seq_len=125,
                 num_layers_enc=4,
                 num_layers_dec=1,
                 num_classes=10,
                 feature_size=(160, 160, 1),
                 **kwargs):

        super().__init__(**kwargs)

        with tf.device("/gpu:0"):
            self.vision_net = get_vision_net(vision_net_id, seq_len)
        with tf.device("/gpu:1"):

            self.loss_metric = Mean(name="loss")
            self.num_layers_enc = num_layers_enc
            self.num_layers_dec = num_layers_dec
            self.seq_len = seq_len
            self.num_classes = num_classes

            # temp = np.product(self.vision_net.layers[-1].output_shape[2:])

            # self.enc_pos_emb = PositionalEmbedding(seq_len, embed_dim, name="PositionalEmbeddingEncoder")

            # encoder

            self.encoder = Sequential(name="Encoders")
            self.encoder.add(self.vision_net)

            # self.encoder.add(self.enc_emb)
            # self.encoder.add(self.enc_pos_emb)

            for i in range(self.num_layers_enc):
                self.encoder.add(TransformerEncoder(embed_dim, feed_forward_dim, num_heads, name="Encoder_" + str(i)))

            # decoder
            self.dec_enc = TransformerOutputEncoding(seq_len, embed_dim, name="InputEmbeddingDecoder")
            self.dec_pos_emb = PositionalEmbedding(seq_len, embed_dim, name="PositionalEmbeddingDecoder")

            for i in range(num_layers_dec):
                setattr(self, f"dec_layer_{i}",
                        TransformerDecoder(embed_dim, feed_forward_dim, num_heads, name="Decoder_" + str(i)))

            self.classifier = Dense(num_classes, activation="softmax")

    def decode(self, enc_out, target):
        a = self.dec_enc(target)
        a = self.dec_pos_emb(a)
        for i in range(self.num_layers_dec):
            a = getattr(self, f"dec_layer_{i}")(enc_out, a)
        return a

    def call(self, inputs):
        # print(inputs)
        source = inputs["source"]
        target = inputs["target"]
        # print("source", source)
        # print("target", target)

        # a = self.enc_emb(source)
        # print("a_0", a)
        # a = self.enc_pos_emb(a)
        # print("a_1", a)
        a = self.encoder(source)
        # print("a", a)
        a = self.decode(a, target)

        return self.classifier(a)

    def train_step(self, batch):
        """Processes one batch inside model.fit()."""
        batch = batch[0]
        source = batch["source"]
        target = batch["target"]
        dec_input = target[:, :-1]
        dec_target = target[:, 1:]

        with tf.GradientTape() as tape:
            preds = self({"source": source, "target": dec_input})
            one_hot = tf.one_hot(dec_target, depth=self.num_classes)
            mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
            loss = self.compiled_loss(one_hot, preds, sample_weight=mask)

        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))
        self.loss_metric.update_state(loss)

        return {"loss": self.loss_metric.result()}

    def test_step(self, batch):
        batch = batch[0]
        # print(batch)
        source = batch["source"]
        target = batch["target"]
        dec_input = target[:, :-1]
        dec_target = target[:, 1:]
        # print("dec_input", dec_input)
        # print("dec_target", dec_target)
        # print("source", source)
        preds = self({"source": source, "target": dec_input})
        one_hot = tf.one_hot(dec_target, depth=self.num_classes)
        mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
        loss = self.compiled_loss(one_hot, preds, sample_weight=mask)
        self.loss_metric.update_state(loss)
        return {"loss": self.loss_metric.result()}

    def calc_cer(self, source, target, target_start_token_idx):
        bs = tf.shape(source)[0].numpy()
        # print("bs", bs)
        # print("source", source)
        enc = self.encoder(source)
        # print("enc_shape", enc.shape)
        dec_input = tf.ones((bs, 1), dtype=tf.int32) * target_start_token_idx

        # dim: bs * 1  --> ["<", "<", ... "<"]
        hit_sum = 0
        dec_logits = []
        for i in range(self.seq_len - 1):
            current_target = target[:, i + 1]

            dec_out = self.decode(enc, dec_input)
            logits = self.classifier(dec_out)
            logits = tf.argmax(logits, axis=-1, output_type=tf.int32)
            last_logit = tf.expand_dims(logits[:, -1], axis=-1)
            hits = np.count_nonzero(current_target == last_logit.numpy()[:, 0])

            hit_sum += (hits / self.seq_len)

            dec_logits.append(current_target)
            dec_input = tf.concat([dec_input, np.expand_dims(current_target, axis=1)], axis=-1)

        return hit_sum / bs

    def generate(self, source, target_start_token_idx):
        """Performs inference over one batch of inputs using greedy decoding."""
        bs = tf.shape(source)[0]
        # print("bs", bs)
        # print("source", source)
        enc = self.encoder(source)
        # print("enc_shape", enc.shape)
        dec_input = tf.ones((bs, 1), dtype=tf.int32) * target_start_token_idx

        # dim: bs * 1  --> ["<", "<", ... "<"]

        dec_logits = []
        for i in range(self.seq_len - 1):
            dec_out = self.decode(enc, dec_input)
            logits = self.classifier(dec_out)
            logits = tf.argmax(logits, axis=-1, output_type=tf.int32)
            last_logit = tf.expand_dims(logits[:, -1], axis=-1)
            dec_logits.append(last_logit)
            dec_input = tf.concat([dec_input, last_logit], axis=-1)
        return dec_input

    @property
    def metrics(self):
        return [self.loss_metric]



class TransformerHeadlessPosVisionTrain(Model):

    def __init__(self, vision_net_id,
                 embed_dim=100,
                 num_heads=1,
                 feed_forward_dim=64,
                 seq_len=125,
                 num_layers_enc=4,
                 num_layers_dec=1,
                 num_classes=10,
                 feature_size=(160, 160, 1),
                 **kwargs):

        super().__init__(**kwargs)

        with tf.device("/gpu:0"):
            self.vision_net = get_vision_net(vision_net_id, seq_len)
        with tf.device("/gpu:1"):

            self.loss_metric = Mean(name="loss")
            self.num_layers_enc = num_layers_enc
            self.num_layers_dec = num_layers_dec
            self.seq_len = seq_len
            self.num_classes = num_classes

            # temp = np.product(self.vision_net.layers[-1].output_shape[2:])

            self.enc_pos_emb = PositionalEmbedding(seq_len, embed_dim, name="PositionalEmbeddingEncoder")

            # encoder

            self.encoder = Sequential(name="Encoders")
            self.encoder.add(self.vision_net)

            # self.encoder.add(self.enc_emb)
            self.encoder.add(self.enc_pos_emb)

            for i in range(self.num_layers_enc):
                self.encoder.add(TransformerEncoder(embed_dim, feed_forward_dim, num_heads, name="Encoder_" + str(i)))

            # decoder
            self.dec_enc = TransformerOutputEncoding(seq_len, embed_dim, name="InputEmbeddingDecoder")
            self.dec_pos_emb = PositionalEmbedding(seq_len, embed_dim, name="PositionalEmbeddingDecoder")

            for i in range(num_layers_dec):
                setattr(self, f"dec_layer_{i}",
                        TransformerDecoder(embed_dim, feed_forward_dim, num_heads, name="Decoder_" + str(i)))

            self.classifier = Dense(num_classes, activation="softmax")

    def decode(self, enc_out, target):
        a = self.dec_enc(target)
        a = self.dec_pos_emb(a)
        for i in range(self.num_layers_dec):
            a = getattr(self, f"dec_layer_{i}")(enc_out, a)
        return a

    def call(self, inputs):
        # print(inputs)
        source = inputs["source"]
        target = inputs["target"]
        # print("source", source)
        # print("target", target)

        # a = self.enc_emb(source)
        # print("a_0", a)
        # a = self.enc_pos_emb(a)
        # print("a_1", a)
        a = self.encoder(source)
        # print("a", a)
        a = self.decode(a, target)

        return self.classifier(a)

    def train_step(self, batch):
        """Processes one batch inside model.fit()."""
        batch = batch[0]
        source = batch["source"]
        target = batch["target"]
        dec_input = target[:, :-1]
        dec_target = target[:, 1:]

        with tf.GradientTape() as tape:
            preds = self({"source": source, "target": dec_input})
            one_hot = tf.one_hot(dec_target, depth=self.num_classes)
            mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
            loss = self.compiled_loss(one_hot, preds, sample_weight=mask)

        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))
        self.loss_metric.update_state(loss)

        return {"loss": self.loss_metric.result()}

    def test_step(self, batch):
        batch = batch[0]
        # print(batch)
        source = batch["source"]
        target = batch["target"]
        dec_input = target[:, :-1]
        dec_target = target[:, 1:]
        # print("dec_input", dec_input)
        # print("dec_target", dec_target)
        # print("source", source)
        preds = self({"source": source, "target": dec_input})
        one_hot = tf.one_hot(dec_target, depth=self.num_classes)
        mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
        loss = self.compiled_loss(one_hot, preds, sample_weight=mask)
        self.loss_metric.update_state(loss)
        return {"loss": self.loss_metric.result()}

    def calc_cer(self, source, target, target_start_token_idx):
        bs = tf.shape(source)[0].numpy()
        # print("bs", bs)
        # print("source", source)
        enc = self.encoder(source)
        # print("enc_shape", enc.shape)
        dec_input = tf.ones((bs, 1), dtype=tf.int32) * target_start_token_idx

        # dim: bs * 1  --> ["<", "<", ... "<"]
        hit_sum = 0
        dec_logits = []
        for i in range(self.seq_len - 1):
            current_target = target[:, i + 1]

            dec_out = self.decode(enc, dec_input)
            logits = self.classifier(dec_out)
            logits = tf.argmax(logits, axis=-1, output_type=tf.int32)
            last_logit = tf.expand_dims(logits[:, -1], axis=-1)
            hits = np.count_nonzero(current_target == last_logit.numpy()[:, 0])

            hit_sum += (hits / self.seq_len)

            dec_logits.append(current_target)
            dec_input = tf.concat([dec_input, np.expand_dims(current_target, axis=1)], axis=-1)

        return hit_sum / bs

    def generate(self, source, target_start_token_idx):
        """Performs inference over one batch of inputs using greedy decoding."""
        bs = tf.shape(source)[0]
        # print("bs", bs)
        # print("source", source)
        enc = self.encoder(source)
        # print("enc_shape", enc.shape)
        dec_input = tf.ones((bs, 1), dtype=tf.int32) * target_start_token_idx

        # dim: bs * 1  --> ["<", "<", ... "<"]

        dec_logits = []
        for i in range(self.seq_len - 1):
            dec_out = self.decode(enc, dec_input)
            logits = self.classifier(dec_out)
            logits = tf.argmax(logits, axis=-1, output_type=tf.int32)
            last_logit = tf.expand_dims(logits[:, -1], axis=-1)
            dec_logits.append(last_logit)
            dec_input = tf.concat([dec_input, last_logit], axis=-1)
        return dec_input

    @property
    def metrics(self):
        return [self.loss_metric]

class TransformerHeadlessNoPos(Model):

    def __init__(self, vision_net=None,
               embed_dim=100,
               num_heads=1,
               feed_forward_dim=64,
               seq_len=125,
               num_layers_enc=4,
               num_layers_dec=1,
               num_classes=10,
               feature_size=(160, 160, 1),
               **kwargs):

        super().__init__(**kwargs)

        self.vision_net = vision_net
        self.loss_metric = Mean(name="loss")
        self.num_layers_enc = num_layers_enc
        self.num_layers_dec = num_layers_dec
        self.seq_len = seq_len
        self.num_classes = num_classes

        # temp = np.product(self.vision_net.layers[-1].output_shape[2:])

        # self.enc_pos_emb = PositionalEmbedding(seq_len, embed_dim, name="PositionalEmbeddingEncoder")

        # encoder
        with tf.device("/gpu:1"):

            self.encoder = Sequential(name="Encoders")

            if vision_net is not None:
                self.encoder.add(self.vision_net)
            else:
                in_shape = list()
                in_shape.append(seq_len)
                for f in feature_size:
                    in_shape.append(f)
                in_shape = tuple(in_shape)

                self.encoder.add(Input(in_shape))
            # self.encoder.add(self.enc_emb)
            # self.encoder.add(self.enc_pos_emb)

            for i in range(self.num_layers_enc):
                self.encoder.add(TransformerEncoder(embed_dim, feed_forward_dim, num_heads, name="Encoder_" + str(i)))

        # decoder
        with tf.device("/gpu:0"):
            self.dec_enc = TransformerOutputEncoding(seq_len, embed_dim, name="InputEmbeddingDecoder")
            self.dec_pos_emb = PositionalEmbedding(seq_len, embed_dim, name="PositionalEmbeddingDecoder")

            for i in range(num_layers_dec):
                setattr(self, f"dec_layer_{i}",
                        TransformerDecoder(embed_dim, feed_forward_dim, num_heads, name="Decoder_" + str(i)))

            self.classifier = Dense(num_classes, activation="softmax")

    def decode(self, enc_out, target):
        a = self.dec_enc(target)
        a = self.dec_pos_emb(a)
        for i in range(self.num_layers_dec):
            a = getattr(self, f"dec_layer_{i}")(enc_out, a)
        return a

    def call(self, inputs):
        # print(inputs)
        source = inputs["source"]
        target = inputs["target"]
        # print("source", source)
        # print("target", target)

        # a = self.enc_emb(source)
        # print("a_0", a)
        # a = self.enc_pos_emb(a)
        # print("a_1", a)
        a = self.encoder(source)
        # print("a", a)
        a = self.decode(a, target)

        return self.classifier(a)

    def train_step(self, batch):
        """Processes one batch inside model.fit()."""
        batch = batch[0]
        source = batch["source"]
        target = batch["target"]
        dec_input = target[:, :-1]
        dec_target = target[:, 1:]

        with tf.GradientTape() as tape:

            preds = self({"source": source, "target": dec_input})
            one_hot = tf.one_hot(dec_target, depth=self.num_classes)
            mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
            loss = self.compiled_loss(one_hot, preds, sample_weight=mask)

        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))
        self.loss_metric.update_state(loss)

        return {"loss": self.loss_metric.result()}

    def test_step(self, batch):
        batch = batch[0]
        # print(batch)
        source = batch["source"]
        target = batch["target"]
        dec_input = target[:, :-1]
        dec_target = target[:, 1:]
        # print("dec_input", dec_input)
        # print("dec_target", dec_target)
        # print("source", source)
        preds = self({"source": source, "target": dec_input})
        one_hot = tf.one_hot(dec_target, depth=self.num_classes)
        mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
        loss = self.compiled_loss(one_hot, preds, sample_weight=mask)
        self.loss_metric.update_state(loss)
        return {"loss": self.loss_metric.result()}

    def calc_cer(self, source, target, target_start_token_idx):
        bs = tf.shape(source)[0].numpy()
        # print("bs", bs)
        # print("source", source)
        enc = self.encoder(source)
        # print("enc_shape", enc.shape)
        dec_input = tf.ones((bs, 1), dtype=tf.int32) * target_start_token_idx

        # dim: bs * 1  --> ["<", "<", ... "<"]
        hit_sum = 0
        dec_logits = []
        for i in range(self.seq_len - 1):
            current_target = target[:, i+1]

            dec_out = self.decode(enc, dec_input)
            logits = self.classifier(dec_out)
            logits = tf.argmax(logits, axis=-1, output_type=tf.int32)
            last_logit = tf.expand_dims(logits[:, -1], axis=-1)
            hits = np.count_nonzero(current_target == last_logit.numpy()[:,0])

            hit_sum += (hits / self.seq_len)

            dec_logits.append(current_target)
            dec_input = tf.concat([dec_input, np.expand_dims(current_target, axis=1)], axis=-1)

        return hit_sum / bs

    def generate(self, source, target_start_token_idx):
        """Performs inference over one batch of inputs using greedy decoding."""
        bs = tf.shape(source)[0]
        # print("bs", bs)
        # print("source", source)
        enc = self.encoder(source)
        # print("enc_shape", enc.shape)
        dec_input = tf.ones((bs, 1), dtype=tf.int32) * target_start_token_idx

        # dim: bs * 1  --> ["<", "<", ... "<"]

        dec_logits = []
        for i in range(self.seq_len - 1):
            dec_out = self.decode(enc, dec_input)
            logits = self.classifier(dec_out)
            logits = tf.argmax(logits, axis=-1, output_type=tf.int32)
            last_logit = tf.expand_dims(logits[:, -1], axis=-1)
            dec_logits.append(last_logit)
            dec_input = tf.concat([dec_input, last_logit], axis=-1)
        return dec_input

    @property
    def metrics(self):
        return [self.loss_metric]



class TransformerWithPreprocess(Model):

    def __init__(self,
               embed_dim=100,
               num_heads=1,
               feed_forward_dim=64,
               seq_len=125,
               num_layers_enc=4,
               num_layers_dec=1,
               num_classes=10,
               feature_size=(160, 160, 1),
               **kwargs):

        super().__init__(**kwargs)
        self.loss_metric = Mean(name="loss")
        self.num_layers_enc = num_layers_enc
        self.num_layers_dec = num_layers_dec
        self.seq_len = seq_len
        self.num_classes = num_classes

        self.vision_net = Sequential(name="VisionLayers")
        self.vision_net.add(Input((seq_len, 160, 160, 1)))
        self.vision_net.add(BorderDrop3D(30))
        self.vision_net.add(BatchNormalization())
        self.vision_net.add(Conv3D(filters=64, kernel_size=(5, 7, 7), strides=(1, 2, 2), padding="same"))
        self.vision_net.add(MaxPooling3D(strides=(1, 2, 2),  padding="same"))

        base = 32
        n = 1
        self.vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
        self.vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

        n = 2
        self.vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
        self.vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

        n = 4
        self.vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
        self.vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

        n = 8
        self.vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
        self.vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

        self.vision_net.add(MaxPooling3D(pool_size=(1, 2, 2), strides=(1, 2, 2),  padding="same"))

        temp = np.product(self.vision_net.layers[-1].output_shape[2:])

        self.vision_net.add(Reshape((seq_len, temp)))
        self.vision_net.compile()
        self.vision_net.summary()

        # self.enc_emb = Dense(embed_dim, activation="relu")

        self.enc_pos_emb = PositionalEmbedding(seq_len, embed_dim, name="PositionalEmbeddingEncoder")

        # encoder
        self.encoder = Sequential(name="Encoders")
        self.encoder.add(self.vision_net)
        # self.encoder.add(self.enc_emb)
        self.encoder.add(self.enc_pos_emb)

        for i in range(self.num_layers_enc):
            self.encoder.add(TransformerEncoder(embed_dim, feed_forward_dim, num_heads, name="Encoder_" + str(i)))

        # decoder
        self.dec_enc = TransformerOutputEncoding(seq_len, embed_dim, name="InputEmbeddingDecoder")
        self.dec_pos_emb = PositionalEmbedding(seq_len, embed_dim, name="PositionalEmbeddingDecoder")

        for i in range(num_layers_dec):
            setattr(self, f"dec_layer_{i}",
                    TransformerDecoder(embed_dim, feed_forward_dim, num_heads, name="Decoder_" + str(i)))

        self.classifier = Dense(num_classes, activation="softmax")

    def decode(self, enc_out, target):
        a = self.dec_enc(target)
        a = self.dec_pos_emb(a)
        for i in range(self.num_layers_dec):
            a = getattr(self, f"dec_layer_{i}")(enc_out, a)
        return a

    def call(self, inputs):
        # print(inputs)
        source = inputs["source"]
        target = inputs["target"]
        # print("source", source)
        # print("target", target)

        # a = self.enc_emb(source)
        # print("a_0", a)
        # a = self.enc_pos_emb(a)
        # print("a_1", a)
        a = self.encoder(source)
        # print("a", a)
        a = self.decode(a, target)

        return self.classifier(a)

    def train_step(self, batch):
        """Processes one batch inside model.fit()."""
        batch = batch[0]
        source = batch["source"]
        target = batch["target"]
        dec_input = target[:, :-1]
        dec_target = target[:, 1:]

        with tf.GradientTape() as tape:

            preds = self({"source": source, "target": dec_input})
            one_hot = tf.one_hot(dec_target, depth=self.num_classes)
            mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
            loss = self.compiled_loss(one_hot, preds, sample_weight=mask)

        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))
        self.loss_metric.update_state(loss)

        return {"loss": self.loss_metric.result()}

    def test_step(self, batch):
        batch = batch[0]
        source = batch["source"]
        target = batch["target"]
        dec_input = target[:, :-1]
        dec_target = target[:, 1:]
        # print("dec_input", dec_input)
        # print("dec_target", dec_target)
        # print("source", source)
        preds = self({"source": source, "target": dec_input})
        one_hot = tf.one_hot(dec_target, depth=self.num_classes)
        mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
        loss = self.compiled_loss(one_hot, preds, sample_weight=mask)
        self.loss_metric.update_state(loss)
        return {"loss": self.loss_metric.result()}

    def generate(self, source, target_start_token_idx):
        """Performs inference over one batch of inputs using greedy decoding."""
        bs = tf.shape(source)[0]
        # print("bs", bs)
        # print("source", source)
        enc = self.encoder(source)
        # print("enc_shape", enc.shape)
        dec_input = tf.ones((bs, 1), dtype=tf.int32) * target_start_token_idx

        # dim: bs * 1  --> ["<", "<", ... "<"]

        dec_logits = []
        for i in range(self.seq_len - 1):
            dec_out = self.decode(enc, dec_input)
            logits = self.classifier(dec_out)
            logits = tf.argmax(logits, axis=-1, output_type=tf.int32)
            last_logit = tf.expand_dims(logits[:, -1], axis=-1)
            dec_logits.append(last_logit)
            dec_input = tf.concat([dec_input, last_logit], axis=-1)
        return dec_input

    @property
    def metrics(self):
        return [self.loss_metric]


class Transformer(Model):

    def __init__(self,
               embed_dim=100,
               num_heads=1,
               feed_forward_dim=64,
               seq_len=125,
               num_layers_enc=4,
               num_layers_dec=1,
               num_classes=10,
               feature_size=256,
               **kwargs):
        super().__init__(**kwargs)
        self.loss_metric = Mean(name="loss")
        self.num_layers_enc = num_layers_enc
        self.num_layers_dec = num_layers_dec
        self.seq_len = seq_len
        self.num_classes = num_classes

        # self.enc_emb = InputEmbedding_V1(seq_len, feature_size, embed_dim)
        # self.enc_emb = Sequential()
        # # self.enc_emb.add(Conv1D)
        # self.enc_emb.add(Reshape((seq_len, feature_size)))
        # self.enc_emb.add(Dense(embed_dim))
        self.enc_emb = Dense(embed_dim, activation="relu")

        self.enc_pos_emb = PositionalEmbedding(seq_len, embed_dim, name="PositionalEmbeddingEncoder")

        # encoder
        self.encoder = Sequential(name="Encoders")
        self.encoder.add(self.enc_emb)
        self.encoder.add(self.enc_pos_emb)

        for i in range(self.num_layers_enc):
            self.encoder.add(TransformerEncoder(embed_dim, feed_forward_dim, num_heads, name="Encoder_" + str(i)))

        # decoder
        self.dec_enc = TransformerOutputEncoding(seq_len, embed_dim, name="InputEmbeddingDecoder")
        self.dec_pos_emb = PositionalEmbedding(seq_len, embed_dim, name="PositionalEmbeddingDecoder")

        for i in range(num_layers_dec):
            setattr(self, f"dec_layer_{i}",
                    TransformerDecoder(embed_dim, feed_forward_dim, num_heads, name="Decoder_" + str(i)))

        self.classifier = Dense(num_classes, activation="softmax")

    def decode(self, enc_out, target):
        a = self.dec_enc(target)
        a = self.dec_pos_emb(a)
        for i in range(self.num_layers_dec):
            a = getattr(self, f"dec_layer_{i}")(enc_out, a)
        return a

    def call(self, inputs):
        # print(inputs)
        source = inputs["source"]
        target = inputs["target"]
        # print("source", source)
        # print("target", target)

        # a = self.enc_emb(source)
        # print("a_0", a)
        # a = self.enc_pos_emb(a)
        # print("a_1", a)
        a = self.encoder(source)
        # print("a", a)
        a = self.decode(a, target)

        return self.classifier(a)

    def train_step(self, batch):
        """Processes one batch inside model.fit()."""
        batch = batch[0]
        source = batch["source"]
        target = batch["target"]
        dec_input = target[:, :-1]
        dec_target = target[:, 1:]

        with tf.GradientTape() as tape:

            preds = self({"source": source, "target": dec_input})
            one_hot = tf.one_hot(dec_target, depth=self.num_classes)
            mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
            loss = self.compiled_loss(one_hot, preds, sample_weight=mask)

        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))
        self.loss_metric.update_state(loss)

        return {"loss": self.loss_metric.result()}

    def test_step(self, batch):
        batch = batch[0]
        source = batch["source"]
        target = batch["target"]
        dec_input = target[:, :-1]
        dec_target = target[:, 1:]
        # print("dec_input", dec_input)
        # print("dec_target", dec_target)
        # print("source", source)
        preds = self({"source": source, "target": dec_input})
        one_hot = tf.one_hot(dec_target, depth=self.num_classes)
        mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
        loss = self.compiled_loss(one_hot, preds, sample_weight=mask)
        self.loss_metric.update_state(loss)
        return {"loss": self.loss_metric.result()}

    def calc_cer(self, source, target, target_start_token_idx):
        bs = tf.shape(source)[0]

        enc = self.encoder(source)
        dec_input = tf.ones((bs, 1), dtype=tf.int32) * target_start_token_idx

        dec_logist = []
        for i in range(self.seq_len - 1):
            dec_out = self.decode(enc, dec_input)
            legits = self.classifier(dec_out)

    def generate(self, source, target_start_token_idx):
        """Performs inference over one batch of inputs using greedy decoding."""
        bs = tf.shape(source)[0]
        # print("bs", bs)
        # print("source", source)
        enc = self.encoder(source)
        # print("enc_shape", enc.shape)
        dec_input = tf.ones((bs, 1), dtype=tf.int32) * target_start_token_idx

        # dim: bs * 1  --> ["<", "<", ... "<"]

        dec_logits = []
        for i in range(self.seq_len - 1):
            dec_out = self.decode(enc, dec_input)
            logits = self.classifier(dec_out)
            logits = tf.argmax(logits, axis=-1, output_type=tf.int32)
            last_logit = tf.expand_dims(logits[:, -1], axis=-1)
            dec_logits.append(last_logit)
            dec_input = tf.concat([dec_input, last_logit], axis=-1)
        return dec_input

    @property
    def metrics(self):
        return [self.loss_metric]


class TransformerPackage(Model):

    def __init__(self,
            embed_dim=100,
            num_heads=1,
            feed_forward_dim=64,
            seq_len=125,
            num_layers_enc=4,
            num_layers_dec=1,
            num_classes=10,
            feature_size=256,
            **kwargs
    ):
        super().__init__(**kwargs)
        self.loss_metric = Mean(name="loss")
        self.num_layers_enc = num_layers_enc
        self.num_layers_dec = num_layers_dec
        self.seq_len = seq_len
        self.num_classes = num_classes


        # encoder
        # self.enc_input = Input((seq_len, feature_size, 1))
        self.enc_emb = InputEmbedding(seq_len, feature_size, 1, name="InputEmbeddingEncoder")
        self.enc_pos_emb = PositionalEmbedding(seq_len, embed_dim, name="PositionalEmbeddingEncoder")

        self.encoder = Sequential()
        self.encoder.add(self.enc_emb)
        self.encoder.add(self.enc_pos_emb)
        for i in range(self.num_layers_enc):
            self.encoder.add(TransformerEncoder(embed_dim, feed_forward_dim, num_heads, name="Encoder_" + str(i)))

        # decoder
        # self.dec_input = Input((seq_len, feature_size, 1))
        self.dec_enc = TransformerOutputEncoding(seq_len, embed_dim, name="InputEmbeddingDecoder")
        self.dec_pos_emb = PositionalEmbedding(seq_len, embed_dim, name="PositionalEmbeddingDecoder")


        for i in range(num_layers_dec):
            setattr(self, f"dec_layer_{i}", TransformerDecoder(embed_dim, feed_forward_dim, num_heads, name="Decoder_" + str(i)))

        # self.decoder = Sequential()
        # for _ in range(self.num_layers_dec):
        #     self.decoder.add(TransformerDecoder(embed_dim, feed_forward_dim, num_heads))

        self.classifier = Dense(num_classes, activation="softmax")

    def decode(self, enc_out, target):
        a = self.dec_enc(target)
        a = self.dec_pos_emb(a)
        for i in range(self.num_layers_dec):
            a = getattr(self, f"dec_layer_{i}")(enc_out, a)
        return a

    def call(self, inputs):
        # print(inputs)
        source = inputs[0]
        target = inputs[1]
        # print("source", source)
        # print("taget", target)
        #
        # a = self.enc_emb(source)
        # a = self.enc_pos_emb(a)
        a = self.encoder(source)

        a = self.decode(a, target)

        return self.classifier(a)

    def train_step(self, batch):
        """Processes one batch inside model.fit()."""
        batch = batch[0]
        source = batch[0]
        target = batch[1]
        dec_input = target[:, :-1]
        dec_target = target[:, 1:]

        with tf.GradientTape() as tape:

            preds = self([source, dec_input])
            one_hot = tf.one_hot(dec_target, depth=self.num_classes)
            mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
            loss = self.compiled_loss(one_hot, preds, sample_weight=mask)

        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))
        self.loss_metric.update_state(loss)

        return {"loss": self.loss_metric.result()}

    def test_step(self, batch):
        batch = batch[0]
        source = batch[0]
        target = batch[1]
        dec_input = target[:, :-1]
        dec_target = target[:, 1:]
        preds = self([source, dec_input])
        one_hot = tf.one_hot(dec_target, depth=self.num_classes)
        mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
        loss = self.compiled_loss(one_hot, preds, sample_weight=mask)
        self.loss_metric.update_state(loss)
        return {"loss": self.loss_metric.result()}

    def generate(self, source, target_start_token_idx):
        """Performs inference over one batch of inputs using greedy decoding."""
        bs = tf.shape(source)[0]
        # print("bs", bs)
        # print("source", source)
        enc = self.encoder(source)
        dec_input = tf.ones((bs, 1), dtype=tf.int32) * target_start_token_idx

        # dim: bs * 1  --> ["<", "<", ... "<"]

        dec_logits = []
        for i in range(self.seq_len - 1):
            dec_out = self.decode(enc, dec_input)
            logits = self.classifier(dec_out)
            logits = tf.argmax(logits, axis=-1, output_type=tf.int32)
            last_logit = tf.expand_dims(logits[:, -1], axis=-1)
            dec_logits.append(last_logit)
            dec_input = tf.concat([dec_input, last_logit], axis=-1)
        return dec_input

    @property
    def metrics(self):
        return [self.loss_metric]
