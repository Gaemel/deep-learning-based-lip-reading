import yaml
from lip_data_reader import LipDataReader
import os

def collect_meta_files(parent_dir):
    res_list = list()
    for sub_dir in os.listdir(parent_dir):
        sub_dir = os.path.join(parent_dir, sub_dir)
        if os.path.isdir(sub_dir):
            res_list += collect_meta_files(sub_dir)
        elif sub_dir.endswith(".yaml") and not sub_dir.endswith("_text.yaml"):
            res_list.append(sub_dir)
    return res_list

def replace(meta_path):

    print(meta_path)
    with open(meta_path) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)

    text_path = data["text"]
    new_text_path = os.path.splitext(text_path)[0][:-len("_text")] + ".txt"
    print("renaming: " + str(text_path) + " " + str(new_text_path))
    os.rename(text_path, new_text_path)
    data["text"] = new_text_path

    with open(meta_path, "w") as file:
        yaml.dump(data, file)
    print()



target_dir = """"""

data_path = """/media/ubuntu/data/lip_reading_data/text"""
ldr = LipDataReader(data_path)
a = collect_meta_files(data_path)

for b in a:
    replace(b)

