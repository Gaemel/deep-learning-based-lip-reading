import cv2
from Lip_Detection import load_model, predict_to_points
from copy import copy
import numpy as np
import time

def show_webcam(mirror=False):
    model_number = 37

    target_size = (160, 160)

    model_path = "./classification/model_" + str(model_number) + ".json"
    weights_path = "./classification/weights_" + str(model_number) + ".h5"
    model = load_model(model_number, model_path, weights_path, target_size)


    cam = cv2.VideoCapture(0)
    while True:

        ret_val, img = cam.read()

        temp = int((img.shape[1] - img.shape[0]) / 2)

        img = img[:, temp:-temp]
        img = cv2.resize(img, target_size)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        data_img = copy(img) / 255
        print(data_img.shape)
        data_img = np.expand_dims(data_img, axis=2)
        data_img = np.expand_dims(data_img, axis=0)

        res = model.predict(data_img)[0]
        print(res)
        # p_0, p_1 = predict_to_points(target_size, *res)

        # img_bb = copy(img)

        # img_bb = cv2.rectangle(img_bb, p_0, p_1, (0, 0, 0), 2)

        cv2.imshow("test", img)

        if cv2.waitKey(1) == 27:
            break  # esc to quit

        time.sleep(0.25)

    cv2.destroyAllWindows()


def main():
    show_webcam(mirror=True)


if __name__ == '__main__':
    main()