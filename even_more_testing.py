

import tensorflow as tf
from tensorflow.python.client import device_lib
print(device_lib.list_local_devices())

strategy = tf.distribute.MirroredStrategy()
print('Number of devices: {}'.format(strategy.num_replicas_in_sync))