from tensorflow.keras import Sequential, Model
from tensorflow.keras.utils import plot_model
from tensorflow.keras import applications
from tensorflow.keras import layers
from tensorflow.keras import optimizers
from tensorflow.keras import losses, metrics
from tensorflow.keras.models import model_from_json
import cv2
import numpy as np
import os
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras import backend as K
from tensorflow import keras
from tensorflow.keras.models import Sequential, Model,load_model
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.callbacks import EarlyStopping,ModelCheckpoint
from tensorflow.keras.layers import Input, Add, Dense, Activation, ZeroPadding2D, BatchNormalization, Flatten, Conv2D, AveragePooling2D, MaxPooling2D, GlobalMaxPooling2D,MaxPool2D
from tensorflow.keras.preprocessing import image
from tensorflow.keras.initializers import glorot_uniform



from residual_conv import MyResidualConv2D, MyResidualIdentityConv2D

import gc

import cv2
import numpy as np
import os
from copy import copy

from Data_Loader import Data_Loadder

import tensorflow as tf
from my_iou import My_IOU


iou = My_IOU()

def model_test():
    i = 0
    model_path = "./temp_model.yaml"
    weights_path = "./temp_weights.h5"

    target_size = (160, 160)

    while load_model(i, model_path, weights_path, target_size) is not None:
        i += 1

    print("all ok")


def load_classification_model(index, model_path, weights_path, target_size):
    if index == 0:
        return load_class_model_0(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou")


def load_class_model_0(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):
    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 16

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((10, 10), (10, 10))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=11, strides=(2, 2), activation="relu"))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(2, 2), activation="relu"))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(2, 2), activation="relu"))
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.AveragePooling2D(pool_size=2, strides=(2, 2)))

        model.add(layers.Flatten())
        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(2, activation="softmax"))
    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=["accuracy"])
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model(index, model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):
    if index == 0:
        return load_model_0(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 1:
        return load_model_1(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 2:
        return load_model_2(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 3:
        return load_model_3(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 4:
        return load_model_4(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 5:
        return load_model_5(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 6:
        return load_model_6(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 7:
        return load_model_7(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 8:
        return load_model_8(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 9:
        return load_model_9(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 10:
        return load_model_10(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 11:
        return load_model_11(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 12:
        return load_model_12(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 13:
        return load_model_13(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 14:
        return load_model_14(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 15:
        return load_model_15(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 16:
        return load_model_16(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 17:
        return load_model_17(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 18:
        return load_model_18(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 19:
        return load_model_19(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 20:
        return load_model_20(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 21:
        return load_model_21(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 22:
        return load_model_22(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 23:
        return load_model_23(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 24:
        return load_model_24(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 25:
        return ResNetLike(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 26:
        return load_model_26(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 27:
        return load_model_27(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 28:
        return load_model_28(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 29:
        return load_model_29(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 30:
        return load_model_30(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 31:
        return load_model_31(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 32:
        return load_model_32(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 33:
        return load_model_33(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 34:
        return load_model_34(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 35:
        return load_model_35(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 36:
        return load_model_36(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 37:
        return load_model_37(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 38:
        return load_model_38(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 39:
        return load_model_39(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 40:
        return load_model_40(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 41:
        return load_model_41(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 42:
        return load_model_42(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 43:
        return load_model_43(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 44:
        return load_model_44(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 45:
        return load_model_45(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 46:
        return load_model_46(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 47:
        return load_model_47(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 48:
        return load_model_48(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 49:
        return load_model_49(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 50:
        return load_model_50(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 51:
        return load_model_51(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 52:
        return load_model_52(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 53:
        return load_model_53(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)
    elif index == 54:
        return load_model_54(model_path, weights_path, target_size, output_size, last_activation=last_activation, metric=metric)


    print("no model with index: " + str(index))


def load_model_54(model_path, weights_path, target_size, output_size=4, last_activation="softmax", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json, custom_objects={"MyResidualIdentityConv2D": MyResidualIdentityConv2D, "MyResidualConv2D": MyResidualConv2D})
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        model.add(layers.Input((target_size[0], target_size[1], 1)))

        model.add(Conv2D(128, (51, 51), strides=(11, 11), padding='same'))
        model.add(BatchNormalization(axis=3))

        model.add(layers.GlobalMaxPool2D())

        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou

    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model


def load_model_53(model_path, weights_path, target_size, output_size=4, last_activation="softmax", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json, custom_objects={"MyResidualIdentityConv2D": MyResidualIdentityConv2D, "MyResidualConv2D": MyResidualConv2D})
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        model.add(layers.Input((target_size[0], target_size[1], 1)))

        model.add(Conv2D(64, (7, 7), strides=(2, 2), padding='same'))
        model.add(BatchNormalization(axis=3))
        model.add(MaxPooling2D((3, 3), strides=(2, 2), padding='same'))

        n = 1
        model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))

        n = 2
        model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))

        n = 4
        model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))

        model.add(layers.GlobalMaxPool2D())
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou

    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_52(model_path, weights_path, target_size, output_size=4, last_activation="softmax", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json, custom_objects={"MyResidualIdentityConv2D": MyResidualIdentityConv2D, "MyResidualConv2D": MyResidualConv2D})
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        model.add(layers.Input((target_size[0], target_size[1], 1)))

        model.add(Conv2D(64, (7, 7), strides=(2, 2), padding='same'))
        model.add(BatchNormalization(axis=3))
        model.add(MaxPooling2D((3, 3), strides=(2, 2), padding='same'))

        n = 1
        model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))

        n = 2
        model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))

        n = 4
        model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))

        n = 8
        model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))

        model.add(layers.GlobalMaxPool2D())
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou

    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model


def load_model_51(model_path, weights_path, target_size, output_size=4, last_activation="softmax", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        model.add(layers.Input((target_size[0], target_size[1], 1)))

        model.add(Conv2D(64, (21, 21), strides=(2, 2), padding='same'))
        model.add(BatchNormalization(axis=3))
        model.add(MaxPooling2D((3, 3), strides=(2, 2), padding='same'))

        a = 8
        b = 32

        n = 1
        model.add(MyResidualConv2D([a * n, a * n, b * n], 3, 2))
        model.add(MyResidualIdentityConv2D([a * n, a * n, b * n], 3))

        n = 2
        model.add(MyResidualConv2D([a * n, a * n, b * n], 3, 2))
        model.add(MyResidualIdentityConv2D([a * n, a * n, b * n], 3))

        n = 4
        model.add(MyResidualConv2D([a * n, a * n, b * n], 3, 2))
        model.add(MyResidualIdentityConv2D([a * n, a * n, b * n], 3))

        n = 8
        model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))

        n = 8
        model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))

        model.add(layers.GlobalMaxPool2D())
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou

    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_50(model_path, weights_path, target_size, output_size=4, last_activation="softmax", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        model.add(layers.Input((target_size[0], target_size[1], 1)))

        model.add(Conv2D(64, (7, 7), strides=(2, 2), padding='same'))
        model.add(BatchNormalization(axis=3))
        model.add(MaxPooling2D((3, 3), strides=(2, 2), padding='same'))

        a = 8
        b = 32

        n = 1
        model.add(MyResidualConv2D([a * n, a * n, b * n], 3, 2))
        model.add(MyResidualIdentityConv2D([a * n, a * n, b * n], 3))

        n = 2
        model.add(MyResidualConv2D([a * n, a * n, b * n], 3, 2))
        model.add(MyResidualIdentityConv2D([a * n, a * n, b * n], 3))

        n = 4
        model.add(MyResidualConv2D([a * n, a * n, b * n], 3, 2))
        model.add(MyResidualIdentityConv2D([a * n, a * n, b * n], 3))

        n = 8

        model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))

        model.add(Conv2D(2, (3, 3), strides=(1, 1), padding='valid'))
        model.add(Flatten())
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou

    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model


def load_model_49(model_path, weights_path, target_size, output_size=4, last_activation="softmax", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        model.add(layers.Input((target_size[0], target_size[1], 1)))

        model.add(Conv2D(64, (21, 21), strides=(2, 2), padding='same'))
        model.add(BatchNormalization(axis=3))
        model.add(MaxPooling2D((3, 3), strides=(2, 2), padding='same'))

        a = 8
        b = 32

        n = 1
        model.add(MyResidualConv2D([a * n, a * n, b * n], 3, 2))
        model.add(MyResidualIdentityConv2D([a * n, a * n, b * n], 3))

        n = 2
        model.add(MyResidualConv2D([a * n, a * n, b * n], 3, 2))
        model.add(MyResidualIdentityConv2D([a * n, a * n, b * n], 3))

        n = 4
        model.add(MyResidualConv2D([a * n, a * n, b * n], 3, 2))
        model.add(MyResidualIdentityConv2D([a * n, a * n, b * n], 3))

        n = 8
        model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))

        model.add(layers.GlobalMaxPool2D())
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou

    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model


def load_model_48(model_path, weights_path, target_size, output_size=4, last_activation="softmax", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        model.add(layers.Input((target_size[0], target_size[1], 1)))

        model.add(Conv2D(64, (7, 7), strides=(2, 2), padding='same'))
        model.add(BatchNormalization(axis=3))
        model.add(MaxPooling2D((3, 3), strides=(2, 2), padding='same'))

        a = 8
        b = 32

        n = 1
        model.add(MyResidualConv2D([a * n, a * n, b * n], 3, 2))
        model.add(MyResidualIdentityConv2D([a * n, a * n, b * n], 3))

        n = 2
        model.add(MyResidualConv2D([a * n, a * n, b * n], 3, 2))
        model.add(MyResidualIdentityConv2D([a * n, a * n, b * n], 3))

        n = 4
        model.add(MyResidualConv2D([a * n, a * n, b * n], 3, 2))
        model.add(MyResidualIdentityConv2D([a * n, a * n, b * n], 3))

        n = 8
        model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))


        model.add(layers.GlobalMaxPool2D())
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou

    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model


def load_model_47(model_path, weights_path, target_size, output_size=4, last_activation="softmax", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        model.add(layers.Input((target_size[0], target_size[1], 1)))

        model.add(Conv2D(64, (21, 21), strides=(5, 5), padding='same'))
        model.add(BatchNormalization(axis=3))
        model.add(MaxPooling2D((3, 3), strides=(2, 2), padding='same'))

        n = 1
        model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))

        n = 2
        model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))

        n = 4
        model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))

        n = 8
        model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))


        model.add(layers.GlobalMaxPool2D())
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou

    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model


def load_model_46(model_path, weights_path, target_size, output_size=4, last_activation="softmax", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json, custom_objects={"MyResidualIdentityConv2D": MyResidualIdentityConv2D, "MyResidualConv2D": MyResidualConv2D})
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        model.add(layers.Input((target_size[0], target_size[1], 1)))

        model.add(Conv2D(64, (7, 7), strides=(2, 2), padding='same'))
        model.add(BatchNormalization(axis=3))
        model.add(MaxPooling2D((3, 3), strides=(2, 2), padding='same'))

        n = 1
        model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))

        n = 2
        model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))

        n = 4
        model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))

        n = 8
        model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
        model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))

        model.add(layers.GlobalMaxPool2D())
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou

    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_45(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.BatchNormalization())
        model.add(layers.ZeroPadding2D(padding=((11, 11), (11, 11))))

        model.add(layers.Conv2D(filters=int(1 * n), kernel_size=21, strides=(2, 2), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))

        model.add(layers.GlobalMaxPool2D())

        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_44(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.BatchNormalization())
        model.add(layers.ZeroPadding2D(padding=((11, 11), (11, 11))))

        model.add(layers.Conv2D(filters=int(1 * n), kernel_size=21, strides=(2, 2), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))

        model.add(layers.GlobalMaxPool2D())

        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_43(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.BatchNormalization())
        model.add(layers.ZeroPadding2D(padding=((11, 11), (11, 11))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=21, strides=(2, 2), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=5, strides=(2, 2), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))

        model.add(layers.GlobalMaxPool2D())

        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_42(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.BatchNormalization())
        model.add(layers.ZeroPadding2D(padding=((9, 9), (9, 9))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=21, strides=(2, 2), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Flatten())


        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.categorical_crossentropy, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model


def load_model_41(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.BatchNormalization())
        model.add(layers.ZeroPadding2D(padding=((9, 9), (9, 9))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=21, strides=(2, 2), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))

        model.add(layers.GlobalMaxPool2D())

        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.categorical_crossentropy, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_40(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.BatchNormalization())
        model.add(layers.ZeroPadding2D(padding=((15, 15), (15, 15))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=41, strides=(2, 2), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))

        model.add(layers.GlobalMaxPool2D())

        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_39(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.BatchNormalization())
        model.add(layers.ZeroPadding2D(padding=((9, 9), (9, 9))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=21, strides=(2, 2), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))

        model.add(layers.GlobalMaxPool2D())

        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_38(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.BatchNormalization())
        model.add(layers.ZeroPadding2D(padding=((11, 11), (11, 11))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=21, strides=(2, 2), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))

        model.add(layers.GlobalMaxPool2D())

        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model


def load_model_37(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.BatchNormalization())
        model.add(layers.ZeroPadding2D(padding=((9, 9), (9, 9))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=21, strides=(2, 2), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=9, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))

        model.add(layers.GlobalMaxPool2D())

        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model


def load_model_36(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.BatchNormalization())
        model.add(layers.ZeroPadding2D(padding=((7, 7), (7, 7))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=15, strides=(2, 2), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))

        model.add(layers.GlobalMaxPool2D())

        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_35(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.BatchNormalization())
        model.add(layers.ZeroPadding2D(padding=((11, 11), (11, 11))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=25, strides=(2, 2), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))

        model.add(layers.GlobalMaxPool2D())

        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_34(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):
    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 8

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((7, 7), (7, 7))))

        model.add(layers.Conv2D(filters=int(1 * n), kernel_size=21, strides=(2, 2), activation="relu"))
        model.add(layers.AveragePooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=5, strides=(2, 2), activation="relu"))
        model.add(layers.AveragePooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(2, 2), activation="relu"))
        model.add(layers.GlobalMaxPool2D())

        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(2 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))
    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model


def load_model_33(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):
    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 64

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.Conv2D(filters=int(1 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(1 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.GlobalMaxPool2D())

        model.add(layers.Dropout(rate=0.1))
        model.add(layers.Dense(int(2 * n), activation="relu"))
        model.add(layers.Dropout(rate=0.0125))
        model.add(layers.Dense(int(n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))
    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_32(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.BatchNormalization())
        model.add(layers.ZeroPadding2D(padding=((9, 9), (9, 9))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=21, strides=(2, 2), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))

        model.add(layers.GlobalMaxPool2D())

        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model



def load_model_31(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):


    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.BatchNormalization())
        model.add(layers.ZeroPadding2D(padding=((2, 2), (2, 2))))

        model.add(layers.Conv2D(filters=int(1 * n), kernel_size=5, strides=(2, 2), activation="relu"))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=21, strides=(2, 2), activation="relu"))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=9, strides=(2, 2), activation="relu"))

        model.add(layers.GlobalMaxPool2D())

        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_30(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.BatchNormalization())
        model.add(layers.ZeroPadding2D(padding=((9, 9), (9, 9))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=31, strides=(1, 1), activation="relu"))

        model.add(layers.GlobalMaxPool2D())

        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_29(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.BatchNormalization())
        model.add(layers.ZeroPadding2D(padding=((9, 9), (9, 9))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=21, strides=(1, 1), activation="relu"))

        model.add(layers.GlobalMaxPool2D())

        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_28(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):
    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 64

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.Conv2D(filters=int(1 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(1 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.GlobalMaxPool2D())

        model.add(layers.Dropout(rate=0.1))
        model.add(layers.Dense(int(2 * n), activation="relu"))
        model.add(layers.Dropout(rate=0.0125))
        model.add(layers.Dense(int(n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))
    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_27(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.BatchNormalization())
        model.add(layers.ZeroPadding2D(padding=((9, 9), (9, 9))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=21, strides=(2, 2), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))

        model.add(layers.GlobalMaxPool2D())

        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_26(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):
    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 64

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.Conv2D(filters=int(1 * n), kernel_size=3, strides=(1, 1), activation="relu", padding="same"))
        model.add(layers.Conv2D(filters=int(1 * n), kernel_size=3, strides=(1, 1), activation="relu", padding="same"))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu", padding="same"))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu", padding="same"))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu", padding="same"))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu", padding="same"))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu", padding="same"))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu", padding="same"))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu", padding="same"))
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu", padding="same"))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))

        model.add(layers.Flatten())

        model.add(layers.Dropout(rate=0.1))
        model.add(layers.Dense(int(2 * n), activation="relu"))
        model.add(layers.Dropout(rate=0.0125))
        model.add(layers.Dense(int(n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))
    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model


def load_model_24(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):
    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 16

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((10, 10), (10, 10))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=11, strides=(2, 2), activation="relu", padding="same"))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu", padding="same"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(2, 2), activation="relu", padding="same"))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu", padding="same"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(2, 2), activation="relu", padding="same"))
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu", padding="same"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(16 * n), kernel_size=3, strides=(2, 2), activation="relu", padding="same"))
        model.add(layers.Conv2D(filters=int(16 * n), kernel_size=3, strides=(1, 1), activation="relu", padding="same"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(1, 1)))

        model.add(layers.Flatten())
        model.add(layers.Dense(int(1 * n), activation="relu"))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))
    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model



def load_model_23(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):
    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 16

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((10, 10), (10, 10))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=11, strides=(2, 2), activation="relu"))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Dropout(rate=0.05))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(2, 2), activation="relu"))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Dropout(rate=0.05))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(2, 2), activation="relu"))
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Dropout(rate=0.05))
        model.add(layers.AveragePooling2D(pool_size=2, strides=(2, 2)))

        model.add(layers.Flatten())
        model.add(layers.Dense(int(1 * n), activation="relu"))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))
    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model


def load_model_22(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):
    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 64

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.Conv2D(filters=int(1 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(1 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Dropout(rate=0.1))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Dropout(rate=0.1))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Dropout(rate=0.0125))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Dropout(rate=0.0125))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))

        model.add(layers.Flatten())

        model.add(layers.Dense(int(2 * n), activation="relu"))
        model.add(layers.Dense(int(n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))
    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model


def load_model_21(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):
    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 64

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.Conv2D(filters=int(1 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(1 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))

        model.add(layers.Flatten())

        model.add(layers.Dropout(rate=0.1))
        model.add(layers.Dense(int(2 * n), activation="relu"))
        model.add(layers.Dropout(rate=0.0125))
        model.add(layers.Dense(int(n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))
    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_20(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):
    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 64

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.Conv2D(filters=int(1 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(1 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))

        model.add(layers.Flatten())

        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(2 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))
    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_19(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):
    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 8

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((7, 7), (7, 7))))

        model.add(layers.Conv2D(filters=int(1 * n), kernel_size=21, strides=(3, 3), activation="relu"))
        model.add(layers.AveragePooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=9, strides=(3, 3), activation="relu"))
        model.add(layers.MaxPooling2D(pool_size=2, strides=(2, 2)))

        model.add(layers.Flatten())
        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(2 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))
    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_18(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):
    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 8

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((7, 7), (7, 7))))

        model.add(layers.Conv2D(filters=int(1 * n), kernel_size=21, strides=(2, 2), activation="relu"))
        model.add(layers.AveragePooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=5, strides=(2, 2), activation="relu"))
        model.add(layers.AveragePooling2D(pool_size=2, strides=(2, 2)))

        model.add(layers.Flatten())
        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(2 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))
    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_17(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):
    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 8

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((7, 7), (7, 7))))

        model.add(layers.Conv2D(filters=int(1 * n), kernel_size=21, strides=(2, 2), activation="relu"))
        model.add(layers.AveragePooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=5, strides=(2, 2), activation="relu"))
        model.add(layers.AveragePooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(2, 2), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))

        model.add(layers.Flatten())
        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(2 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))
    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model


def load_model_16(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):
    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 8

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((7, 7), (7, 7))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=21, strides=(2, 2), activation="relu"))
        model.add(layers.AveragePooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=5, strides=(2, 2), activation="relu"))
        model.add(layers.AveragePooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(2, 2), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))

        model.add(layers.Flatten())
        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(2 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))
    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_15(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 64

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((10, 10), (10, 10))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=21, strides=(2, 2), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))

        model.add(layers.Flatten())
        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.25 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model



def load_model_14(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((10, 10), (10, 10))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=21, strides=(2, 2), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))

        model.add(layers.Flatten())
        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.25 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model


def load_model_13(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.BatchNormalization())
        model.add(layers.ZeroPadding2D(padding=((9, 9), (9, 9))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=21, strides=(2, 2), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))

        model.add(layers.Flatten())
        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model


def load_model_12(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((9, 9), (2, 2))))

        m = n

        model.add(layers.Conv2D(filters=int(m), kernel_size=21, strides=(3, 3), activation="relu"))
        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.BatchNormalization())
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        m *= 2

        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.BatchNormalization())
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        m *= 2

        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.BatchNormalization())


        model.add(layers.Flatten())
        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(2 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model



def load_model_11(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((2, 2), (2, 2))))

        m = n

        model.add(layers.Conv2D(filters=int(m), kernel_size=5, strides=(3, 3), activation="relu"))
        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        m *= 2

        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        m *= 2

        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))

        model.add(layers.Flatten())
        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(2 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model



def load_model_10(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((9, 9), (9, 9))))

        m = n

        model.add(layers.Conv2D(filters=int(m), kernel_size=21, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        m *= 2

        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        m *= 2

        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        m *= 2

        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        m *= 2



        model.add(layers.Flatten())
        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(2 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model



def load_model_9(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((1, 1), (1, 1))))

        m = n

        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        m *= 2

        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        m *= 2

        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        m *= 2

        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(m), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        m *= 2



        model.add(layers.Flatten())
        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(2 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model



def load_model_8(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((9, 9), (9, 9))))

        model.add(layers.Conv2D(filters=int(1 * n), kernel_size=21, strides=(3, 3), activation="relu"))
        # model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))

        model.add(layers.Flatten())
        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(2 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model


def load_model_7(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((10, 10), (10, 10))))

        model.add(layers.Conv2D(filters=int(1 * n), kernel_size=21, strides=(2, 2), activation="relu"))
        # model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))

        model.add(layers.Flatten())
        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(2 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_6(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 16

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((9, 9), (9, 9))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=21, strides=(5, 5), activation="relu"))
        model.add(layers.AveragePooling2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=7, strides=(3, 3), activation="relu"))
        model.add(layers.AveragePooling2D(pool_size=2, strides=(2, 2)))

        model.add(layers.Flatten())
        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_5(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 16

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((4, 4), (4, 4))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=7, strides=(2, 2), activation="relu"))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=2, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(2, 2), activation="relu"))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=2, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(2, 2), activation="relu"))
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))

        model.add(layers.Flatten())
        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error,
                  metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model


def load_model_4(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 16

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((3, 3), (3, 3))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=5, strides=(2, 2), activation="relu"))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(2, 2), activation="relu"))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(2, 2), activation="relu"))
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))

        model.add(layers.Flatten())
        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model


def load_model_3(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 16

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((10, 10), (10, 10))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=11, strides=(2, 2), activation="relu"))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(2, 2), activation="relu"))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(2, 2), activation="relu"))
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.AveragePooling2D(pool_size=2, strides=(2, 2)))

        model.add(layers.Flatten())
        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model


def load_model_2(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 16

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((10, 10), (10, 10))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=21, strides=(2, 2), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))

        model.add(layers.Flatten())
        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))
    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model



def load_model_1(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((10, 10), (10, 10))))

        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=21, strides=(2, 2), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(8 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))

        model.add(layers.Flatten())
        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(0.5 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))
    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric], run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def load_model_0(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        name = os.path.split(os.path.splitext(model_path)[0])[-1]
        model = Sequential(name=name)

        n = 32

        model.add(layers.Input((target_size[0], target_size[1], 1)))
        # model.add(layers.BatchNormalization(axis=3, epsilon=0.000001, momentum=0.99))

        model.add(layers.ZeroPadding2D(padding=((10, 10), (10, 10))))

        model.add(layers.Conv2D(filters=int(1 * n), kernel_size=21, strides=(2, 2), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(2 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))
        model.add(layers.Conv2D(filters=int(4 * n), kernel_size=3, strides=(1, 1), activation="relu"))
        model.add(layers.MaxPool2D(pool_size=2, strides=(2, 2)))

        model.add(layers.Flatten())
        # model.add(layers.Dropout(rate=0.25))
        model.add(layers.Dense(int(2 * n), activation="relu"))
        # model.add(layers.Dropout(rate=0.125))
        model.add(layers.Dense(int(1 * n), activation="relu"))
        model.add(layers.Dense(output_size, activation=last_activation))

    # global iou
    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=metric, run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model


def identity_block(X, f, filters, stage, block):
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'
    F1, F2, F3 = filters

    X_shortcut = X

    X = Conv2D(filters=F1, kernel_size=(1, 1), strides=(1, 1), padding='valid', name=conv_name_base + '2a',
               kernel_initializer=glorot_uniform(seed=0))(X)
    X = BatchNormalization(axis=3, name=bn_name_base + '2a')(X)
    X = Activation('relu')(X)

    X = Conv2D(filters=F2, kernel_size=(f, f), strides=(1, 1), padding='same', name=conv_name_base + '2b',
               kernel_initializer=glorot_uniform(seed=0))(X)
    X = BatchNormalization(axis=3, name=bn_name_base + '2b')(X)
    X = Activation('relu')(X)

    X = Conv2D(filters=F3, kernel_size=(1, 1), strides=(1, 1), padding='valid', name=conv_name_base + '2c',
               kernel_initializer=glorot_uniform(seed=0))(X)
    X = BatchNormalization(axis=3, name=bn_name_base + '2c')(X)

    X = Add()([X, X_shortcut])  # SKIP Connection
    X = Activation('relu')(X)

    return X


def convolutional_block(X, f, filters, stage, block, s=2):
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    F1, F2, F3 = filters

    X_shortcut = X

    X = Conv2D(filters=F1, kernel_size=(1, 1), strides=(s, s), padding='valid', name=conv_name_base + '2a',
               kernel_initializer=glorot_uniform(seed=0))(X)
    X = BatchNormalization(axis=3, name=bn_name_base + '2a')(X)
    X = Activation('relu')(X)

    X = Conv2D(filters=F2, kernel_size=(f, f), strides=(1, 1), padding='same', name=conv_name_base + '2b',
               kernel_initializer=glorot_uniform(seed=0))(X)
    X = BatchNormalization(axis=3, name=bn_name_base + '2b')(X)
    X = Activation('relu')(X)

    X = Conv2D(filters=F3, kernel_size=(1, 1), strides=(1, 1), padding='valid', name=conv_name_base + '2c',
               kernel_initializer=glorot_uniform(seed=0))(X)
    X = BatchNormalization(axis=3, name=bn_name_base + '2c')(X)

    X_shortcut = Conv2D(filters=F3, kernel_size=(1, 1), strides=(s, s), padding='valid', name=conv_name_base + '1',
                        kernel_initializer=glorot_uniform(seed=0))(X_shortcut)
    X_shortcut = BatchNormalization(axis=3, name=bn_name_base + '1')(X_shortcut)

    X = Add()([X, X_shortcut])
    X = Activation('relu')(X)

    return X


def ResNetLike(model_path, weights_path, target_size, output_size=4, last_activation="sigmoid", metric="iou"):
    if len(target_size) == 2:
        target_size = (target_size[0], target_size[1], 1)

    if os.path.exists(model_path):
        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        model = model_from_json(model_json)
        model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

    else:
        X_input = Input(target_size)

        X = ZeroPadding2D((3, 3))(X_input)

        X = Conv2D(16, (7, 7), strides=(2, 2), name='conv1', kernel_initializer=glorot_uniform(seed=0))(X)
        X = BatchNormalization(axis=3, name='bn_conv1')(X)
        X = Activation('relu')(X)
        X = MaxPooling2D((3, 3), strides=(2, 2))(X)

        X = convolutional_block(X, f=3, filters=[16, 16, 160], stage=2, block='a', s=1)
        X = identity_block(X, 3, [16, 16, 160], stage=2, block='b')
        X = identity_block(X, 3, [16, 16, 160], stage=2, block='c')

        X = convolutional_block(X, f=3, filters=[16*2, 16*2, 160*2], stage=3, block='a', s=2)
        X = identity_block(X, 3, [16*2, 16*2, 160*2], stage=3, block='b')
        X = identity_block(X, 3, [16*2, 16*2, 160*2], stage=3, block='c')
        X = identity_block(X, 3, [16*2, 16*2, 160*2], stage=3, block='d')

        X = convolutional_block(X, f=3, filters=[16*4, 16*4, 160*4], stage=4, block='a', s=2)
        X = identity_block(X, 3, [16*4, 16*4, 160*4], stage=4, block='b')
        X = identity_block(X, 3, [16*4, 16*4, 160*4], stage=4, block='c')
        X = identity_block(X, 3, [16*4, 16*4, 160*4], stage=4, block='d')

        X = AveragePooling2D(pool_size=(2, 2), padding='same')(X)

        X = Dense(256, activation='relu')(X)
        X = Dense(128, activation='relu')(X)
        X = Dense(output_size, activation=last_activation)(X)

        model = Model(inputs=X_input, outputs=X, name='ResNet_like')
    if metric == "iou":
        metric = iou
    model.compile(optimizer=optimizers.Adam(learning_rate=0.000001), loss=losses.mean_squared_error, metrics=[metric],
                  run_eagerly=True)
    plot_model(model, to_file=str(os.path.splitext(model_path)[0]) + ".png", expand_nested=True, show_shapes=True)
    model.summary()
    return model

def vg16_test():
    model = applications.VGG16()
    model.summary()

def test_one():
    i = 54
    model_path = "./temp_model.yaml"
    weights_path = "./temp_weights.h5"

    target_size = (160, 160)

    load_model(i, model_path, weights_path, target_size)


if __name__ == '__main__':
    test_one()