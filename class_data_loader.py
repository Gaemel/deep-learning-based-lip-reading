import os
import numpy as np
import cv2
from copy import copy

def my_one_hot(size, label):
    temp = np.zeros((size))
    temp[label] = 1

    return temp


def class_data_loader(target_path, test_data_part=0.04):

    train_data = list()
    train_labels = list()

    test_data = list()
    test_labels = list()


    for dir in os.listdir(target_path):
        label = my_one_hot(len(os.listdir(target_path)), int(dir))
        temp_data = list()
        temp_labels = list()

        for file in os.listdir(os.path.join(target_path, dir)):
            if not file.endswith(".png"):
                continue

            temp_data.append(cv2.cvtColor(cv2.imread(os.path.join(target_path, dir, file)), cv2.COLOR_BGR2GRAY) / 255)
            temp_labels.append(copy(label))

        if test_data_part is not None and test_data_part >= 0:
            part = int(len(temp_data) * test_data_part)

            test_data += temp_data[:part]
            test_labels += temp_labels[:part]

            train_data += temp_data[part:]
            train_labels += temp_labels[part:]

    train_data = np.array(train_data)
    train_labels = np.array(train_labels)
    test_data = np.array(test_data)
    test_labels = np.array(test_labels)

    train_data = np.expand_dims(train_data, axis=3)
    test_data = np.expand_dims(test_data, axis=3)

    return train_data, train_labels, test_data, test_labels


if __name__ == '__main__':
    target_path = "/media/ubuntu/data/lip_reading_data/detection_data"

    class_data_loader(target_path)