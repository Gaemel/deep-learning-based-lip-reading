from tensorflow.keras.layers import Layer, Embedding, Input, ZeroPadding1D
from tensorflow.keras.models import Model
from tensorflow.keras.metrics import CategoricalCrossentropy, Mean
import tensorflow as tf
import numpy as np
from MyTransformer_v2 import TransformerSelfAttention, DecoderEncoderAttention, ResidualFeedForward, TransformerOutputEncoding
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.losses import CategoricalCrossentropy
from tensorflow.python.ops import array_ops
from tensorflow.python.framework import tensor_shape

class Paralel_Embedding(Layer):

    def __init__(self, embedding_count, input_dim, output_dim, **kwargs):
        super().__init__(**kwargs)
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.embedding_count = embedding_count

        self.embeddings = list()
        for x in range(self.embedding_count):
            self.embeddings.append(Embedding(input_dim, output_dim, name=self.name + "_" + str(x)))

    def call(self, inputs, channels):
        bs = inputs.shape[0]

        if bs is None:
            print("!!!")
            return tf.zeros((self.input_dim, self.output_dim))

        zeros = tf.zeros((self.input_dim, ))

        res = list()
        for x in range(bs):
            c = channels[x]
            temp = self.embeddings[c](zeros)
            # print(c)
            res.append(temp)

        res = tf.stack(res)
        print("???")
        return res

class Transformer_Type_A(Model):

    def __init__(self, seq_len, model_dim, codec_count=2, **kwargs):
        super().__init__(**kwargs)

        self.loss_metric = CategoricalCrossentropy(name="loss")

        self.seq_len = seq_len
        self.model_dim = model_dim
        self.input_layer = Input((self.seq_len, self.model_dim))

        self.embedding = Paralel_Embedding(codec_count, seq_len, model_dim)


    def call(self, inputs):
        data = inputs[0]
        channel = inputs[1]

        x = self.embedding(data, channel)
        print("x")
        print(x)
        return x


def some_test():
    bs = 4
    seq_len = 2
    model_dim = 5

    a = Transformer_Type_A(seq_len, model_dim, 2)
    channel = np.array([0, 1]*int(bs/2))
    some_inputs = np.ones((bs, seq_len, model_dim))
    b = a.predict([some_inputs, channel])

    print("b")
    print(b)
    print()
    print("in")
    print(some_inputs)


class My_Padding(Layer):

    def __init__(self, out_size, **kwargs):
        super().__init__(**kwargs)
        self.out_size = out_size

    def call(self, inputs):
        print(inputs.shape)
        print(type(inputs))
        bs = inputs.shape[0]
        in_len = inputs.shape[1]

        if bs is None:
            return inputs

        zeros = tf.zeros((bs, self.out_size - in_len))
        temp = tf.concat([inputs, zeros], 1)

        return temp

    def compute_output_shape(self, input_shape):
        return tensor_shape.TensorShape([input_shape[0], self.out_size])

class test_model(Model):

    def __init__(self, key_dim, num_heads, model_dim, seq_len, **kwargs):
        super().__init__(**kwargs)
        dropout_rate = 0.1
        self.pd = My_Padding(seq_len)
        self.emb = Embedding(seq_len, model_dim)
        #
        # self.self_att = TransformerSelfAttention(key_dim, num_heads, dropout_rate, use_mask=False)
        # self.enc_att = DecoderEncoderAttention(key_dim, num_heads, dropout_rate)
        # self.ff = ResidualFeedForward(model_dim, model_dim)

    def call(self, inputs):
        enc_out, target = inputs
        print("target shape", target.shape)
        x = self.pd(target)
        a = self.emb(x)
        print("a", a.shape)
        return a
        # b = self.self_att(a)
        # print("b", b.shape)
        # c = self.enc_att(enc_out, b)
        # print("c", c.shape)
        # return self.ff(c)

def some_test_2():
    pd = My_Padding(10)

    ones = np.ones((3, 8, 1))
    print(ones)
    print(pd(ones).numpy())

def some_test_1():
    bs = 2
    seq_len = 10
    model_dim = 5
    key_dim = 4
    num_heads = 2
    dropout_rate = 0.1

    ones_0 = np.ones((bs, seq_len, model_dim))

    # emb = Embedding(seq_len, model_dim)
    # blub = emb(ones_0).numpy()

    tm = test_model(key_dim=key_dim, num_heads=num_heads, model_dim=model_dim, seq_len=seq_len)
    # tm.compile(optimizer=Adam(0.001), loss=CategoricalCrossentropy())

    for x in range(1, 10):
        ones_1 = np.ones((bs, x))

        res = tm.predict((ones_0, ones_1))
        print(res.shape)

    print(res)
    tm.summary()

def next_test():
    for x in range(100):
        r = np.random.randint(1, 10)
        print(r)

if __name__ == '__main__':
    next_test()
