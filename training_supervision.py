import yaml
import os
import pandas as pd

from LipReadinTrainer import Trainer


def load_config(config_path):
    with open(config_path) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    return data


def pop_target(config):
    model = config["models"]
    if len(model.keys()) <= 0:
        return None

    key = list(model.keys())[0]
    content = model.pop(key)
    config["models"] = model

    return key, content


def get_epoch(save_path):
    if not os.path.exists(save_path):
        return 0

    for sub in os.listdir(save_path):
        if sub.endswith("hist.csv"):
            df = pd.read_csv(os.path.join(save_path, sub))
            return df.shape[0]

    return 0


def some_test():
    some_path = """/media/ubuntu/data/main_saves/1/"""
    a = get_epoch(some_path)
    print()


def main():
    config_path = "training_config.yaml"

    config = load_config(config_path)
    max_epoch = config["epochs"]
    learning_rat = config["learning_rate"]

    test_steps = config["test_steps"]
    max_seq_len = config["max_seq_len"]
    preprocessed_data = config["preprocessed_data"]
    interval_saves = config["interval_saves"]
    cropped = (100, 100, 1)

    while True:

        next_model = pop_target(config)

        if next_model is None:
            break

        next_model_key, next_model_config = next_model

        train_save_path = os.path.join(config["save_location"], next_model_key)
        start_epoch = get_epoch(train_save_path)
        print("start_epoch", start_epoch)

        if start_epoch >= max_epoch:
            continue

        print("start training for: " + str(next_model) + " on epoch: " + str(start_epoch))

        batch_size = next_model_config["batch_size"]
        test_data_size = next_model_config["test_data_size"]

        t = Trainer(data_paths=next_model_config["train_data_sets"],
                    model_number=next_model_config["model_number"],
                    save_location=train_save_path,
                    batch_size=batch_size,
                    test_step=test_steps,
                    use_pre_processed_data=preprocessed_data,
                    max_seq_len=max_seq_len,
                    load=start_epoch != 0,
                    test_data_size=test_data_size,
                    lr=learning_rat,
                    cropped=cropped,
                    interval_saves=interval_saves,
                    sub_batch=batch_size)
        t.train(max_epoch, start_epoch)



        print("training done")

    print("all done")

if __name__ == '__main__':
    main()

