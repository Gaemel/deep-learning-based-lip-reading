import os
from copy import copy
from MyTransformers_V5 import MyExplicitSwitchTransformerWorkaround, MyExplicitSwitchTransformerWorkaroundTail
import numpy as np

os.environ["TF_MIN_GPU_MULTIPROCESSOR_COUNT"]="6"

from lip_data_reader import LipDataReader, VectorizeChar
from ModelSaver import Saver

from TransformerModels import get_transformer_model, compile_model
from tensorflow.keras.callbacks import Callback
import tensorflow as tf

import gc


class DisplayOutputs(Callback):
    def __init__(
        self, batch, idx_to_token, target_start_token_idx=0, target_end_token_idx=1
    ):
        """Displays a batch of outputs after every epoch

        Args:
            batch: A test batch containing the keys "source" and "target"
            idx_to_token: A List containing the vocabulary tokens corresponding to their indices
            target_start_token_idx: A start token index in the target vocabulary
            target_end_token_idx: An end token index in the target vocabulary
        """
        self.batch = batch
        self.target_start_token_idx = target_start_token_idx
        self.target_end_token_idx = target_end_token_idx
        self.idx_to_char = idx_to_token

    def on_epoch_end(self, epoch, logs=None):
        if epoch % 5 != 0:
            return
        source = self.batch["source"]
        target = self.batch["target"]
        bs = tf.shape(source)[0]
        preds = self.model.generate(source, self.target_start_token_idx)
        preds = preds.numpy()
        for i in range(bs):
            target_text = "".join([self.idx_to_char[_] for _ in target[i, :]])
            prediction = ""
            for idx in preds[i, :]:
                prediction += self.idx_to_char[idx]
                if idx == self.target_end_token_idx:
                    break
            print(f"target:     {target_text.replace('-','')}")
            print(f"prediction: {prediction}\n")


class Trainer():

    def __init__(self, data_paths, model_number, save_location, batch_size=None, test_step=1, use_pre_processed_data=True, max_seq_len=25*5, load=False, test_data_size=100, lr=None, cropped=None, interval_saves=None, sub_batch=None, include_indexes=False):
        metrics = ["loss", "test_loss", "nce", "test_nce"]
        line_styles = dict()
        for key in data_paths.keys():
            metrics.append(str(key) + "_test_loss")
            line_styles[str(key) + "_test_loss"] = "--"

            metrics.append(str(key) + "_test_nce")
            line_styles[str(key) + "_test_nce"] = "dashdot"

        if use_pre_processed_data:
            in_shape = (512, )
        else:
            if cropped is not None:
                in_shape = cropped
            else:
                in_shape = (160, 160, 1)
        self.include_indexes = include_indexes
        self.sub_batch = sub_batch
        self.lr = lr
        if isinstance(lr, dict):
            lr = lr[0]
        print("vocab size: ", len(VectorizeChar.get_vocabulary()))
        load_target = Saver.BEST_WIGHTS
        if isinstance(load,  str):
            load_target = load
            load = True

        if load:
            self._model = None
            self._load(True)
            self._saver = Saver.from_data(save_location, metrics=metrics, main_metric="test_loss", model_title="model_" + str(model_number))
            self._saver.set_smooth_plot("loss")
            self._model = get_transformer_model(model_number, max_seq_len, in_shape, len(VectorizeChar.get_vocabulary()), build_bs=sub_batch, lr=lr, bs=sub_batch)
            self._model = self._saver.load_weights(self._model, load_target)

        else:
            self._model = get_transformer_model(model_number, max_seq_len, in_shape, len(VectorizeChar.get_vocabulary()), build_bs=sub_batch, lr=lr, bs=sub_batch)
            self._model.summary()
            self._saver = Saver(save_location, metrics=metrics, main_metric="test_loss", save_dir_title=None,
                                iter_names=True, verbose=True, model_title="model_" + str(model_number), smooth_plot="loss")

        self._saver.set_filter({"loss":21, "val_loss": 5})
        self._saver.set_line_styles(line_styles)
        self._data_loader = LipDataReader(data_paths, batch_size, pre_processed=use_pre_processed_data, max_input_len=max_seq_len, max_text_len=max_seq_len, test_data_size=test_data_size)

        self._model.summary()
        self._test_step = test_step
        self._batch_size = batch_size
        self._epoch = 0
        self.warm_up = True
        self._current_lr = None
        self.interval_saves = interval_saves

    def set_epoch(self, epoch):
        if isinstance(self.lr, dict):

            self._epoch = 0
            for e in range(epoch):
                next_e_step = self.get_next_lr()
                if next_e_step is not None and self._epoch == next_e_step:
                    compile_model(self._model, self.lr[next_e_step])
                    self._current_lr = self.lr[next_e_step]
                self._epoch += 1

        self._epoch = epoch

    def _load(self, most_recent=True):
        self._load_epoch()

    def _save(self, hist, test_keys=None, produce_img=True):
        print("save model")
        loss = hist["loss"][0]

        score_dict = {"loss": loss}
        if "nce" in hist:
            score_dict["nce"] = hist["nce"]

        if test_keys is not None:
            temp_loss = 0
            temp_nce = 0.0
            count = 0

            for key in test_keys:
                if key + "_test_nce" in hist:
                    score_dict[key + "_test_nce"] = hist[key + "_test_nce"]
                if key not in hist:
                    continue

                score_dict[str(key) + "_test_loss"] = hist[key]
                temp_loss += hist[key]
                temp_nce += hist[key + "_test_nce"]
                count += 1

            if count > 0:
                temp_loss /= count
                temp_nce /= count
                score_dict["test_loss"] = temp_loss
                score_dict["test_nce"] = temp_nce

        self._saver.next_data_point(self._model, score_dict, do_plot=produce_img)

    def _pack(self, data):
        if isinstance(data, dict):
            temp = dict()
            for key in data.keys():
                temp[key] = self._pack(data[key])
            return temp
        else:
            if self.include_indexes:
                return {"source": data[0], "target": data[1], "index": data[2]}
            else:
                return {"source": data[0], "target": data[1]}

    def _load_next_batch(self):
        print("preparing data")
        return self._pack(self._data_loader.create_train_dataset())

    def _load_test_data(self, as_dict=False):
        print("preparing test data")
        return self._pack(self._data_loader.create_test_dataset(as_dict=as_dict))

    def _load_epoch(self):
        self._epoch = 0

    def _create_display_callback(self, ds):
        return DisplayOutputs(ds, self._data_loader.get_vocab(),
                              target_start_token_idx=self._data_loader.get_vectorizer().get_start_token(),
                              target_end_token_idx=self._data_loader.get_vectorizer().get_end_token())

    def calc_next_char_metric(self, data, split=None):
        if split is not None:

            temp = 0
            data_len = int(data["source"].shape[0] / split)

            for x in range(split):
                a = data_len*x
                b = data_len*(x+1)-1
                c = data["source"][a:b]
                temp += self._model.calc_cer(data["source"][data_len * x:data_len * (x + 1) - 1], data["target"][data_len * x:data_len * (x + 1) - 1], self._data_loader.get_vectorizer().get_start_token())

            return temp / split

        if self.include_indexes:
            return self._model.calc_cer(data["source"], data["target"], data["indexes"], self._data_loader.get_vectorizer().get_start_token())
        else:
            return self._model.calc_cer(data["source"], data["target"], self._data_loader.get_vectorizer().get_start_token())

    def get_next_lr(self):
        if not isinstance(self.lr, dict):
            return None

        ep_list = list(self.lr.keys())
        ep_list.sort()

        for e in ep_list:
            if e >= self._epoch:
                return e

        return None

    def train(self, number_of_epochs, start_epoch=0):
        self._load()


        ds = self._load_next_batch()
        test_ds = self._load_test_data(as_dict=True)
        display_cb = self._create_display_callback(test_ds)
        self.set_epoch(start_epoch)

        while self._epoch < number_of_epochs:
            if self.interval_saves is not None and self._epoch % self.interval_saves == 0 and self._epoch != 0:
                self._saver.save_interval_weights(self._model, self._epoch)

            next_e_step = self.get_next_lr()

            if next_e_step is not None and self._epoch == next_e_step:
                compile_model(self._model, self.lr[next_e_step])
                self._current_lr = self.lr[next_e_step]
                print("update lr on epoch: ", str(self._epoch), " to ", str(self._current_lr))

            print("current epoch: ", str(self._epoch))
            print("batch size: ", str(len(ds["source"])))
            print("lr: " + str(self._current_lr))
            print("start fit")
            print(ds["source"].shape)
            hist = self._model.fit(ds, batch_size=self.sub_batch).history

            if self._epoch % self._test_step == 0:

                hist["nce"] = self.calc_next_char_metric(ds, 16)
                del ds

                for i, key in enumerate(test_ds.keys()):
                    if isinstance(self._model, MyExplicitSwitchTransformerWorkaroundTail) or isinstance(self._model, MyExplicitSwitchTransformerWorkaround):
                        self._model.set_mode(i)

                    hist[key] = self._model.evaluate(test_ds[key], batch_size=len(test_ds[key]))
                    hist[key + "_test_nce"] = self.calc_next_char_metric(test_ds[key])

                if isinstance(self._model, MyExplicitSwitchTransformerWorkaroundTail) or isinstance(self._model, MyExplicitSwitchTransformerWorkaround):
                    self._model.set_mode(None)

            elif self._batch_size is not None:
                del ds

            self._save(hist, test_ds.keys(), self._epoch % 10 == 0)
            gc.collect()

            self._epoch += 1
            if self._epoch < number_of_epochs and self._batch_size is not None:
                ds = self._load_next_batch()


def pretrain():
    # data_path = """/media/ubuntu/data/combined/pre_processed"""
    data_path = """/media/ubuntu/data/combined/pre_train"""
    model_number = 10
    save_location = "/media/ubuntu/data/saves/"
    batch_size = 1000
    preprocessed = False

    t = Trainer(data_path, model_number, save_location, batch_size, use_pre_processed_data=preprocessed, max_seq_len=44, test_step=5)

    t.train(100000)
    print("done")


def continue_pretraining():
    data_path = """/media/ubuntu/data/combined/pre_train"""
    model_number = 7
    save_location = "/media/ubuntu/data/saves/3"
    batch_size = 1000
    preprocessed = False

    t = Trainer(data_path, model_number, save_location, batch_size, use_pre_processed_data=preprocessed, max_seq_len=44, test_step=10, load=True)

    t.train(100000)
    print("done")


def main_train():
    data_path = {"eng_pre": """/media/ubuntu/data/combined/100/english_pre"""}
    model_number = 3003
    save_location = "/media/ubuntu/data/main_saves/new_test"
    batch_size = 64
    sub_batch = 64
    test_data_size = 32
    preprocessed = True
    interval_saves = 200
    include_data_index = False

    cropped = (100, 100, 1)
    lr = {0: 0.0001, 100: 0.0001}
    t = Trainer(data_path, model_number, save_location, batch_size, use_pre_processed_data=preprocessed, max_seq_len=75, test_step=100, test_data_size=test_data_size, lr=lr, cropped=cropped, interval_saves=interval_saves, sub_batch=sub_batch, include_indexes=include_data_index)

    t.train(100000)
    print("done")


def continue_maintraining():
    data_path = {"eng_pre": """/media/ubuntu/data/combined/100/english_pre"""}
    model_number = 2015
    save_location = "/media/ubuntu/data/main_saves/new_test"
    batch_size = 64
    sub_batch = None
    test_data_size = 32
    preprocessed = True
    interval_saves = 200
    load_weight = Saver.RECENT_WIGHTS
    start_epoch = 7001

    cropped = (100, 100, 1)
    lr = {0: 0.00001}

    t = Trainer(data_path, model_number, save_location, batch_size, use_pre_processed_data=preprocessed, max_seq_len=40, test_step=500, test_data_size=test_data_size, load=load_weight, lr=lr, cropped=cropped, interval_saves=interval_saves, sub_batch=sub_batch)

    t.train(100000, start_epoch=start_epoch)
    print("done")


if __name__ == '__main__':
    main_train()





