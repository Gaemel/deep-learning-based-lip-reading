from tensorflow.keras.layers import Input, Embedding, MultiHeadAttention, Layer, LayerNormalization, Dense, Conv1D, Reshape, MaxPooling3D, Flatten, BatchNormalization, Conv3D, Conv2D, MaxPooling2D, GlobalMaxPool2D
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.metrics import Mean, CategoricalCrossentropy
import tensorflow as tf
from residual_conv import MyResidualIdentityConv2D, MyResidualConv2D
from tensorflow.keras.callbacks import Callback
import numpy as np
from vision_nets import get_vision_net
from my_trig_pos_embd import TrigPosEmbedding


from MyTransformer_v2 import PositionalEmbedding, TransformerEncoder, TransformerDecoder, TransformerOutputEncoding, TransformerDecoderMaskless




class TransformerBaseMean(Model):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def decode(self, enc_out, target):
        a = self.dec_enc(target)
        a = self.dec_pos_emb(a)
        for i in range(self.num_layers_dec):
            a = getattr(self, f"dec_layer_{i}")(enc_out, a)
        return a

    def call(self, inputs):
        # print(inputs)
        source = inputs["source"]
        target = inputs["target"]
        # print("source", source)
        # print("target", target)

        # a = self.enc_emb(source)
        # print("a_0", a)
        # a = self.enc_pos_emb(a)
        # print("a_1", a)
        a = self.encoder(source)
        # print("a", a)
        a = self.decode(a, target)

        return self.classifier(a)

    def train_step(self, batch):
        """Processes one batch inside model.fit()."""
        batch = batch[0]
        source = batch["source"]
        target = batch["target"]

        dec_input = target[:, :-1]
        dec_target = target[:, 1:]

        with tf.GradientTape() as tape:
            preds = self({"source": source, "target": dec_input})
            # print("dec_target", dec_target)
            one_hot = tf.one_hot(dec_target, depth=self.num_classes)
            mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
            loss = self.compiled_loss(one_hot, preds, sample_weight=mask)

        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))
        self.loss_metric.update_state(loss)

        return {"loss": self.loss_metric.result()}

    def test_step(self, batch):
        batch = batch[0]
        # print(batch)
        source = batch["source"]
        target = batch["target"]
        dec_input = target[:, :-1]
        dec_target = target[:, 1:]
        # print("dec_input", dec_input)
        # print("dec_target", dec_target)
        # print("source", source)
        preds = self({"source": source, "target": dec_input})
        one_hot = tf.one_hot(dec_target, depth=self.num_classes)
        mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
        loss = self.compiled_loss(one_hot, preds, sample_weight=mask)
        self.loss_metric.update_state(loss)
        return {"loss": self.loss_metric.result()}

    def calc_cer(self, source, target, target_start_token_idx):
        bs = tf.shape(source)[0].numpy()
        # print("bs", bs)
        # print("source", source)
        enc = self.encoder(source)
        # print("enc_shape", enc.shape)
        dec_input = tf.ones((bs, 1), dtype=tf.int32) * target_start_token_idx

        # dim: bs * 1  --> ["<", "<", ... "<"]
        hit_sum = 0
        dec_logits = []
        for i in range(self.seq_len - 1):
            current_target = target[:, i + 1]

            dec_out = self.decode(enc, dec_input)
            logits = self.classifier(dec_out)
            logits = tf.argmax(logits, axis=-1, output_type=tf.int32)
            last_logit = tf.expand_dims(logits[:, -1], axis=-1)
            hits = np.count_nonzero(current_target == last_logit.numpy()[:, 0])

            hit_sum += (hits / self.seq_len)

            dec_logits.append(current_target)
            dec_input = tf.concat([dec_input, np.expand_dims(current_target, axis=1)], axis=-1)

        return hit_sum / bs

    def generate(self, source, target_start_token_idx):
        """Performs inference over one batch of inputs using greedy decoding."""
        bs = tf.shape(source)[0]
        # print("bs", bs)
        # print("source", source)
        enc = self.encoder(source)
        # print("enc_shape", enc.shape)
        dec_input = tf.ones((bs, 1), dtype=tf.int32) * target_start_token_idx

        # dim: bs * 1  --> ["<", "<", ... "<"]

        dec_logits = []
        for i in range(self.seq_len - 1):
            a = dec_input.numpy()
            dec_out = self.decode(enc, dec_input)
            logits = self.classifier(dec_out)
            logits = tf.argmax(logits, axis=-1, output_type=tf.int32)
            last_logit = tf.expand_dims(logits[:, -1], axis=-1)
            dec_logits.append(last_logit)
            dec_input = tf.concat([dec_input, last_logit], axis=-1)

        return dec_input

    @property
    def metrics(self):
        return [self.loss_metric]


class TransformerBaseMeanMaskLess(TransformerBaseMean):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def train_step(self, batch):
        """Processes one batch inside model.fit()."""
        batch = batch[0]
        source = batch["source"]
        target = batch["target"]

        ############# need for Mask less
        split_index = np.random.randint(2, target.shape[1]+1)
        target = target[:, :split_index]
        #############

        dec_input = target[:, :-1]
        dec_target = target[:, 1:]

        with tf.GradientTape() as tape:
            preds = self({"source": source, "target": dec_input})
            one_hot = tf.one_hot(dec_target, depth=self.num_classes)
            mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
            loss = self.compiled_loss(one_hot, preds, sample_weight=mask)

        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))
        self.loss_metric.update_state(loss)

        return {"loss": self.loss_metric.result()}

    def test_step(self, batch):
        batch = batch[0]
        # print(batch)
        source = batch["source"]
        target = batch["target"]

        ############# need for Mask less
        split_index = np.random.randint(2, target.shape[1] + 1)
        target = target[:, :split_index]
        #############

        dec_input = target[:, :-1]
        dec_target = target[:, 1:]
        # print("dec_input", dec_input)
        # print("dec_target", dec_target)
        # print("source", source)
        preds = self({"source": source, "target": dec_input})
        one_hot = tf.one_hot(dec_target, depth=self.num_classes)
        mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
        loss = self.compiled_loss(one_hot, preds, sample_weight=mask)
        self.loss_metric.update_state(loss)
        return {"loss": self.loss_metric.result()}


class TransformerBaseCrosent(TransformerBaseMean):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)


    def train_step(self, batch):
        """Processes one batch inside model.fit()."""
        batch = batch[0]
        source = batch["source"]
        target = batch["target"]
        dec_input = target[:, :-1]
        dec_target = target[:, 1:]

        with tf.GradientTape() as tape:
            preds = self({"source": source, "target": dec_input})
            one_hot = tf.one_hot(dec_target, depth=self.num_classes)
            mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
            loss = self.compiled_loss(one_hot, preds, sample_weight=mask)

        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))
        self.loss_metric.update_state(one_hot, preds, sample_weight=mask)

        return {"loss": self.loss_metric.result()}

    def test_step(self, batch):
        batch = batch[0]
        # print(batch)
        source = batch["source"]
        target = batch["target"]

        ############# need for Mask less
        split_index = np.random.randint(2, target.shape[1] + 1)
        target = target[:, :split_index]
        #############

        dec_input = target[:, :-1]
        dec_target = target[:, 1:]
        # print("dec_input", dec_input)
        # print("dec_target", dec_target)
        # print("source", source)
        preds = self({"source": source, "target": dec_input})
        one_hot = tf.one_hot(dec_target, depth=self.num_classes)
        mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
        loss = self.compiled_loss(one_hot, preds, sample_weight=mask)
        self.loss_metric.update_state(one_hot, preds, sample_weight=mask)
        return {"loss": self.loss_metric.result()}

class TransformerBaseCrosentMaskLess(TransformerBaseMean):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)


    def train_step(self, batch):
        """Processes one batch inside model.fit()."""
        batch = batch[0]
        source = batch["source"]
        target = batch["target"]

        ############# need for Mask less
        split_index = np.random.randint(2, target.shape[1] + 1)
        target = target[:, :split_index]
        #############

        dec_input = target[:, :-1]
        dec_target = target[:, 1:]

        with tf.GradientTape() as tape:
            preds = self({"source": source, "target": dec_input})
            one_hot = tf.one_hot(dec_target, depth=self.num_classes)
            mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
            loss = self.compiled_loss(one_hot, preds, sample_weight=mask)

        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))
        self.loss_metric.update_state(one_hot, preds, sample_weight=mask)

        return {"loss": self.loss_metric.result()}

    def test_step(self, batch):
        batch = batch[0]
        # print(batch)
        source = batch["source"]
        target = batch["target"]
        dec_input = target[:, :-1]
        dec_target = target[:, 1:]
        # print("dec_input", dec_input)
        # print("dec_target", dec_target)
        # print("source", source)
        preds = self({"source": source, "target": dec_input})
        one_hot = tf.one_hot(dec_target, depth=self.num_classes)
        mask = tf.math.logical_not(tf.math.equal(dec_target, 0))
        loss = self.compiled_loss(one_hot, preds, sample_weight=mask)
        self.loss_metric.update_state(one_hot, preds, sample_weight=mask)
        return {"loss": self.loss_metric.result()}

####################

class TransformerHeadlessEmbPosMean(TransformerBaseMean):

    def __init__(self, vision_net_id,
                 model_dim=512,
                 key_dim=64,
                 num_heads=1,
                 feed_forward_dim=64,
                 seq_len=125,
                 num_layers_enc=4,
                 num_layers_dec=1,
                 num_classes=10,
                 **kwargs):

        super().__init__(**kwargs)

        # with tf.device("/gpu:0"):
        self.vision_net = get_vision_net(vision_net_id, seq_len)
        # with tf.device("/gpu:1"):

        self.loss_metric = Mean(name="loss")
        self.num_layers_enc = num_layers_enc
        self.num_layers_dec = num_layers_dec
        self.seq_len = seq_len
        self.num_classes = num_classes

        # temp = np.product(self.vision_net.layers[-1].output_shape[2:])

        # self.enc_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingEncoder")
        self.enc_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingEncoder")
        # encoder

        self.encoder = Sequential(name="Encoders")
        self.encoder.add(self.vision_net)

        # self.encoder.add(self.enc_emb)
        self.encoder.add(self.enc_pos_emb)

        for i in range(self.num_layers_enc):
            self.encoder.add(TransformerEncoder(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim, num_heads=num_heads, name="Encoder_" + str(i)))

        # decoder
        self.dec_enc = TransformerOutputEncoding(seq_len=seq_len, model_dim=model_dim, name="InputEmbeddingDecoder")
        # self.dec_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingDecoder")
        self.dec_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingDecoder")

        for i in range(num_layers_dec):
            setattr(self, f"dec_layer_{i}",
                    TransformerDecoder(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim, num_heads=num_heads, name="Decoder_" + str(i)))

        self.classifier = Dense(num_classes, activation="softmax")


class TransformerHeadlessEmbSinPosMean(TransformerBaseMean):

    def __init__(self, vision_net_id,
                 model_dim=512,
                 key_dim=64,
                 num_heads=1,
                 feed_forward_dim=64,
                 seq_len=125,
                 num_layers_enc=4,
                 num_layers_dec=1,
                 num_classes=10,
                 **kwargs):

        super().__init__(**kwargs)

        # with tf.device("/gpu:0"):
        self.vision_net = get_vision_net(vision_net_id, seq_len)
        # with tf.device("/gpu:1"):

        self.loss_metric = Mean(name="loss")
        self.num_layers_enc = num_layers_enc
        self.num_layers_dec = num_layers_dec
        self.seq_len = seq_len
        self.num_classes = num_classes

        # temp = np.product(self.vision_net.layers[-1].output_shape[2:])

        self.enc_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingEncoder")
        # self.enc_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingEncoder")
        # encoder

        self.encoder = Sequential(name="Encoders")
        self.encoder.add(self.vision_net)

        # self.encoder.add(self.enc_emb)
        self.encoder.add(self.enc_pos_emb)

        for i in range(self.num_layers_enc):
            self.encoder.add(TransformerEncoder(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim, num_heads=num_heads, name="Encoder_" + str(i)))

        # decoder
        self.dec_enc = TransformerOutputEncoding(seq_len=seq_len, model_dim=model_dim, name="InputEmbeddingDecoder")
        self.dec_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingDecoder")
        # self.dec_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingDecoder")

        for i in range(num_layers_dec):
            setattr(self, f"dec_layer_{i}",
                    TransformerDecoder(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim, num_heads=num_heads, name="Decoder_" + str(i)))

        self.classifier = Dense(num_classes, activation="softmax")


class TransformerHeadlessEmbSinPosMean(TransformerBaseMean):

    def __init__(self, vision_net_id,
                 model_dim=512,
                 key_dim=64,
                 num_heads=1,
                 feed_forward_dim=64,
                 seq_len=125,
                 num_layers_enc=4,
                 num_layers_dec=1,
                 num_classes=10,
                 **kwargs):

        super().__init__(**kwargs)

        # with tf.device("/gpu:0"):
        self.vision_net = get_vision_net(vision_net_id, seq_len)
        # with tf.device("/gpu:1"):

        self.loss_metric = Mean(name="loss")
        self.num_layers_enc = num_layers_enc
        self.num_layers_dec = num_layers_dec
        self.seq_len = seq_len
        self.num_classes = num_classes

        # temp = np.product(self.vision_net.layers[-1].output_shape[2:])

        self.enc_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingEncoder")
        # self.enc_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingEncoder")
        # encoder

        self.encoder = Sequential(name="Encoders")
        self.encoder.add(self.vision_net)

        # self.encoder.add(self.enc_emb)
        self.encoder.add(self.enc_pos_emb)

        for i in range(self.num_layers_enc):
            self.encoder.add(TransformerEncoder(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim, num_heads=num_heads, name="Encoder_" + str(i)))

        # decoder
        self.dec_enc = TransformerOutputEncoding(seq_len=seq_len, model_dim=model_dim, name="InputEmbeddingDecoder")
        self.dec_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingDecoder")
        # self.dec_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingDecoder")

        for i in range(num_layers_dec):
            setattr(self, f"dec_layer_{i}",
                    TransformerDecoder(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim, num_heads=num_heads, name="Decoder_" + str(i)))

        self.classifier = Dense(num_classes, activation="softmax")


class MyBaseTransformer(TransformerBaseMean):

    def __init__(self,
                 model_dim=512,
                 key_dim=64,
                 num_heads=1,
                 feed_forward_dim=64,
                 seq_len=125,
                 num_layers_enc=4,
                 num_layers_dec=1,
                 num_classes=10,
                 **kwargs):

        super().__init__(**kwargs)

        # with tf.device("/gpu:1"):

        self.loss_metric = Mean(name="loss")
        self.num_layers_enc = num_layers_enc
        self.num_layers_dec = num_layers_dec
        self.seq_len = seq_len
        self.num_classes = num_classes

        # temp = np.product(self.vision_net.layers[-1].output_shape[2:])

        self.enc_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingEncoder")
        # self.enc_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingEncoder")
        # encoder

        self.encoder = Sequential(name="Encoders")
        self.encoder.add(self.enc_pos_emb)

        for i in range(self.num_layers_enc):
            self.encoder.add(TransformerEncoder(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim, num_heads=num_heads, name="Encoder_" + str(i)))

        # decoder
        self.dec_enc = TransformerOutputEncoding(seq_len=seq_len, model_dim=model_dim, name="InputEmbeddingDecoder")
        self.dec_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingDecoder")
        # self.dec_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingDecoder")

        # with tf.device("/gpu:0"):
        for i in range(num_layers_dec):
            setattr(self, f"dec_layer_{i}",
                    TransformerDecoder(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim, num_heads=num_heads, name="Decoder_" + str(i)))

        self.classifier = Dense(num_classes, activation="softmax")



class TransformerHeadlessEmbPosCrossent(TransformerBaseCrosent):

    def __init__(self, vision_net_id,
                 model_dim=512,
                 key_dim=64,
                 num_heads=1,
                 feed_forward_dim=64,
                 seq_len=125,
                 num_layers_enc=4,
                 num_layers_dec=1,
                 num_classes=10,
                 **kwargs):

        super().__init__(**kwargs)

        # with tf.device("/gpu:0"):
        self.vision_net = get_vision_net(vision_net_id, seq_len)
        # with tf.device("/gpu:1"):

        self.loss_metric = CategoricalCrossentropy(name="loss")
        self.num_layers_enc = num_layers_enc
        self.num_layers_dec = num_layers_dec
        self.seq_len = seq_len
        self.num_classes = num_classes

        # temp = np.product(self.vision_net.layers[-1].output_shape[2:])

        # self.enc_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingEncoder")
        self.enc_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingEncoder")
        # encoder

        self.encoder = Sequential(name="Encoders")
        self.encoder.add(self.vision_net)

        # self.encoder.add(self.enc_emb)
        self.encoder.add(self.enc_pos_emb)

        for i in range(self.num_layers_enc):
            self.encoder.add(TransformerEncoder(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim, num_heads=num_heads, name="Encoder_" + str(i)))

        # decoder
        self.dec_enc = TransformerOutputEncoding(seq_len=seq_len, model_dim=model_dim, name="InputEmbeddingDecoder")
        # self.dec_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingDecoder")
        self.dec_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingDecoder")

        for i in range(num_layers_dec):
            setattr(self, f"dec_layer_{i}",
                    TransformerDecoder(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim, num_heads=num_heads, name="Decoder_" + str(i)))

        self.classifier = Dense(num_classes, activation="softmax")


class TransformerHeadlessEmbSinPosCrossent(TransformerBaseCrosent):

    def __init__(self, vision_net_id,
                 model_dim=512,
                 key_dim=64,
                 num_heads=1,
                 feed_forward_dim=64,
                 seq_len=125,
                 num_layers_enc=4,
                 num_layers_dec=1,
                 num_classes=10,
                 **kwargs):

        super().__init__(**kwargs)

        # with tf.device("/gpu:0"):
        self.vision_net = get_vision_net(vision_net_id, seq_len)
        # with tf.device("/gpu:1"):

        self.loss_metric = CategoricalCrossentropy(name="loss")
        self.num_layers_enc = num_layers_enc
        self.num_layers_dec = num_layers_dec
        self.seq_len = seq_len
        self.num_classes = num_classes

        # temp = np.product(self.vision_net.layers[-1].output_shape[2:])

        self.enc_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingEncoder")
        # self.enc_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingEncoder")
        # encoder

        self.encoder = Sequential(name="Encoders")
        self.encoder.add(self.vision_net)

        # self.encoder.add(self.enc_emb)
        self.encoder.add(self.enc_pos_emb)

        for i in range(self.num_layers_enc):
            self.encoder.add(TransformerEncoder(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim, num_heads=num_heads, name="Encoder_" + str(i)))

        # decoder
        self.dec_enc = TransformerOutputEncoding(seq_len=seq_len, model_dim=model_dim, name="InputEmbeddingDecoder")
        self.dec_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingDecoder")
        # self.dec_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingDecoder")

        for i in range(num_layers_dec):
            setattr(self, f"dec_layer_{i}",
                    TransformerDecoder(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim, num_heads=num_heads, name="Decoder_" + str(i)))

        self.classifier = Dense(num_classes, activation="softmax")


############# MaskLess --->


class TransformerHeadlessEmbPosMeanMaskLess(TransformerBaseMeanMaskLess):

    def __init__(self, vision_net_id,
                 model_dim=512,
                 key_dim=64,
                 num_heads=1,
                 feed_forward_dim=64,
                 seq_len=125,
                 num_layers_enc=4,
                 num_layers_dec=1,
                 num_classes=10,
                 **kwargs):

        super().__init__(**kwargs)

        # with tf.device("/gpu:0"):
        self.vision_net = get_vision_net(vision_net_id, seq_len)
        # with tf.device("/gpu:1"):

        self.loss_metric = Mean(name="loss")
        self.num_layers_enc = num_layers_enc
        self.num_layers_dec = num_layers_dec
        self.seq_len = seq_len
        self.num_classes = num_classes

        # temp = np.product(self.vision_net.layers[-1].output_shape[2:])

        # self.enc_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingEncoder")
        self.enc_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingEncoder")
        # encoder

        self.encoder = Sequential(name="Encoders")
        self.encoder.add(self.vision_net)

        # self.encoder.add(self.enc_emb)
        self.encoder.add(self.enc_pos_emb)

        for i in range(self.num_layers_enc):
            self.encoder.add(TransformerEncoder(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim, num_heads=num_heads, name="Encoder_" + str(i)))

        # decoder
        self.dec_enc = TransformerOutputEncoding(seq_len=seq_len, model_dim=model_dim, name="InputEmbeddingDecoder")
        # self.dec_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingDecoder")
        self.dec_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingDecoder")

        for i in range(num_layers_dec):
            setattr(self, f"dec_layer_{i}",
                    TransformerDecoderMaskless(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim, num_heads=num_heads, name="Decoder_" + str(i)))

        self.classifier = Dense(num_classes, activation="softmax")


class TransformerHeadlessEmbSinPosMeanMaskLess(TransformerBaseMeanMaskLess):

    def __init__(self, vision_net_id,
                 model_dim=512,
                 key_dim=64,
                 num_heads=1,
                 feed_forward_dim=64,
                 seq_len=125,
                 num_layers_enc=4,
                 num_layers_dec=1,
                 num_classes=10,
                 **kwargs):

        super().__init__(**kwargs)

        # with tf.device("/gpu:0"):
        self.vision_net = get_vision_net(vision_net_id, seq_len)
        # with tf.device("/gpu:1"):

        self.loss_metric = Mean(name="loss")
        self.num_layers_enc = num_layers_enc
        self.num_layers_dec = num_layers_dec
        self.seq_len = seq_len
        self.num_classes = num_classes

        # temp = np.product(self.vision_net.layers[-1].output_shape[2:])

        self.enc_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingEncoder")
        # self.enc_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingEncoder")
        # encoder

        self.encoder = Sequential(name="Encoders")
        self.encoder.add(self.vision_net)

        # self.encoder.add(self.enc_emb)
        self.encoder.add(self.enc_pos_emb)

        for i in range(self.num_layers_enc):
            self.encoder.add(TransformerEncoder(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim, num_heads=num_heads, name="Encoder_" + str(i)))

        # decoder
        self.dec_enc = TransformerOutputEncoding(seq_len=seq_len, model_dim=model_dim, name="InputEmbeddingDecoder")
        self.dec_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingDecoder")
        # self.dec_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingDecoder")

        for i in range(num_layers_dec):
            setattr(self, f"dec_layer_{i}",
                    TransformerDecoderMaskless(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim, num_heads=num_heads, name="Decoder_" + str(i)))

        self.classifier = Dense(num_classes, activation="softmax")


class TransformerHeadlessEmbPosCrossentMaskLess(TransformerBaseCrosentMaskLess):

    def __init__(self, vision_net_id,
                 model_dim=512,
                 key_dim=64,
                 num_heads=1,
                 feed_forward_dim=64,
                 seq_len=125,
                 num_layers_enc=4,
                 num_layers_dec=1,
                 num_classes=10,
                 **kwargs):

        super().__init__(**kwargs)

        # with tf.device("/gpu:0"):
        self.vision_net = get_vision_net(vision_net_id, seq_len)
        # with tf.device("/gpu:1"):

        self.loss_metric = CategoricalCrossentropy(name="loss")
        self.num_layers_enc = num_layers_enc
        self.num_layers_dec = num_layers_dec
        self.seq_len = seq_len
        self.num_classes = num_classes

        # temp = np.product(self.vision_net.layers[-1].output_shape[2:])

        # self.enc_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingEncoder")
        self.enc_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingEncoder")
        # encoder

        self.encoder = Sequential(name="Encoders")
        self.encoder.add(self.vision_net)

        # self.encoder.add(self.enc_emb)
        self.encoder.add(self.enc_pos_emb)

        for i in range(self.num_layers_enc):
            self.encoder.add(TransformerEncoder(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim, num_heads=num_heads, name="Encoder_" + str(i)))

        # decoder
        self.dec_enc = TransformerOutputEncoding(seq_len=seq_len, model_dim=model_dim, name="InputEmbeddingDecoder")
        # self.dec_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingDecoder")
        self.dec_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingDecoder")

        for i in range(num_layers_dec):
            setattr(self, f"dec_layer_{i}",
                    TransformerDecoderMaskless(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim, num_heads=num_heads, name="Decoder_" + str(i)))

        self.classifier = Dense(num_classes, activation="softmax")


class TransformerHeadlessEmbSinPosCrossentMaskLess(TransformerBaseCrosentMaskLess):

    def __init__(self, vision_net_id,
                 model_dim=512,
                 key_dim=64,
                 num_heads=1,
                 feed_forward_dim=64,
                 seq_len=125,
                 num_layers_enc=4,
                 num_layers_dec=1,
                 num_classes=10,
                 **kwargs):

        super().__init__(**kwargs)

        # with tf.device("/gpu:0"):
        self.vision_net = get_vision_net(vision_net_id, seq_len)
        # with tf.device("/gpu:1"):

        self.loss_metric = CategoricalCrossentropy(name="loss")
        self.num_layers_enc = num_layers_enc
        self.num_layers_dec = num_layers_dec
        self.seq_len = seq_len
        self.num_classes = num_classes

        # temp = np.product(self.vision_net.layers[-1].output_shape[2:])

        self.enc_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingEncoder")
        # self.enc_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingEncoder")
        # encoder

        self.encoder = Sequential(name="Encoders")
        self.encoder.add(self.vision_net)

        # self.encoder.add(self.enc_emb)
        self.encoder.add(self.enc_pos_emb)

        for i in range(self.num_layers_enc):
            self.encoder.add(TransformerEncoder(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim, num_heads=num_heads, name="Encoder_" + str(i)))

        # decoder
        self.dec_enc = TransformerOutputEncoding(seq_len=seq_len, model_dim=model_dim, name="InputEmbeddingDecoder")
        self.dec_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingDecoder")
        # self.dec_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingDecoder")

        for i in range(num_layers_dec):
            setattr(self, f"dec_layer_{i}",
                    TransformerDecoderMaskless(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim, num_heads=num_heads, name="Decoder_" + str(i)))

        self.classifier = Dense(num_classes, activation="softmax")
