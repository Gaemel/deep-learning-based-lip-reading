from tensorflow.keras import Model
from tensorflow.keras.models import model_from_json
from tensorflow.keras.layers import Conv2D, BatchNormalization, Activation, ZeroPadding2D
from tensorflow.keras import optimizers, losses, metrics

from math import ceil

import cv2
import numpy as np
import matplotlib.pyplot as plt

from copy import copy



def load_model(model_path, weights_path):
    json_file = open(model_path, "r")
    model_json = json_file.read()
    json_file.close()
    model = model_from_json(model_json)
    model.load_weights(weights_path)
    model.compile(optimizer=optimizers.Adam(learning_rate=0.00001), loss=losses.mean_squared_error,
                  metrics=[metrics.MeanIoU(2)])
    model.summary()
    return model


def visualize(model, img):
    pass

def load_img(img_path, gray=True):

    if gray:
        img = cv2.cvtColor(cv2.imread(img_path), cv2.COLOR_BGR2GRAY) / 255
    else:
        img = cv2.imread(img_path) / 255

    img_data = np.expand_dims(copy(img), axis=2)
    img_data = np.expand_dims(img_data, axis=0)

    return img_data, img


def show_features(feature, layer):
    columns = rows = ceil(np.sqrt(feature.shape[-1]))

    fig = plt.figure(figsize=(10, 10), num=layer.name)

    for i in range(1, columns * rows + 1):

        if i > feature.shape[-1]:
            break

        fig = plt.subplot(rows, columns, i)
        fig.set_xticks([])
        fig.set_yticks([])
        # print(feature[0, :, :, i - 1])
        plt.imshow(feature[0, :, :, i - 1], cmap="gray")
    plt.show()


def create_output_rich_model(model, target_layers):
    outputs = [model.layers[i].output for i in range(len(model.layers)) if type(model.layers[i]) in target_layers]
    return Model(inputs=model.inputs, outputs=outputs), outputs

def main():
    version = 37

    model_path = """/home/ubuntu/PycharmProjects/lip-reading/classification/model_""" + str(version) + """.json"""
    weights_path = """/home/ubuntu/PycharmProjects/lip-reading/classification/weights_""" + str(version) + """.h5"""
    img_path = """/media/ubuntu/data/lip_reading_data/test_data_raw/ykm-jlSkCL4_S1_0.png"""

    target_layers = [Conv2D, BatchNormalization, Activation, ZeroPadding2D]

    model = load_model(model_path, weights_path)
    model, layer_list = create_output_rich_model(model, target_layers)

    img_data, img = load_img(img_path)

    cv2.imshow("input", img)
    cv2.waitKey(100)

    feature_output = model.predict(img_data)

    for i in range(len(feature_output)):
        show_features(feature_output[i], layer_list[i])

if __name__ == '__main__':
    main()