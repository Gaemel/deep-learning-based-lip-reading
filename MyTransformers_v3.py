from builtins import int

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.metrics import Mean, CategoricalCrossentropy
from tensorflow.keras.optimizers import Adam
## from # from https://keras.io/examples/nlp/text_classification_with_switch_transformer/
from MyTransformer_v2 import TransformerSelfAttention, TransformerOutputEncoding, DecoderEncoderAttention, TransformerEncoder
from MyTransformer_v4 import TransformerBaseMean
from my_trig_pos_embd import TrigPosEmbedding
from vision_nets import get_vision_net

import numpy as np

def load_balanced_loss(router_probs, expert_mask):
    # router_probs [tokens_per_batch, num_experts] is the probability assigned for
    # each expert per token. expert_mask [tokens_per_batch, num_experts] contains
    # the expert with the highest router probability in one−hot format.

    num_experts = tf.shape(expert_mask)[-1]
    # Get the fraction of tokens routed to each expert.
    # density is a vector of length num experts that sums to 1.
    density = tf.reduce_mean(expert_mask, axis=0)
    # Get fraction of probability mass assigned to each expert from the router
    # across all tokens. density_proxy is a vector of length num experts that sums to 1.
    density_proxy = tf.reduce_mean(router_probs, axis=0)
    # Want both vectors to have uniform allocation (1/num experts) across all
    # num_expert elements. The two vectors will be pushed towards uniform allocation
    # when the dot product is minimized.
    loss = tf.reduce_mean(density_proxy * density) * tf.cast(
        (num_experts ** 2), tf.dtypes.float32
    )
    return loss

## from # from https://keras.io/examples/nlp/text_classification_with_switch_transformer/
class Router(layers.Layer):
    def __init__(self, num_experts, expert_capacity):
        self.num_experts = num_experts
        self.route = layers.Dense(units=num_experts)
        self.expert_capacity = expert_capacity
        super(Router, self).__init__()

    def call(self, inputs, training=False):
        # inputs shape: [tokens_per_batch, embed_dim]
        # router_logits shape: [tokens_per_batch, num_experts]
        router_logits = self.route(inputs)

        if training:
            # Add noise for exploration across experts.
            router_logits += tf.random.uniform(
                shape=router_logits.shape, minval=0.9, maxval=1.1
            )
        # Probabilities for each token of what expert it should be sent to.
        router_probs = keras.activations.softmax(router_logits, axis=-1)
        # Get the top−1 expert for each token. expert_gate is the top−1 probability
        # from the router for each token. expert_index is what expert each token
        # is going to be routed to.
        expert_gate, expert_index = tf.math.top_k(router_probs, k=1)
        # expert_mask shape: [tokens_per_batch, num_experts]
        expert_mask = tf.one_hot(expert_index, depth=self.num_experts)
        # Compute load balancing loss.
        aux_loss = load_balanced_loss(router_probs, expert_mask)
        self.add_loss(aux_loss)
        # Experts have a fixed capacity, ensure we do not exceed it. Construct
        # the batch indices, to each expert, with position in expert make sure that
        # not more that expert capacity examples can be routed to each expert.
        position_in_expert = tf.cast(
            tf.math.cumsum(expert_mask, axis=0) * expert_mask, tf.dtypes.int32
        )
        # Keep only tokens that fit within expert capacity.
        expert_mask *= tf.cast(
            tf.math.less(
                tf.cast(position_in_expert, tf.dtypes.int32), self.expert_capacity
            ),
            tf.dtypes.float32,
        )
        expert_mask_flat = tf.reduce_sum(expert_mask, axis=-1)
        # Mask out the experts that have overflowed the expert capacity.
        expert_gate *= expert_mask_flat
        # Combine expert outputs and scaling with router probability.
        # combine_tensor shape: [tokens_per_batch, num_experts, expert_capacity]
        combined_tensor = tf.expand_dims(
            expert_gate
            * expert_mask_flat
            * tf.squeeze(tf.one_hot(expert_index, depth=self.num_experts), 1),
            -1,
        ) * tf.squeeze(tf.one_hot(position_in_expert, depth=self.expert_capacity), 1)
        # Create binary dispatch_tensor [tokens_per_batch, num_experts, expert_capacity]
        # that is 1 if the token gets routed to the corresponding expert.
        dispatch_tensor = tf.cast(combined_tensor, tf.dtypes.float32)

        return dispatch_tensor, combined_tensor


def create_feedforward_network(dense_dim, embed_dim, num_dense):

    seq = Sequential()
    for _ in range(num_dense):
        seq.add(layers.Dense(dense_dim, activation=tf.nn.gelu))
    seq.add(layers.Dense(embed_dim, activation="linear"))

    return seq

class MySwitch(layers.Layer):
    def __init__(self, num_experts, embed_dim, dense_dim, num_tokens_per_batch, num_dense=1):
        self.num_experts = num_experts
        self.embed_dim = embed_dim
        self.experts = [
            create_feedforward_network(dense_dim, embed_dim, num_dense) for _ in range(num_experts)
        ]
        self.num_tokens_per_batch = num_tokens_per_batch
        self.expert_capacity = num_tokens_per_batch // self.num_experts
        self.router = Router(self.num_experts, self.expert_capacity)
        super(MySwitch, self).__init__()

    def call(self, inputs):

        batch_size = inputs.shape[0]
        num_tokens_per_example = inputs.shape[1]

        # inputs shape: [num_tokens_per_batch, embed_dim]
        inputs = tf.reshape(inputs, [num_tokens_per_example*batch_size, self.embed_dim])
        # dispatch_tensor shape: [expert_capacity, num_experts, tokens_per_batch]
        # combine_tensor shape: [tokens_per_batch, num_experts, expert_capacity]
        dispatch_tensor, combine_tensor = self.router(inputs)
        # expert_inputs shape: [num_experts, expert_capacity, embed_dim]
        expert_inputs = tf.einsum("ab,acd->cdb", inputs, dispatch_tensor)
        expert_inputs = tf.reshape(
            expert_inputs, [self.num_experts, self.expert_capacity, self.embed_dim]
        )
        # Dispatch to experts
        expert_input_list = tf.unstack(expert_inputs, axis=0)
        expert_output_list = [
            self.experts[idx](expert_input)
            for idx, expert_input in enumerate(expert_input_list)
        ]
        # expert_outputs shape: [expert_capacity, num_experts, embed_dim]
        expert_outputs = tf.stack(expert_output_list, axis=1)
        # expert_outputs_combined shape: [tokens_per_batch, embed_dim]
        expert_outputs_combined = tf.einsum(
            "abc,xba->xc", expert_outputs, combine_tensor
        )
        # output shape: [batch_size, num_tokens_per_example, embed_dim]
        outputs = tf.reshape(
            expert_outputs_combined,
            [batch_size, num_tokens_per_example, self.embed_dim],
        )
        return outputs


class SwitchTransformerEncoder(layers.Layer):

    def __init__(self, num_tokens_per_batch, key_dim=64, model_dim=512, dense_dim=512, num_heads=2, num_experts=2, dropout_rate=0.1, **kwargs):
        super().__init__(**kwargs)
        self.self_att = TransformerSelfAttention(key_dim, num_heads, dropout_rate, use_mask=False)
        self.ff = MySwitch(num_experts=num_experts, embed_dim=dense_dim, dense_dim=model_dim, num_tokens_per_batch=num_tokens_per_batch, num_dense=1)

    def call(self, inputs):
        # print(inputs.shape)
        a = self.self_att(inputs)
        # print(a.shape)
        a = self.ff(a)
        return a


class SwitchTransformerDecoder(layers.Layer):

    def __init__(self, num_tokens_per_batch, key_dim=64, model_dim=512, dense_dim=512, num_heads=2, num_experts=2, dropout_rate=0.1, **kwargs):
        super().__init__(**kwargs)

        self.self_att = TransformerSelfAttention(key_dim, num_heads, dropout_rate, use_mask=True)
        self.enc_att = DecoderEncoderAttention(key_dim, num_heads, dropout_rate)
        self.ff = MySwitch(num_experts=num_experts, embed_dim=dense_dim, dense_dim=model_dim, num_tokens_per_batch=num_tokens_per_batch, num_dense=1)

    def call(self, enc_out, target):
        a = self.self_att(target)
        a = self.enc_att(enc_out, a)
        return self.ff(a)


class PaddingTransformerOutputEncoding(layers.Layer):

    def __init__(self, seq_len, model_dim, **kwargs):
        super().__init__(**kwargs)

        self.seq_len = seq_len
        self.emb = layers.Embedding(seq_len, model_dim)
        # self.pos_emb = Embedding()

    def call(self, inputs):
        in_seq = inputs.shape[1]
        bs = inputs.shape[0]

        if in_seq < self.seq_len:
            zeros = tf.zeros((inputs.shape[0], self.seq_len - in_seq),  dtype=tf.float32)
            # print("inputs", inputs.shape)
            # print("inputs", inputs)
            # print("zeros", zeros.shape)
            # print("zeros", zeros)
            temp = tf.constant([inputs, zeros])
            return self.emb(temp)
        else:
            return self.emb(inputs)


class MySwitchTransformer(TransformerBaseMean):

    def __init__(self, vision_net_id,
                 num_tokens_per_batch,
                 model_dim=512,
                 key_dim=64,
                 num_heads=1,
                 feed_forward_dim=64,
                 seq_len=125,
                 num_layers_enc=4,
                 num_layers_dec=1,
                 num_classes=10,
                 **kwargs):

        super().__init__(**kwargs)
        self.num_tokens_per_batch = num_tokens_per_batch
        # with tf.device("/gpu:1"):

        self.loss_metric = Mean(name="loss")
        self.num_layers_enc = num_layers_enc
        self.num_layers_dec = num_layers_dec
        self.seq_len = seq_len
        self.num_classes = num_classes

        self.enc_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingEncoder")
        # self.enc_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingEncoder")
        # encoder

        self.encoder = Sequential(name="Encoders")
        # self.encoder_input = layers.Input((seq_len, 512))
        # self.encoder.add(self.encoder_input)
        self.encoder.add(self.enc_pos_emb)

        for i in range(self.num_layers_enc):
            self.encoder.add(SwitchTransformerEncoder(num_tokens_per_batch=num_tokens_per_batch, key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim, num_heads=num_heads, name="Encoder_" + str(i)))

        # with tf.device("/gpu:0"):
        self.dec_enc = TransformerOutputEncoding(seq_len=seq_len, model_dim=model_dim, name="InputEmbeddingDecoder")
        self.dec_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingDecoder")
        # self.dec_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingDecoder")

        for i in range(num_layers_dec):
            setattr(self, f"dec_layer_{i}",
                    SwitchTransformerDecoder(num_tokens_per_batch=num_tokens_per_batch, key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim,
                                       num_heads=num_heads, name="Decoder_" + str(i)))

        self.classifier = layers.Dense(num_classes, activation="softmax")



class MySwitchTransformerTail(TransformerBaseMean):

    def __init__(self, vision_net_id,
                 num_tokens_per_batch,
                 model_dim=512,
                 key_dim=64,
                 num_heads=1,
                 feed_forward_dim=64,
                 seq_len=125,
                 num_layers_enc=4,
                 num_layers_dec=1,
                 num_classes=10,
                 **kwargs):

        super().__init__(**kwargs)
        self.num_tokens_per_batch = num_tokens_per_batch
        # with tf.device("/gpu:1"):

        self.loss_metric = Mean(name="loss")
        self.num_layers_enc = num_layers_enc
        self.num_layers_dec = num_layers_dec
        self.seq_len = seq_len
        self.num_classes = num_classes

        self.enc_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingEncoder")
        # self.enc_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingEncoder")
        # encoder

        self.encoder = Sequential(name="Encoders")
        # self.encoder_input = layers.Input((seq_len, 512))
        # self.encoder.add(self.encoder_input)
        self.encoder.add(self.enc_pos_emb)

        for i in range(self.num_layers_enc):
            self.encoder.add(TransformerEncoder(key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim,
                                                num_heads=num_heads, name="Encoder_" + str(i)))

        # with tf.device("/gpu:0"):
        self.dec_enc = TransformerOutputEncoding(seq_len=seq_len, model_dim=model_dim, name="InputEmbeddingDecoder")
        self.dec_pos_emb = TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD, name="PositionalEmbeddingDecoder")
        # self.dec_pos_emb = PositionalEmbedding(seq_len, model_dim, name="PositionalEmbeddingDecoder")

        for i in range(num_layers_dec):
            setattr(self, f"dec_layer_{i}",
                    SwitchTransformerDecoder(num_tokens_per_batch=num_tokens_per_batch, key_dim=key_dim, model_dim=model_dim, dense_dim=feed_forward_dim,
                                       num_heads=num_heads, name="Decoder_" + str(i)))

        self.classifier = layers.Dense(num_classes, activation="softmax")

def test_0():
    bs = 2
    seq_len = 10

    in_ing = np.zeros((bs, seq_len, 100, 100, 1))
    in_targets = np.zeros((bs, seq_len), dtype=int)

    ds = {"source": in_ing, "target": in_targets}
    print()
    model_dim = 512
    key_dim = 64
    dense_dim = 512
    num_heads = 4

    num_enc = 4
    num_dec = 4

    vision_net = 203

    vocab_size = 32

    transformer = MySwitchTransformer(num_tokens_per_batch=bs * seq_len,
                                      vision_net_id=vision_net,
                                      model_dim=model_dim,
                                      key_dim=key_dim,
                                      num_heads=num_heads,
                                      feed_forward_dim=dense_dim,
                                      seq_len=seq_len,
                                      num_layers_enc=num_enc,
                                      num_layers_dec=num_dec,
                                      num_classes=vocab_size
                                      )

    transformer.compile(optimizer=Adam(learning_rate=0.0001), loss="categorical_crossentropy")
    transformer.fit(ds, batch_size=bs)

def test_1():
    bs = 10
    seq = 3
    num_experts = 2
    embed_dim = 20
    dense_dim = 100
    num_tokens_per_batch = bs*seq
    num_dense = 1
    s = MySwitch(num_experts=num_experts, embed_dim=embed_dim, dense_dim=dense_dim, num_tokens_per_batch=num_tokens_per_batch, num_dense=num_dense)

    data = np.zeros((bs, seq, embed_dim))

    res = s(data)


def test_2():
    print(tf.version)

    bs = 10
    seq = 6
    model_dim = 20

    # ptoe = PaddingTransformerOutputEncoding(seq, model_dim)
    ones_a = np.ones((bs, seq), dtype=np.float32)
    ones_b = np.ones((bs, seq-2), dtype=np.float32)
    # res_a = ptoe(ones_a).numpy()
    # res_b = ptoe(ones_b).numpy()

    # print(res_a.shape)
    # print(res_b.shape)

    model = Sequential()
    model.add(layers.Input((seq, )))
    model.add(PaddingTransformerOutputEncoding(seq, model_dim))
    model.compile(loss="mse")

    res_model_a = model.predict(ones_a, batch_size=bs)
    res_model_b = model.predict(ones_b, batch_size=bs)
    print(res_model_a)
    print(res_model_b)

if __name__ == '__main__':
    test_0()
