from ModelSaver import Saver
from MyTransformers_V5 import MyExplicitSwitchTransformerWorkaroundTail, MyExplicitSwitchTransformerWorkaround
from TransformerModels import get_transformer_model
from lip_data_reader import VectorizeChar, LipDataReader
from vision_net_extractor import extract_trained_vision_net, extend_srq_len
import os
import numpy as np

model_number_dict = {"TE4_TD4": 5001,
                     }
VISION_NET_WEIGHTS = "vision_net_weights.h5"
VISION_NET_ID = 203
VISION_NET_PRE_TRAIN_MODEL_ID = 2015
SEQ_LEN = 75
TOKEN_VECTOR_SIZE = 512
VOCAB_SIZE = len(VectorizeChar.get_vocabulary())
MODEL_TRANSLATION_DICT = {"TE4-TD4": 5001,
                          "TE6-TD6": 5000,
                          "SE6-SD6": 3000,
                          "SE4-SD4": 3002,
                          "TE6-SD6": 3000,
                          "TE4-SD4": 3003,
                          "EE6-ED6": 4000,
                          "TE6-ED6": 4001,
                          "EE4-ED4": 4002,
                          "TE4-ED4": 4003
                          }

def get_model_number_by_name(model_name):
    return MODEL_TRANSLATION_DICT[model_name]

def load_model(model_number, model_weights):
    model = get_transformer_model(model_number, SEQ_LEN, (TOKEN_VECTOR_SIZE, ), VOCAB_SIZE)
    model.load_weights(model_weights)
    return model

def load_video_net(weights_location):
    vis_net = extract_trained_vision_net(model_number=VISION_NET_PRE_TRAIN_MODEL_ID, target_weights=weights_location, save_location=None, in_shape=(100, 100, 1))
    vis_net = extend_srq_len(vis_net, VISION_NET_ID, SEQ_LEN)
    return vis_net

def pack(data, include_indexes=False):
    if isinstance(data, dict):
        temp = dict()
        for key in data.keys():
            temp[key] = pack(data[key])
        return temp
    else:
        if include_indexes:
            return {"source": data[0], "target": data[1], "index": data[2]}
        else:
            return {"source": data[0], "target": data[1]}

def load_data_sets(data_set_locations=dict, test_data=True, batch_size=128):
    data_reader = LipDataReader(data_set_locations, batch_size, pre_processed=True, max_input_len=SEQ_LEN,
                  max_text_len=SEQ_LEN, test_data_size=batch_size)

    if test_data:
        data = data_reader.create_test_dataset(as_dict=True)
    else:
        data = data_reader.create_train_dataset()

    return pack(data)

def apply_vision_net(vision_net, data):
    for key in data.keys():
        data[key]["source"] = vision_net.predict(data[key]["source"])
    return data


def calc_cer(model, data, split=None):
    res_dict = dict()
    all = 0
    for i, key in enumerate(data.keys()):
        if isinstance(model, MyExplicitSwitchTransformerWorkaroundTail) or isinstance(model, MyExplicitSwitchTransformerWorkaround):
            model.set_mode(i)

        temp = __calc_next_char_metric(model, data[key], split=split)
        temp = 1.0 - temp
        res_dict[key] = temp
        all += temp

    if isinstance(model, MyExplicitSwitchTransformerWorkaroundTail) or isinstance(model, MyExplicitSwitchTransformerWorkaround):
        model.set_mode(None)

    res_dict["all"] = all / (len(res_dict.keys()))
    return res_dict

def __calc_next_char_metric(model, data, split=None):
    if split is not None:

        temp = 0
        data_len = int(data["source"].shape[0] / split)

        for x in range(split):
            temp += model.calc_cer(data["source"][data_len * x:data_len * (x + 1) - 1], data["target"][data_len * x:data_len * (x + 1) - 1], VectorizeChar.START_TOKEN)

        return temp / split

    return model.calc_cer(data["source"], data["target"], VectorizeChar.START_TOKEN)




def eval_model():
    # the paths to the pre-processed data
    data_location = {"eng": """/media/ubuntu/data/combined/100/english_pre""", "ger": """/media/ubuntu/data/combined/100/german_pre"""}

    target_model = "TE6-ED6"
    # the transformer model to eval
    transformer_model_number = get_model_number_by_name(target_model)

    # vision_net_weights = """./weights/vision_net/vision_net_weights.h5"""
    transformer_model_weights = """/media/ubuntu/data/chain_saves/model_4001_new/model_4001_recent_weights.h5"""

    # vis_net = load_video_net(vision_net_weights)
    transformer_model = load_model(transformer_model_number, transformer_model_weights)

    data_sets = load_data_sets(data_location)
    # data_sets = apply_vision_net(vis_net, data_sets)
    # del vis_net

    nce = calc_cer(transformer_model, data_sets, 8)
    for key in nce.keys():
        print(key, nce[key])
    # print(data_sets)

if __name__ == '__main__':
    eval_model()