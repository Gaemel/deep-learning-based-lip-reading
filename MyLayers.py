from tensorflow.keras.layers import Layer, Embedding
import numpy as np
import tensorflow as tf
from my_trig_pos_embd import TrigPosEmbedding
from tensorflow import keras

class BorderDrop3D(Layer):

    def __init__(self, border, **kwargs):
        self.border = border
        super().__init__(**kwargs)

    def call(self, inputs):
        return inputs[:, :, self.border:-self.border, self.border:-self.border, :]
        # return inputs[self.border:-self.border, self.border:-self.border, :]

class PosEncoding(Layer):

    def __init__(self, seq_len, model_dim, **kwargs):
        super().__init__(**kwargs)
        self.seq_len = seq_len
        self.model_dim = model_dim
        self.pos_matrix = self._get_embed()

    def call(self, inputs):
        bs = inputs.shape[0]
        seq_len = inputs.shape[1]
        return inputs + self.get_embed(bs, seq_len)

    def get_embed(self, bs, seq_len):

        some_range = np.arange(0, self.model_dim, 1, dtype=np.float32)
        some_range = np.array(seq_len * [some_range])

        for x in range(seq_len):

            if x % 2 == 0:
                some_range[x] = np.sin(some_range[x] * (int(x / 2) + 1))
            else:
                some_range[x] = np.cos(some_range[x] * (int(x / 2) + 1))

        some_range = np.array(bs * [some_range])
        return tf.convert_to_tensor(some_range)

    def _get_embed(self):
        some_range = np.arange(0, self.model_dim, 1, dtype=np.float32)
        some_range = np.array((self.seq_len) * [some_range])

        for x in range(self.seq_len):

            if x % 2 == 0:
                some_range[x] = np.sin(some_range[x] * (int(x / 2) + 1))
            else:
                some_range[x] = np.cos(some_range[x] * (int(x / 2) + 1))

        # some_range = np.transpose(some_range)
        return some_range


def my_func(some_input):
    # some_input = np.transpose(some_input)
    for x in range(some_input.shape[0]):

        if x % 2 == 0:
            some_input[x] = tf.sin(some_input[x] * (int(x / 2) + 1))
        else:
            some_input[x] = tf.cos(some_input[x] * (int(x / 2) + 1))

    # some_input = np.transpose(some_input)
    return some_input

def some_test_1():
    seq = 75
    dim = 512

    ones = np.ones((3, seq, dim))

    model = keras.models.Sequential()
    model.add(keras.layers.Input((seq, dim)))
    model.add(TrigPosEmbedding(mode=TrigPosEmbedding.MODE_ADD))
    model.compile('adam', 'mse')
    model.summary()

    a = model.predict(ones)
    print(a)

def some_test():
    seq_len = 100
    model_dim = 60

    pos_em = PosEncoding(seq_len, model_dim)
    ones = np.ones((3, seq_len, model_dim), dtype=np.float64)
    r = np.random.random((3, seq_len, model_dim))
    a = pos_em(ones)
    a = a.numpy()
    b = pos_em(r)
    b = b.numpy()

    print()


if __name__ == '__main__':
    some_test_1()
