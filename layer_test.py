from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Input, Dense, LayerNormalization
from MyTransformer import TransformerEncoder, TransformerSelfAttention, ResidualFeedForward
import tensorflow as tf
import numpy as np

def test_TransformerEncoder():

    seq_len = 5
    embed_dim = 10
    ff_dim = 10
    num_heads = 2

    commen_imput = Input((seq_len, embed_dim))

    te = TransformerEncoder(embed_dim, ff_dim, num_heads)
    model = Sequential()
    model.add(commen_imput)
    model.add(te)

    model.compile()
    model.summary()

    model_1 = Sequential()

    self_att = TransformerSelfAttention(embed_dim, num_heads)
    ff = ResidualFeedForward(ff_dim, embed_dim)
    temp = ff(commen_imput)

    model_1.add(commen_imput)
    model_1.add(self_att)
    model_1.add(ff)

    model_1.compile()
    model_1.summary()

    model_2 = Sequential()
    model_2.add(commen_imput)
    model_2.add(ff)

    model_3 = Sequential()
    model_3.add(commen_imput)
    model_3.add(Dense(seq_len, ff_dim, activation=tf.nn.gelu))
    model_3.add(Dense(seq_len, embed_dim, activation="linear"))
    model_3.add(LayerNormalization())

    ones = np.ones((1, seq_len, embed_dim))


    model_2.compile()
    model_2.summary()


    model_3.compile()
    model_3.summary()

    a = model.predict(ones)
    b = model_1.predict(ones)
    c = model_2.predict(ones)
    d = model_3.predict(ones)

    print()


if __name__ == '__main__':
    test_TransformerEncoder()


