from tensorflow.keras.layers import Conv2D, Conv3D, Add, Layer, BatchNormalization, Activation, Input, MaxPooling2D
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.initializers import glorot_uniform
import numpy as np

class MyResidualConv2D(Layer):

    FILTERS = "filters"
    KERNEL_SIZE = "kernel_size"
    STRIDES = "strides"

    def __init__(self, filters, kernel_size, strides=2, **kwargs):
        super().__init__(**kwargs)
        self.filters = filters
        self.kernel_size = kernel_size
        self.strides = strides

        F0, F1, F2 = filters

        self.conv_2d_0 = Conv2D(filters=F0, kernel_size=(1, 1), strides=(strides, strides), padding='valid',
                                kernel_initializer=glorot_uniform(seed=0))
        self.norm_0 = BatchNormalization(axis=3)
        self.activation_0 = Activation("relu")

        ####

        self.conv_2d_1 = Conv2D(filters=F1, kernel_size=(kernel_size, kernel_size), strides=(1, 1), padding='same',
                                kernel_initializer=glorot_uniform(seed=0))
        self.norm_1 = BatchNormalization(axis=3)
        self.activation_1 = Activation("relu")

        ####

        self.conv_2d_2 = Conv2D(filters=F2, kernel_size=(1, 1), strides=(1, 1), padding='valid',
                                kernel_initializer=glorot_uniform(seed=0))
        self.norm_2 = BatchNormalization(axis=3)

        self.short_cut = Conv2D(filters=F2, kernel_size=(1, 1), strides=(strides, strides), padding='valid',
                                kernel_initializer=glorot_uniform(seed=0))
        self.short_cut_norm = BatchNormalization(axis=3)

        self.add = Add()

        self.activation_all = Activation("relu")

    def get_config(self):
        config = super(MyResidualConv2D, self).get_config()
        config[self.FILTERS] = self.filters
        config[self.KERNEL_SIZE] = self.kernel_size
        config[self.STRIDES] = self.strides

        return config

    def call(self, inputs):
        x = self.conv_2d_0(inputs)
        x = self.norm_0(x)
        x = self.activation_0(x)

        x = self.conv_2d_1(x)
        x = self.norm_1(x)
        x = self.activation_1(x)

        x = self.conv_2d_2(x)
        x = self.norm_2(x)

        short_cut = self.short_cut(inputs)
        short_cut = self.short_cut_norm(short_cut)

        x = self.add([x, short_cut])
        x = self.activation_all(x)

        return x

    def get_kernels(self):
        weight_list = list()
        weight_list.append(self.conv_2d_0.get_weights())
        weight_list.append(self.conv_2d_1.get_weights())
        weight_list.append(self.conv_2d_2.get_weights())
        weight_list.append(self.short_cut.get_weights())

        return weight_list

    def set_kernels(self, weight_list):
        self.conv_2d_0.set_weights(weight_list[0])
        self.conv_2d_1.set_weights(weight_list[1])
        self.conv_2d_2.set_weights(weight_list[2])
        self.short_cut.set_weights(weight_list[3])


class MyResidualIdentityConv2D(Layer):


    FILTERS = "filters"
    KERNEL_SIZE = "kernel_size"

    def __init__(self, filters, kernel_size, **kwargs):
        super().__init__(**kwargs)
        self.filters = filters
        self.kernel_size = kernel_size


        F0, F1, F2 = filters

        self.conv_2d_0 = Conv2D(filters=F0, kernel_size=(1, 1), strides=(1, 1), padding='valid',
               kernel_initializer=glorot_uniform(seed=0))
        self.norm_0 = BatchNormalization(axis=3)
        self.activation_0 = Activation("relu")

        ####

        self.conv_2d_1 = Conv2D(filters=F1, kernel_size=(kernel_size, kernel_size), strides=(1, 1), padding='same',
                                kernel_initializer=glorot_uniform(seed=0))
        self.norm_1 = BatchNormalization(axis=3)
        self.activation_1 = Activation("relu")

        ####

        self.conv_2d_2 = Conv2D(filters=F2, kernel_size=(1, 1), strides=(1, 1), padding='valid',
                   kernel_initializer=glorot_uniform(seed=0))
        self.norm_2 = BatchNormalization(axis=3)
        self.add = Add()

        self.activation_all = Activation("relu")

    def get_config(self):
        config = super(MyResidualIdentityConv2D, self).get_config()
        config[self.FILTERS] = self.filters
        config[self.KERNEL_SIZE] = self.kernel_size

        return config

    def call(self, inputs):
        # print(inputs)
        x = self.conv_2d_0(inputs)
        # print(x)
        x = self.norm_0(x)
        x = self.activation_0(x)

        x = self.conv_2d_1(x)
        x = self.norm_1(x)
        x = self.activation_1(x)

        x = self.conv_2d_2(x)
        x = self.norm_2(x)

        x = self.add([x, inputs])
        x = self.activation_all(x)

        return x

    def get_kernels(self):
        weight_list = list()
        weight_list.append(self.conv_2d_0.get_weights())
        weight_list.append(self.conv_2d_1.get_weights())
        weight_list.append(self.conv_2d_2.get_weights())

        return weight_list

    def set_kernels(self, weight_list):
        self.conv_2d_0.set_weights(weight_list[0])
        self.conv_2d_1.set_weights(weight_list[1])
        self.conv_2d_2.set_weights(weight_list[2])


class MyResidualConv2DV2(Layer):

    FILTERS = "filters"
    KERNEL_SIZE = "kernel_size"
    STRIDES = "strides"

    def __init__(self, filters, kernel_size, strides, **kwargs):
        super().__init__(**kwargs)
        self.filters = filters
        self.kernel_size = kernel_size
        self.strides = strides

        self.shortcut_conv = Conv2D(filters, (1, 1), strides=strides, padding="valid")

        self.conv_0 = Conv2D(filters, kernel_size, strides=(1, 1), padding="same")
        self.conv_1 = Conv2D(filters, kernel_size, strides=strides, padding="same")

        self.norm = BatchNormalization()

    def call(self, inputs):

        x = self.conv_0(inputs)
        x = self.conv_1(x)

        short_cut = self.shortcut_conv(inputs)

        return self.norm(x + short_cut)

    def get_config(self):
        config = super(MyResidualConv2DV2, self).get_config()
        config[self.FILTERS] = self.filters
        config[self.KERNEL_SIZE] = self.kernel_size
        config[self.STRIDES] = self.strides

        return config

    def get_kernels(self):
        weight_list = list()
        weight_list.append(self.conv_0.get_weights())
        weight_list.append(self.conv_1.get_weights())
        weight_list.append(self.shortcut_conv.get_weights())

        return weight_list

    def set_kernels(self, weight_list):
        self.conv_0.set_weights(weight_list[0])
        self.conv_1.set_weights(weight_list[1])
        self.shortcut_conv.set_weights(weight_list[2])

def the_next_test():
    some_len = 7

    filters = 64
    kernel = (3, 3)
    strides = (2, 2)

    c_list = list()
    for x in range(some_len):
        c_list.append(MyResidualConv2DV2(filters, kernel, strides))

    temp = np.ones((1, 100, 100, 1))

    for x in range(some_len):
        print(temp.shape)
        temp = c_list[x](temp)
    temp = temp.numpy()


    print(temp.shape)

def some_other_test():

    model = Sequential()

    model.add(Input((200, 200, 1)))
    model.add(Conv2D(64, (7, 7), strides=(2, 2), padding='same'))
    model.add(BatchNormalization(axis=3))
    # model.add(Activation('relu'))
    model.add(MaxPooling2D((3, 3), strides=(2, 2), padding='same'))

    n = 1
    model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
    model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))

    n = 2
    model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
    model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))

    n = 4
    model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
    model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))

    n = 8
    model.add(MyResidualConv2D([16 * n, 16 * n, 64 * n], 3, 2))
    model.add(MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3))

    model.compile()
    model.summary()


def some_test():

    i = Input((200, 200, 1))

    x = i

    x = Conv2D(64, (7, 7), strides=(2, 2), padding='same')(x)
    x = BatchNormalization(axis=3)(x)
    # x = Activation('relu')(x)
    x = MaxPooling2D((3, 3), strides=(2, 2), padding='same')(x)

    n = 1
    x = MyResidualConv2D([16*n, 16*n, 64*n], 3, 2)(x)
    x = MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3)(x)

    n = 2
    x = MyResidualConv2D([16*n, 16*n, 64*n], 3, 2)(x)
    x = MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3)(x)

    n = 4
    x = MyResidualConv2D([16*n, 16*n, 64*n], 3, 2)(x)
    x = MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3)(x)

    n = 8
    x = MyResidualConv2D([16*n, 16*n, 64*n], 3, 2)(x)
    x = MyResidualIdentityConv2D([16 * n, 16 * n, 64 * n], 3)(x)

    m = Model(inputs=[i], outputs=[x])
    m.compile()
    m.summary()


if __name__ == '__main__':
    the_next_test()
