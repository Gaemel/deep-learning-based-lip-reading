import numpy as np
import cv2
import os
from tensorflow.keras.models import model_from_json, Model, Sequential
from tensorflow.keras.layers import Dense
from shutil import copy

class Processor():

    def __init__(self, data_path, model_path, weights_path, max_video_len=25*5):
        self._model = self._load_model(model_path, weights_path)
        self._data_path = self._prepare_data_paths(data_path)
        self._max_video_len = max_video_len

        self._source_pointer = (0, 0)
        self._sources = list()

    def _prepare_data_paths(self, parent_dir):
        if not isinstance(parent_dir, list):
            parent_dir = [parent_dir]

        video_list = list()
        for d in parent_dir:
            video_list.append(self.collect_mp4_files(d))
        return video_list

    def collect_mp4_files(self, parent_dir):
        res_list = list()
        for title in os.listdir(parent_dir):
            sub_dir = os.path.join(parent_dir, title)
            if os.path.isdir(sub_dir):
                res_list += self.collect_mp4_files(sub_dir)
            elif sub_dir.endswith(".mp4"):
                res_list.append(sub_dir)
        return res_list

    # def save_batch(self, target_dir, data, number):
    #     pass

    def save_preprocessed(self, target_dir, og_file_name, data):
        file_name = os.path.join(target_dir, os.path.split(os.path.splitext(og_file_name)[0])[-1] + "_pp.npy")
        print(file_name)
        with open(file_name, 'wb') as f:
            np.save(f, data)

    def run(self, out_path, batch_size=1000):
        # batch = list()
        # batch_index = 0
        data, source = self._load_next_data_point()

        i = 0
        while data is not None:
            prediction = self._model.predict(data)
            self.save_preprocessed(out_path, str(i), prediction)
            self.copy_target_file(source, out_path, str(i))

            i += 1
            data, source = self._load_next_data_point()

        # for i in range(len(self._data_path)):
        #
        #     data = self._load_next_data_point(self._data_path[i])
        #     prediction = self._model.predict(data)
        #     self.save_preprocessed(out_path, self._data_path[i], prediction)
            # batch.append(prediction)
            #
            # if len(batch) >= batch_size:
            #     self._save_batch(out_path, prediction, batch_index)
            #     batch_index += 1

        print("done")

    def copy_target_file(self, source_video_path, out_path, target_title):
        text_source = os.path.splitext(source_video_path)[0] + ".txt"
        text_target = os.path.join(out_path, target_title + ".txt")
        copy(text_source, text_target)

    def _add_padding(self, video):
        padding = np.zeros((self._max_video_len, video.shape[1], video.shape[2]))

        video = np.concatenate((video, padding))
        video = video[:self._max_video_len]

        return video

    def _load_next_data_point(self):
        list_pointer, data_pointer = self._source_pointer
        if len(self._data_path[list_pointer]) <= data_pointer:
            return None, None

        target_path = self._data_path[list_pointer][data_pointer]

        list_pointer += 1
        if len(self._data_path) <= list_pointer:
            list_pointer = 0
            data_pointer += 1

        self._source_pointer = (list_pointer, data_pointer)

        cap = cv2.VideoCapture(target_path)
        scene = list()

        while True:
            s, f = cap.read()
            if not s:
                break
            scene.append(cv2.cvtColor(f, cv2.COLOR_BGR2GRAY) / 255)

        cap.release()

        scene = np.array(scene)
        scene = self._add_padding(scene)
        scene = np.expand_dims(scene, axis=-1)
        return scene, target_path

    def _load_model(self, model_path, weights_path):

        json_file = open(model_path, "r")
        model_json = json_file.read()
        json_file.close()
        temp_model = model_from_json(model_json)
        temp_model.load_weights(weights_path)
        print("read model from " + str(model_path))
        print("read weights from " + str(weights_path))

        temp_model.summary()

        model = Sequential()

        for i in range(len(temp_model.layers)):
            if isinstance(temp_model.layers[i], Dense):
                break
            else:
                model.add(temp_model.layers[i])
        model.build((None, 160, 160, 1))
        model.summary()
        return model


def prepare_data():
    model_path = """/home/ubuntu/PycharmProjects/lip-reading/classification/model_38.json"""
    weights_path = """/home/ubuntu/PycharmProjects/lip-reading/classification/weights_38.h5"""
    data_path = ["""/media/ubuntu/data/lip_reading_data/sorted_data""", """/media/ubuntu/data/data/2/mvlrs_v1/main"""]
    out_path = """/media/ubuntu/data/combined/pre_processed"""

    p = Processor(data_path, model_path, weights_path)
    p.run(out_path)

if __name__ == '__main__':
    prepare_data()