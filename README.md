To generate text from a video run: generate.py
To evaluate the model run: model_testing.py (this is faulty for the model trained with to faulty vision front-end)

Training was done with training_supervision.py and LipReadinTrainer.py

The models are listed in TransformerModels.py
The building blocks for the models are in MyTransformer*.py

packages used are:

- tensorflow 2.4.0 GPU
- numpy
- pandas
- yaml
- cv2