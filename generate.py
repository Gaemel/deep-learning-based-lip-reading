from MyTransformers_V5 import MyExplicitSwitchTransformerWorkaroundTail, MyExplicitSwitchTransformerWorkaround
from lip_data_reader import VectorizeChar
from model_testing import load_video_net, load_model
import cv2
import numpy as np

def load_scene(target_file, seq_len=80, do_crop=False):
    cap = cv2.VideoCapture(target_file)
    scene = list()

    while True:

        s, f = cap.read()
        if not s:
            break
        scene.append(cv2.cvtColor(f, cv2.COLOR_BGR2GRAY) / 255)

    cap.release()

    while len(scene) < seq_len:
        scene.append(np.zeros((scene[-1].shape)))

    scene = np.array(scene)
    if do_crop:
        scene = scene[:,30:-30, 30:-30]

    return scene

def prepare_data(data_path, seq_len=75, do_crop=False):
    if isinstance(data_path, list):
        res = list()
        for d in data_path:
            res += prepare_data(d, seq_len, do_crop)
        return res
    else:
        return [load_scene(data_path, seq_len, do_crop)]



def generate():
    # list of 100x100x1 grayscale videos of talking faces, centered on the lips
    data_location = ["""/home/ubuntu/PycharmProjects/lip-reading/test_vid/7SZGGnrXb1M_S0.mp4"""]
    do_crop = True # True if the input data 160 x 160
    seq_len = 75

    data = prepare_data(data_location, seq_len, do_crop)

    # the transformer model
    transformer_model_number = 4001
    transformer_model_weights = """./weights/transformer_weights/model_4001_weights_new.h5"""

    vision_net_weights = """./weights/vision_net/vision_net_weights.h5"""
    vis_net = load_video_net(vision_net_weights)
    transformer_model = load_model(transformer_model_number, transformer_model_weights)
    transformer_model.summary()
    vec_char = VectorizeChar()

    for i, d in enumerate(data):
        d = np.expand_dims(d, axis=-1)
        d = np.expand_dims(d, axis=0)
        d = vis_net.predict(d)
        if isinstance(transformer_model, MyExplicitSwitchTransformerWorkaroundTail) or isinstance(transformer_model, MyExplicitSwitchTransformerWorkaround):
            for x in range(2):
                transformer_model.set_mode(x)
                res = transformer_model.generate(d, vec_char.START_TOKEN).numpy()
                res = np.reshape(res, (seq_len, ))
                # 0 -> eng; 1 -> ger
                print("translation for data ", i, " on language ", x)
                print("\t", vec_char.translate_vector(res))
            transformer_model.set_mode(None)
        else:
            res = transformer_model.generate(d, vec_char.START_TOKEN).numpy()
            res = np.reshape(res, (seq_len,))
            print("translation for data ", i)
            print("\t", vec_char.translate_vector(res))
        print()

    print("done")


if __name__ == '__main__':
    generate()





