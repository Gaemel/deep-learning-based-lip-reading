from builtins import str
import tensorflow.keras.backend as K
import yaml
import os
from time import gmtime, strftime
import pandas as pd
from tensorflow.keras.models import load_model as load_keras_model

from TransformerModels import get_transformer_model, get_random_target_data, get_random_data
import matplotlib.pyplot as plt
import numpy as np


class Saver():

    MODEL = "model"
    RECENT_WIGHTS = "recent_wights"
    BEST_WIGHTS = "best_wights"
    HISTORY = "history"

    DEFAULT_TIME_STAMP_FORMAT = "%Y_%m_%d_%H_%M_%S"

    TIME_STAMP = "time_stamp"

    def __init__(self, target_dir, model_title, metrics, main_metric=None, low_is_good=True, add_time_stamp=False, iter_names=True, verbose=False, save_dir_title=None, time_stamp_format=None, filters=None, smooth_plot=None, line_styles=None):
        if time_stamp_format is None:
            self._time_stamp_format = self.DEFAULT_TIME_STAMP_FORMAT
        else:
            self._time_stamp_format = time_stamp_format

        self._target_dir = target_dir
        self._model_title = model_title
        self._weights_title = model_title + "_weights"
        self._hist_title = model_title + "_hist"
        self._metrics = metrics
        self._main_metric = main_metric if main_metric is not None else self._metrics[0]
        self._low_is_good = low_is_good

        self._hist = pd.DataFrame(columns=metrics + [self.TIME_STAMP])

        self._best_score = None
        self._verbose = verbose
        self._filters = filters

        if save_dir_title is not None:
            self._create_save_dir(save_dir_title, add_time_stamp, iter_names)

        self.smooth_plot = list() if smooth_plot is None else smooth_plot
        self._line_styles = dict() if line_styles is None else line_styles

    def _load(self, metrics):
        self._load_hist(metrics)

    def _load_hist(self, metrics):
        self._hist = pd.DataFrame(columns=metrics + [self.TIME_STAMP])
        self._hist = pd.read_csv(self.get_history_path())
        self._best_score = min(self._hist[self._main_metric]) if self._low_is_good else max(self._hist[self._main_metric])

    # @staticmethod
    # def load_model_weights(model, target_weights=RECENT_WIGHTS):
    #     if target_weights is None:
    #         target_weights = Saver.RECENT_WIGHTS
    #
    #     if target_weights == Saver.RECENT_WIGHTS:
    #         model.load_weights

    def load_weights(self, model, weights=RECENT_WIGHTS):
        if weights is None:
            weights = self.RECENT_WIGHTS

        if weights == self.RECENT_WIGHTS:
            print("load: ", self.get_recent_weights_path())
            model.load_weights(self.get_recent_weights_path())
        elif weights == self.BEST_WIGHTS:
            print("load: ", self.get_best_weights_path())
            model.load_weights(self.get_best_weights_path())

        return model

    def load_model(self, weights=None):
        model = load_keras_model(self.get_model_path())

        if weights is not None:
            model = self.load_weights(model, weights)

        return model

    def set_line_styles(self, line_styles):
        self._line_styles = line_styles

    def set_smooth_plot(self, some_list):
        self.smooth_plot = some_list

    @staticmethod
    def from_data(data_path, model_title, metrics, main_metric=None, low_is_good=True):
        s = Saver(data_path, model_title, metrics, main_metric, low_is_good=low_is_good)
        s._load(metrics)

        return s

    def get_time_stamp(self):
        return strftime(self._time_stamp_format, gmtime())

    def _create_save_dir(self, save_dir_title, add_time_stamp:bool, iter_names:bool):

        if save_dir_title != "":
            save_dir_title += "_"

        if add_time_stamp:
            save_dir_title += self.get_time_stamp() + "_"

        if iter_names:
            i = 0
            while True:
                temp_title = save_dir_title + str(i)
                if os.path.exists(os.path.join(self._target_dir, temp_title)):
                    i += 1
                else:
                    save_dir_title = temp_title
                    break

        if save_dir_title.endswith("_"):
            save_dir_title = save_dir_title[:-1]

        self._target_dir = os.path.join(self._target_dir, save_dir_title)
        if self._verbose:
            print("creating: " + str(self._target_dir))

        os.makedirs(self._target_dir)

    def _update_hist(self, scores):
        scores[self.TIME_STAMP] = self.get_time_stamp()
        self._hist = self._hist.append(scores, ignore_index=True)

    def next_data_point(self, model, score_dict, do_plot=True):
        if not os.path.exists(self.get_save_dir_path()):
            os.makedirs(self.get_save_dir_path(), exist_ok=True)
        # self._save_model(model)

        if self._main_metric in score_dict.keys():

            if self._best_score is None or (score_dict[self._main_metric] <= self._best_score and self._low_is_good) or (not self._low_is_good and score_dict[self._main_metric] >= self._best_score):

                self._best_score = score_dict[self._main_metric]
                self._save_weights(model, best=True)

        self._save_weights(model, best=False)
        self._update_hist(score_dict)
        self._save_hist()
        if do_plot:
            self.plot_hist()
        self._save_lr(model)

        self._write_meta_file()

    def _save_lr(self, model):
        lr = K.eval(model.optimizer.lr)
        with open(os.path.join(self.get_save_dir_path(), "lr.txt"), "w+") as file:
            file.write(str(lr))

    def get_filter(self, key=None):
        if self._filters is not None and key is not None and key in self._filters:
            return self._filters[key]
        else:
            return 101

    def plot_hist(self):

        # plt.figure(figsize=(20, 20))

        fig, ax = plt.subplots(figsize=(12, 12))
        t = np.arange(0, self._hist.shape[0], 1)
        ax.set(xlabel="epoch", ylabel="loss")
        # ax.set_yscale('log')
        ax.grid(True, ls="-", which="both")
        for m in self._metrics:
            filter_size = self.get_filter(m)
            filter = np.ones(filter_size) / filter_size
            filter_half = int((filter_size - 1) / 2)

            a = self._hist[m]
            a = a.fillna(method='ffill', limit=1000)
            a = a.to_numpy()

            if m in self._line_styles:
                ax.plot(t, a, label=m, linewidth=2, linestyle=self._line_styles[m])
            else:
                ax.plot(t, a, label=m, linewidth=2)
            if m in self.smooth_plot:
                temp = np.array([a[0]] * filter_half + a.tolist() + [a[a.shape[0]-1]] * filter_half)
                some_test = np.convolve(filter, temp, "same").tolist()
                some_test = np.array(some_test[filter_half:-filter_half])
                ax.plot(t, some_test, label=m + "_smooth", linewidth=1, linestyle=":")

        legend = ax.legend(loc='upper right', fontsize='x-large')

        # plt.yscale("log")
        plt.savefig(self.get_graph_path())
        plt.cla()
        plt.clf()
        plt.close("all")

        # plt.show()

    def _save_hist(self):
        if self._verbose:
            print("saving history as: " + self.get_history_path())
        self._hist.to_csv(self.get_history_path(), index=False)

    def _save_model(self, model):
        if not os.path.exists(self.get_model_path()):
            if self._verbose:
                print("saved model at: " + str(self.get_model_path()))
            model.save(self.get_model_path())

    def save_interval_weights(self, model, epoch):
        self._save_weights(model, interval=epoch)

    def _save_weights(self, model, best=False, interval=None):
        if interval is not None:
            if self._verbose:
                print("saving (best) weights as: " + self.get_interval_weights_path(interval))
            model.save_weights(self.get_interval_weights_path(interval))

        elif best:
            if self._verbose:
                print("saving (best) weights as: " + self.get_best_weights_path())
            model.save_weights(self.get_best_weights_path())
        else:
            if self._verbose:
                print("saving weights as: " + self.get_recent_weights_path())
            model.save_weights(self.get_recent_weights_path())

    def set_verbosity(self, is_verbose):
        self._verbose = is_verbose

    def get_save_dir_path(self):
        return self._target_dir

    def get_model_path(self):
        return os.path.join(self._target_dir, self._model_title)

    def get_meta_path(self):
        return os.path.join(self._target_dir, self._model_title + "_meta.yaml")

    def get_recent_weights_path(self):
        return os.path.join(self._target_dir, self._model_title + "_recent_weights.h5")

    def get_best_weights_path(self):
        return os.path.join(self._target_dir, self._model_title + "_best_weights.h5")

    def get_interval_weights_path(self, epoch):
        return os.path.join(self._target_dir, self._model_title + "_" + str(epoch) + "_weights.h5")

    def get_graph_path(self):
        return os.path.join(self._target_dir, self._model_title + "_hist.png")

    def get_history_path(self):
        return os.path.join(self._target_dir, self._model_title + "_hist.csv")

    def _write_meta_file(self):
        if os.path.exists(self.get_meta_path()):
            return

        meta_dict = {self.MODEL: self.get_model_path(),
                     self.RECENT_WIGHTS: self.get_recent_weights_path(),
                     self.BEST_WIGHTS: self.get_best_weights_path(),
                     self.HISTORY: self.get_history_path()}

        with open(self.get_meta_path(), "w") as file:
            if self._verbose:
                print("save meta at: " + str(self.get_meta_path()))
            yaml.dump(meta_dict, file)

    def set_filter(self, filters):
        self._filters = filters

def bluber():

    model = get_transformer_model(4, 44, (160, 160, 1), 44, 10)

    try:
        model.summary()
    except Exception as e:
        print(e)
    bs = 10

    x = get_random_data((44, 160, 160, 1), bs)
    y = get_random_target_data((44, 44), bs)

    ds = {"source": x, "target": y}

    # text = model.generate(x, 2)
    loss = model.fit(ds, validation_data=ds)

    print(x.shape)
    print(y.shape)

    print("start predict")
    p = model.predict(ds)

    print("-----------------")
    print(p)
    print("-----------------")

    return model, ds


def some_testing():
    some_path = """/media/ubuntu/data/test"""
    model, ds = bluber()
    # model = None
    s = Saver(some_path, "model", ["loss", "val_loss"], save_dir_title="", add_time_stamp=False, iter_names=True, verbose=True)

    s.next_data_point(model, {"loss": 0.5384, "val_loss": 0.3})
    s.next_data_point(model, {"loss": 0.5384})
    s.next_data_point(model, {"loss": 0.5384, "val_loss": 0.2})
    s.next_data_point(model, {"loss": 0.5384, "val_loss": 0.3})

    some_path = s.get_save_dir_path()

    s_l = Saver.from_data(some_path, "model", main_metric="val_loss", metrics=["loss", "val_loss"])
    model_1 = get_transformer_model(4, 44, (160, 160, 1), 44, 10)
    p_2 = model_1.predict(ds)
    model_1 = s_l.load_weights(model_1)
    model_1.summary()
    # model = s_l.load_model()

    p_0 = model.predict(ds)
    p_1 = model_1.predict(ds)
    print("-----------------")
    print("P0")
    print(p_0)
    print("-----------------")
    print("P1")
    print(p_1)
    print("-----------------")
    print("P2")
    print(p_2)

    print("done")

def load():
    some_path = """/media/ubuntu/data/test/2021_07_15_15_38_38"""
    s = Saver.from_data(some_path, "model", ["loss", "val_loss"])
    m = s.load_model()

    print("done")

def write():
    filters = {"loss":21, "val_loss": 5}

    some_path = """/media/ubuntu/data/test"""
    model = bluber()[0]
    # model = None
    s = Saver(some_path, "model", ["loss", "val_loss"], main_metric="val_loss", save_dir_title="", add_time_stamp=False, iter_names=True, verbose=True, filters=filters)
    s.next_data_point(model, {"loss": 0.5384, "val_loss": 0.3})
    s.next_data_point(model, {"loss": 0.6})
    s.next_data_point(model, {"loss": 0.3, "val_loss": 0.2})
    s.next_data_point(model, {"loss": 0.5384, "val_loss": 0.3})
    s.next_data_point(model, {"loss": 0.3, "val_loss": 0.2})
    s.next_data_point(model, {"loss": 0.5, "val_loss": 0.3})
    s.next_data_point(model, {"loss": 0.2, "val_loss": 0.2})
    s.next_data_point(model, {"loss": 0.1, "val_loss": 0.3})
    s.next_data_point(model, {"loss": 0.3, "val_loss": 0.2})
    s.next_data_point(model, {"loss": 0.5384, "val_loss": 0.3})

    s.plot_hist()

if __name__ == '__main__':
    write()
