from tensorflow.keras.layers import Input, Embedding, MultiHeadAttention, Layer, LayerNormalization, Dense, Conv1D, Reshape, MaxPooling3D, AveragePooling3D, Flatten, BatchNormalization, Conv3D, Conv2D, MaxPooling2D, GlobalMaxPool2D
from tensorflow.keras.models import Model, Sequential

from residual_conv import MyResidualIdentityConv2D, MyResidualConv2D, MyResidualConv2DV2
import numpy as np

from MyLayers import BorderDrop3D


def get_vision_net(version_number, seq_len):
    if version_number == 0:
        return get_vision_net_0(seq_len)
    if version_number == 1:
        return get_vision_net_1(seq_len)
    if version_number == 2:
        return get_vision_net_2(seq_len)
    if version_number == 3:
        return get_vision_net_3(seq_len)
    if version_number == 4:
        return get_vision_net_4(seq_len)
    if version_number == 5:
        return get_vision_net_5(seq_len)
    if version_number == 6:
        return get_vision_net_6(seq_len)
    if version_number == 7:
        return get_vision_net_7(seq_len)
    if version_number == 8:
        return get_vision_net_8(seq_len)
    if version_number == 9:
        return get_vision_net_9(seq_len)
    if version_number == 10:
        return get_vision_net_10(seq_len)
    if version_number == 100:
        return get_vision_net_100(seq_len)
    if version_number == 101:
        return get_vision_net_101(seq_len)
    if version_number == 200:
        return get_vision_net_200(seq_len)
    if version_number == 201:
        return get_vision_net_201(seq_len)
    if version_number == 202:
        return get_vision_net_202(seq_len)
    if version_number == 203:
        return get_vision_net_203(seq_len)
    
    
def get_vision_net_0(seq_len):
    vision_net = Sequential(name="VisionLayers")
    vision_net.add(Input((seq_len, 160, 160, 1)))

    vision_net.add(BorderDrop3D(35))
    vision_net.add(BatchNormalization())
    vision_net.add(Conv3D(filters=64, kernel_size=(5, 7, 7), strides=(1, 2, 2), padding="same"))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))

    base = 64
    n = 1
    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    n = 2
    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    n = 4
    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    vision_net.add(MaxPooling3D(pool_size=(1, 2, 2), strides=(1, 2, 2), padding="same"))

    temp = np.product(vision_net.layers[-1].output_shape[2:])

    vision_net.add(Reshape((seq_len, temp)))
    vision_net.compile()
    vision_net.summary()

    return vision_net


def get_vision_net_10(seq_len):
    vision_net = Sequential(name="VisionLayers")
    vision_net.add(Input((seq_len, 90, 90, 1)))
    vision_net.add(BatchNormalization())
    # vision_net.add(Conv2D(filters=18, kernel_size=(3, 3), strides=(2, 2)))
    vision_net.add(Conv3D(filters=64, kernel_size=(5, 7, 7), strides=(1, 2, 2), padding="same"))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))

    base = 32
    n = 1

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    n = 2

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    n = 4

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    n = 8

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    vision_net.add(AveragePooling3D(pool_size=(1, 3, 3), strides=(1, 3, 3), padding="same"))

    temp = np.product(vision_net.layers[-1].output_shape[2:])

    vision_net.add(Reshape((seq_len, temp)))

    vision_net.compile()
    vision_net.summary()

    return vision_net




def get_vision_net_100(seq_len):
    vision_net = Sequential(name="VisionLayers")
    vision_net.add(Input((seq_len, 100, 100, 1)))
    vision_net.add(BatchNormalization())
    # vision_net.add(Conv2D(filters=18, kernel_size=(3, 3), strides=(2, 2)))
    vision_net.add(Conv3D(filters=32, kernel_size=(5, 7, 7), strides=(1, 2, 2), padding="same"))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))

    base = 32
    n = 1

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    n = 2

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    n = 4

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    n = 8

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    vision_net.add(MaxPooling3D(pool_size=(1, 4, 4), strides=(1, 4, 4), padding="same"))

    temp = np.product(vision_net.layers[-1].output_shape[2:])

    vision_net.add(Reshape((seq_len, temp)))

    vision_net.compile()
    vision_net.summary()

    return vision_net



def get_vision_net_101(seq_len):
    vision_net = Sequential(name="VisionLayers")
    vision_net.add(Input((seq_len, 100, 100, 1)))
    vision_net.add(BatchNormalization())
    # vision_net.add(Conv2D(filters=18, kernel_size=(3, 3), strides=(2, 2)))
    vision_net.add(Conv3D(filters=32, kernel_size=(5, 7, 7), strides=(1, 2, 2), padding="same"))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))

    base = 64
    n = 1

    vision_net.add(MyResidualConv2D([base * n, base * n, base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, base * n], 3))

    n = 2

    vision_net.add(MyResidualConv2D([base * n, base * n, base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, base * n], 3))

    n = 4

    vision_net.add(MyResidualConv2D([base * n, base * n, base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, base * n], 3))

    n = 8

    vision_net.add(MyResidualConv2D([base * n, base * n, base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, base * n], 3))

    vision_net.add(MaxPooling3D(pool_size=(1, 4, 4), strides=(1, 4, 4), padding="same"))

    temp = np.product(vision_net.layers[-1].output_shape[2:])

    vision_net.add(Reshape((seq_len, temp)))

    vision_net.compile()
    vision_net.summary()

    return vision_net


def get_vision_net_9(seq_len):
    vision_net = Sequential(name="VisionLayers")
    vision_net.add(Input((seq_len, 90, 90, 1)))
    vision_net.add(BatchNormalization())
    # vision_net.add(Conv2D(filters=18, kernel_size=(3, 3), strides=(2, 2)))
    vision_net.add(Conv3D(filters=32, kernel_size=(5, 7, 7), strides=(1, 2, 2), padding="same"))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))

    base = 32
    n = 1

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    n = 2

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    n = 4

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    n = 8

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    vision_net.add(AveragePooling3D(pool_size=(1, 3, 3), strides=(1, 3, 3), padding="same"))

    temp = np.product(vision_net.layers[-1].output_shape[2:])

    vision_net.add(Reshape((seq_len, temp)))

    vision_net.compile()
    vision_net.summary()

    return vision_net


def get_vision_net_8(seq_len):
    vision_net = Sequential(name="VisionLayers")
    vision_net.add(Input((seq_len, 90, 90, 1)))
    vision_net.add(BatchNormalization())
    vision_net.add(Conv3D(filters=32, kernel_size=(5, 7, 7), strides=(1, 2, 2), padding="same"))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))

    base = 32
    n = 1

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    n = 2

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    n = 4

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    vision_net.add(AveragePooling3D(pool_size=(1, 3, 3), strides=(1, 3, 3), padding="same"))

    temp = np.product(vision_net.layers[-1].output_shape[2:])

    vision_net.add(Reshape((seq_len, temp)))

    vision_net.compile()
    vision_net.summary()

    return vision_net


def get_vision_net_7(seq_len):
    vision_net = Sequential(name="VisionLayers")
    vision_net.add(Input((seq_len, 90, 90, 1)))
    vision_net.add(BatchNormalization())
    vision_net.add(Conv3D(filters=32, kernel_size=(5, 7, 7), strides=(1, 2, 2), padding="same"))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))

    base = 32
    n = 1

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    n = 2

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    n = 4

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    vision_net.add(AveragePooling3D(pool_size=(1, 3, 3), strides=(1, 3, 3), padding="same"))

    temp = np.product(vision_net.layers[-1].output_shape[2:])

    vision_net.add(Reshape((seq_len, temp)))

    vision_net.compile()
    vision_net.summary()

    return vision_net


def get_vision_net_6(seq_len):
    vision_net = Sequential(name="VisionLayers")
    vision_net.add(Input((seq_len, 90, 90, 1)))
    vision_net.add(BatchNormalization())
    vision_net.add(Conv2D(filters=16, kernel_size=(3, 3), strides=(2, 2)))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))
    vision_net.add(Conv3D(filters=64, kernel_size=(5, 7, 7), strides=(1, 3, 3), padding="same"))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))

    base = 64
    n = 1

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    n = 2

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    vision_net.add(MaxPooling3D(pool_size=(1, 2, 2), strides=(1, 2, 2), padding="same"))

    temp = np.product(vision_net.layers[-1].output_shape[2:])

    vision_net.add(Reshape((seq_len, temp)))

    vision_net.compile()
    vision_net.summary()

    return vision_net


def get_vision_net_200(seq_len):
    vision_net = Sequential(name="VisionLayers")
    vision_net.add(Input((seq_len, 100, 100, 1)))
    vision_net.add(BatchNormalization())
    vision_net.add(Conv2D(filters=32, kernel_size=(3, 3), strides=(2, 2)))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))
    vision_net.add(Conv3D(filters=128, kernel_size=(5, 21, 21), strides=(1, 3, 3), padding="same"))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))

    base = 128
    n = 2

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    vision_net.add(MaxPooling3D(pool_size=(1, 3, 3), strides=(1, 3, 3), padding="same"))

    temp = np.product(vision_net.layers[-1].output_shape[2:])

    vision_net.add(Reshape((seq_len, temp)))

    vision_net.compile()
    vision_net.summary()

    return vision_net



def get_vision_net_201(seq_len):
    vision_net = Sequential(name="VisionLayers")
    vision_net.add(Input((seq_len, 100, 100, 1)))
    vision_net.add(BatchNormalization())
    vision_net.add(Conv2D(filters=32, kernel_size=(3, 3), strides=(2, 2)))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))
    vision_net.add(Conv3D(filters=128, kernel_size=(5, 21, 21), strides=(1, 3, 3), padding="same"))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))

    base = 64
    n = 1
    k = (3, 3)
    s = (1, 1)

    vision_net.add(MyResidualConv2DV2(filters=base*n, kernel_size=k, strides=s))

    s = (2, 2)
    n = 2
    vision_net.add(MyResidualConv2DV2(filters=base*n, kernel_size=k, strides=s))

    n = 4
    vision_net.add(MyResidualConv2DV2(filters=base*n, kernel_size=k, strides=s))

    n = 8
    vision_net.add(MyResidualConv2DV2(filters=base*n, kernel_size=k, strides=s))


    vision_net.add(MaxPooling3D(pool_size=(1, 2, 2), strides=(1, 2, 2), padding="same"))

    temp = np.product(vision_net.layers[-1].output_shape[2:])

    vision_net.add(Reshape((seq_len, temp)))

    vision_net.compile()
    vision_net.summary()

    return vision_net

def get_vision_net_202(seq_len):
    vision_net = Sequential(name="VisionLayers")
    vision_net.add(Input((seq_len, 100, 100, 1)))
    vision_net.add(BatchNormalization())
    vision_net.add(Conv2D(filters=32, kernel_size=(3, 3), strides=(2, 2)))
    vision_net.add(Conv3D(filters=128, kernel_size=(5, 21, 21), strides=(1, 3, 3), padding="same"))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))

    base = 64
    n = 1
    k = (3, 3)
    s = (1, 1)

    vision_net.add(MyResidualConv2DV2(filters=base*n, kernel_size=k, strides=s))

    s = (2, 2)
    n = 2
    vision_net.add(MyResidualConv2DV2(filters=base*n, kernel_size=k, strides=s))

    n = 4
    vision_net.add(MyResidualConv2DV2(filters=base*n, kernel_size=k, strides=s))

    n = 8
    vision_net.add(MyResidualConv2DV2(filters=base*n, kernel_size=k, strides=s))


    vision_net.add(MaxPooling3D(pool_size=(1, 2, 2), strides=(1, 2, 2), padding="same"))

    temp = np.product(vision_net.layers[-1].output_shape[2:])

    vision_net.add(Reshape((seq_len, temp)))

    vision_net.compile()
    vision_net.summary()

    return vision_net


def get_vision_net_203(seq_len):
    vision_net = Sequential(name="VisionLayers")
    vision_net.add(Input((seq_len, 100, 100, 1)))
    vision_net.add(BatchNormalization())
    # vision_net.add(Conv2D(filters=32, kernel_size=(3, 3), strides=(2, 2)))
    vision_net.add(Conv3D(filters=64, kernel_size=(5, 7, 7), strides=(1, 3, 3), padding="same"))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))

    base = 64
    n = 1
    k = (3, 3)
    s = (1, 1)

    vision_net.add(MyResidualConv2DV2(filters=base*n, kernel_size=k, strides=s))

    s = (2, 2)
    n = 2
    vision_net.add(MyResidualConv2DV2(filters=base*n, kernel_size=k, strides=s))

    n = 4
    vision_net.add(MyResidualConv2DV2(filters=base*n, kernel_size=k, strides=s))

    n = 8
    vision_net.add(MyResidualConv2DV2(filters=base*n, kernel_size=k, strides=s))

    vision_net.add(MaxPooling3D(pool_size=(1, 3, 3), strides=(1, 3, 3), padding="same"))

    temp = np.product(vision_net.layers[-1].output_shape[2:])

    vision_net.add(Reshape((seq_len, temp)))

    vision_net.compile()
    vision_net.summary()

    return vision_net



def get_vision_net_5(seq_len):
    vision_net = Sequential(name="VisionLayers")
    vision_net.add(Input((seq_len, 90, 90, 1)))
    vision_net.add(BatchNormalization())
    vision_net.add(Conv2D(filters=20, kernel_size=(3, 3), strides=(2, 2)))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))
    vision_net.add(Conv3D(filters=128, kernel_size=(5, 21, 21), strides=(1, 3, 3), padding="same"))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))

    base = 128
    n = 1

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    vision_net.add(MaxPooling3D(pool_size=(1, 2, 2), strides=(1, 2, 2), padding="same"))

    temp = np.product(vision_net.layers[-1].output_shape[2:])

    vision_net.add(Reshape((seq_len, temp)))

    vision_net.compile()
    vision_net.summary()

    return vision_net


def get_vision_net_4(seq_len):
    vision_net = Sequential(name="VisionLayers")
    vision_net.add(Input((seq_len, 160, 160, 1)))
    vision_net.add(BorderDrop3D(35))
    vision_net.add(BatchNormalization())
    vision_net.add(Conv2D(filters=20, kernel_size=(3, 3), strides=(2, 2)))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))
    vision_net.add(Conv3D(filters=128, kernel_size=(5, 21, 21), strides=(1, 3, 3), padding="same"))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))

    base = 128
    n = 1

    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    vision_net.add(MaxPooling3D(pool_size=(1, 2, 2), strides=(1, 2, 2), padding="same"))

    temp = np.product(vision_net.layers[-1].output_shape[2:])

    vision_net.add(Reshape((seq_len, temp)))

    vision_net.compile()
    vision_net.summary()

    return vision_net


def get_vision_net_3(seq_len):
    vision_net = Sequential(name="VisionLayers")
    vision_net.add(Input((seq_len, 160, 160, 1)))
    vision_net.add(BorderDrop3D(35))
    vision_net.add(BatchNormalization())
    vision_net.add(Conv2D(filters=20, kernel_size=(3, 3), strides=(2, 2)))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))
    vision_net.add(Conv3D(filters=128, kernel_size=(5, 21, 21), strides=(1, 3, 3), padding="same"))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))

    base = 128
    n = 2
    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    vision_net.add(MaxPooling3D(pool_size=(1, 2, 2), strides=(1, 2, 2), padding="same"))

    temp = np.product(vision_net.layers[-1].output_shape[2:])

    vision_net.add(Reshape((seq_len, temp)))
    vision_net.compile()
    vision_net.summary()

    return vision_net

def get_vision_net_2(seq_len):
    vision_net = Sequential(name="VisionLayers")
    vision_net.add(Input((seq_len, 160, 160, 1)))
    vision_net.add(BorderDrop3D(35))
    vision_net.add(BatchNormalization())
    vision_net.add(Conv2D(filters=20, kernel_size=(3, 3), strides=(2, 2)))
    vision_net.add(Conv3D(filters=100, kernel_size=(5, 21, 21), strides=(1, 3, 3), padding="same"))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))

    base = 100
    n = 1
    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    n = 2
    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    vision_net.add(MaxPooling3D(pool_size=(1, 2, 2), strides=(1, 2, 2), padding="same"))

    temp = np.product(vision_net.layers[-1].output_shape[2:])

    vision_net.add(Reshape((seq_len, temp)))
    vision_net.compile()
    vision_net.summary()

    return vision_net



def get_vision_net_1(seq_len):
    vision_net = Sequential(name="VisionLayers")
    vision_net.add(Input((seq_len, 160, 160, 1)))
    vision_net.add(BorderDrop3D(35))
    vision_net.add(BatchNormalization())
    vision_net.add(Conv3D(filters=64, kernel_size=(5, 9, 9), strides=(1, 2, 2), padding="same"))
    vision_net.add(MaxPooling3D(strides=(1, 2, 2), padding="same"))

    base = 32
    n = 1
    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    n = 2
    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    n = 8
    vision_net.add(MyResidualConv2D([base * n, base * n, 2 * base * n], 3, 2))
    vision_net.add(MyResidualIdentityConv2D([base * n, base * n, 2 * base * n], 3))

    vision_net.add(MaxPooling3D(pool_size=(1, 3, 3), strides=(1, 3, 3), padding="same"))

    temp = np.product(vision_net.layers[-1].output_shape[2:])

    vision_net.add(Reshape((seq_len, temp)))
    vision_net.compile()
    vision_net.summary()

    return vision_net


def test_net(seq_len):

    vision_net = Sequential(name="VisionLayers")
    vision_net.add(Input((seq_len, 10, 1)))
    vision_net.add(Conv2D(filters=2, kernel_size=(3, 3), strides=(2, 2)))
    vision_net.add(Flatten())
    vision_net.compile()
    vision_net.summary()

    return vision_net


def copy_kernel(net_from, net_to):

    for i, layer in enumerate(net_from.layers):

        if isinstance(layer, MyResidualConv2D) or isinstance(layer, MyResidualIdentityConv2D) or isinstance(layer, MyResidualConv2DV2):
            net_to.layers[i].set_kernels(layer.get_kernels())

        elif isinstance(layer, Conv3D) or isinstance(layer, Conv2D):
            net_to.layers[i].set_weights(layer.get_weights())

    return net_to

def copy_test():
    net_small = get_vision_net_3(10)
    net_large = get_vision_net_3(20)

    ones_small = np.ones((1, 10, 160, 160, 1))
    ones_large = np.ones((1, 20, 160, 160, 1))
    random_large = np.random.random_sample((1, 20, 160, 160, 1))
    random_small = random_large[:, :10, :, :]
    c = net_large.predict(ones_large)
    c_0 = net_large.predict(random_large)

    new_net = copy_kernel(net_small, net_large)
    a = net_small.predict(ones_small)
    a_0 = net_small.predict(random_small)
    b = net_large.predict(ones_large)
    b_0 = net_large.predict(random_large)

    print(a)
    print(30*"-")
    print(b)
    print(30*"-")
    print(c)
    print(30*"-")
    print()

    print(a_0)
    print(30*"-")
    print(b_0)
    print(30*"-")
    print(c_0)
    print(30*"-")

def main():
    copy_test()


if __name__ == '__main__':
    main()


